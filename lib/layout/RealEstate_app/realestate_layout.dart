import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:house_app/controller/main_screen_controller.dart';
import 'package:house_app/core/constant/app_strings.dart';

class RealEstateLayout extends StatelessWidget {
  List<BottomNavigationBarItem> bottomItem =  [
    BottomNavigationBarItem(
        icon: Icon(
          Icons.home,
          color: Colors.blueGrey,
        ),
        label: AppStrings.home.tr),
    BottomNavigationBarItem(
        icon: Icon(
          Icons.add_home,
          color: Colors.lightBlueAccent,
          size: 35,
        ),
        label: AppStrings.posters.tr),
    BottomNavigationBarItem(
        icon: Icon(
          Icons.settings,
          color: Colors.blueGrey,
        ),
        label: AppStrings.settings.tr),
  ];

  @override
  Widget build(BuildContext context) {
    MainScreenController controller = Get.put(MainScreenController());
    return Scaffold(
        appBar: AppBar(
          leading: const Icon(Icons.home_work_rounded),
          title: Text(AppStrings.soporific.tr),
        ),
        body: GetBuilder<MainScreenController>(builder: (context) {
          return controller.currentPage;
        }),
        bottomNavigationBar:
            GetBuilder<MainScreenController>(builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: BottomNavigationBar(
                elevation: 8,
                selectedItemColor: const Color(0xFF2AA8C0),
                currentIndex: controller.currentIndex,
                onTap: (index) {
                  controller.onTapBottom(index);
                },
                items: bottomItem),
          );
        }));
  }
}
