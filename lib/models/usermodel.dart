import 'package:flutter/cupertino.dart';

class UserModel {
  final int id;
  final String name;
  final String phone;

  UserModel({
    required this.id,
    required this.name,
    required this.phone,
  });
}

class BoardingModel {
  final Widget image;
  final String title;
  final String body;

  BoardingModel({
    required this.image,
    required this.title,
    required this.body,
  });
}

class User {
  final String? token;
  final int? id;
  final String? name;
  final String? email;
  final String? number;
  final String? password;
  final String? c_password;

  User(
      {this.token,
      this.name,
      required this.email,
      this.number,
      required this.password,
      this.c_password,
      this.id});
}
