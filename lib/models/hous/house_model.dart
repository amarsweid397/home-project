// class HouseModel {
//   int? id;
//   int? price;
//   int? size;
//   int? rent_or_buy;
//   int? region_id;
//   String? region;
//   int? direction;
//   int? max_price;
//   int? min_price;
//   int? min_size;
//   int? max_size;
//   int? bedroom_furniture;
//   int? sitting_room_furniture;
//   int? kitchen_furniture;
//   int? bathroom_furniture;
//   int? floor;
//   int? num_of_rooms;
//   int? dicoration;
//   int? building_id;
//   String? user_name;
//   String? image_1;
//   String? image_2;
//   String? image_3;
//   String? address;
//   HouseModel({
//     this.min_price,
//     this.max_price,
//     this.min_size,
//     this.max_size,
//     this.rent_or_buy,
//     this.region_id,
//     this.direction,
//     this.bedroom_furniture,
//     this.sitting_room_furniture,
//     this.kitchen_furniture,
//     this.bathroom_furniture,
//     this.floor,
//     this.num_of_rooms,
//     this.dicoration,
//     this.building_id,
//     this.user_name,
//     this.image_1,
//     this.image_2,
//     this.image_3,
//     this.address,
//   });
//   HouseModel.fromJson(Map<String, dynamic> map) {
//     price = int.parse(map['price'].toString());
//     size = int.parse(map['size'].toString());
//     region = map['region'].toString();
//     direction = int.parse(map['direction'].toString());
//     floor = int.parse(map['floor'].toString());
//     rent_or_buy = int.parse(map['floor'].toString());
//     dicoration = int.parse(map['dicoration'].toString());
//     building_id = int.parse(map['building_id'].toString());
//     user_name = map['user_name'].toString();
//     image_1 = map['image_1'].toString();
//     image_2 = map['image_2'].toString();
//     image_3 = map['image_3'].toString();
//   }
//   RoomtoJson() {
//     return {
//       'rent_or_buy': rent_or_buy,
//       'region_id': region_id,
//       'min_price': min_price,
//       'max_price': max_price,
//       'floor': floor,
//       'num_of_rooms': num_of_rooms,
//       'min_size': min_size,
//       'max_size': max_size,
//       'direction': direction,
//     };
//   }
// }
import 'dart:io';

class HouseModel {
  int? id;

  int? size;

  String? region;

  int? max_price;
  int? min_price;
  int? min_size;
  int? max_size;
  int? num_of_rooms;
  String? user_name;
  String? image_1;
  String? image_2;
  String? image_3;
  String? payment_type;
  int? is_owner;
  int? rent_or_buy;
  int? payment_type_id;
  int? furniture_type_id;
  int? bedroom_furniture;
  int? sitting_room_furniture;
  int? kitchen_furniture;
  int? bathroom_furniture;
  int? building_id;
  int? region_id;
  String? address;
  int? numOfRooms;
  int? price;
  int? floor;
  int? direction;
  int? dicoration;
  File? image1;
  File? image2;
  File? image3;
  String? created_date;
  HouseModel({
    this.min_price,
    this.max_price,
    this.min_size,
    this.max_size,
    this.rent_or_buy,
    this.region_id,
    this.direction,
    this.bedroom_furniture,
    this.sitting_room_furniture,
    this.kitchen_furniture,
    this.bathroom_furniture,
    this.floor,
    this.num_of_rooms,
    this.dicoration,
    this.building_id,
    this.user_name,
    this.image_1,
    this.image_2,
    this.image_3,
    this.address,
  });
  HouseModel.addAddverst(
      {this.dicoration,
      this.is_owner,
      this.rent_or_buy,
      this.payment_type_id,
      this.image1,
      this.image2,
      this.image3,
      this.furniture_type_id,
      this.sitting_room_furniture,
      this.bedroom_furniture,
      this.bathroom_furniture,
      this.kitchen_furniture,
      this.building_id,
      this.region_id,
      this.address,
      this.numOfRooms,
      this.price,
      this.floor,
      this.size,
      this.direction});

  HouseModel.fromJson(Map<String, dynamic> map) {
    price = int.parse(map['price'].toString());
    size = int.parse(map['size'].toString());
    region = map['region'].toString();
    direction = int.parse(map['direction'].toString());
    floor = int.parse(map['floor'].toString());
    rent_or_buy = int.parse(map['floor'].toString());
    dicoration = int.parse(map['dicoration'].toString());
    building_id = int.parse(map['building_number'].toString());
    user_name = map['user_name'].toString();
    image_1 = map['image_1'].toString();
    image_2 = map['image_2'].toString();
    image_3 = map['image_3'].toString();
    num_of_rooms = int.parse(map["num_of_rooms"].toString());
    payment_type = map["payment_type"].toString();
  }

  HouseModel.fromjsonUserInfo(Map<String, dynamic> map) {
    price = int.parse(map['price'].toString());
    created_date = map['created_date'].toString();
    image_1 = map['image_1'].toString();
    image_2 = map['image_2'].toString();
    image_3 = map['image_3'].toString();
  }

  RoomtoJson() {
    return {
      'dicoration': dicoration,
      'is_owner': is_owner,
      'rent_or_buy': rent_or_buy,
      'payment_type_id': payment_type_id,
      'furniture_type_id': furniture_type_id,
      'sitting_room_furniture': sitting_room_furniture,
      'bedroom_furniture': bedroom_furniture,
      'bathroom_furniture': bathroom_furniture,
      'kitchen_furniture': kitchen_furniture,
      'building_id': building_id,
      'address': address,
      'numOfRooms': numOfRooms,
      'price': price,
      'size': size,
      'direction': direction,
      'region_id': region_id,
      'floor': floor,
    };
  }
}
