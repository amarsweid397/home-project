class LandModel {
  String? land_type;
  int? region_id;
  int? rent_or_buy;
  int? max_size;
  int? min_size;
  int? min_price;
  int? max_price;
  int? direction;
  int? size;
  int? price;
  String? region;
  String? user_name;
  String? image_1;
  String? image_2;
  String? image_3;
  int? is_owner;
  int? payment_type_id;
  String? created_date;
  LandModel(
      {this.rent_or_buy,
      this.region_id,
      this.min_price,
      this.max_price,
      this.land_type,
      this.min_size,
      this.max_size,
      this.direction});
  LandModel.addLand(
      {this.land_type,
      this.is_owner,
      this.rent_or_buy,
      this.payment_type_id,
      this.region_id,
      this.region,
      this.price,
      this.size,
      this.direction});
  LandModel.fromJson(Map<String, dynamic> map) {
    price = int.parse(map['price'].toString());
    size = int.parse(map['size'].toString());
    region = map['region'].toString();
    land_type = (map[land_type].toString());
    user_name = map['user_name'].toString();
    image_1 = map['image_1'].toString();
    image_2 = map['image_2'].toString();
    image_3 = map['image_3'].toString();
  }
  LandModel.fromJsonUser(Map<String, dynamic> map) {
    price = int.parse(map['price'].toString());
    created_date = map['created_date'].toString();
    image_1 = map['image_1'].toString();
    image_2 = map['image_2'].toString();
    image_3 = map['image_3'].toString();
  }
  toJson() {
    return {
      'land_type': land_type,
      'is_owner': is_owner,
      'rent_or_buy': rent_or_buy,
      'payment_type_id': payment_type_id,
      'region_id': region_id,
      'address': region,
      'price': price,
      'size': size,
      'direction': direction,
    };
  }
}
