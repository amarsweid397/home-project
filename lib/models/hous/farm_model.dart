class FarmModel {
  int? direction;
  int? max_price;
  int? min_price;
  int? min_size;
  int? max_size;
  int? rent_or_buy;
  int? region_id;
  int? num_of_floors;
  int? price;
  String? created_date;
  // int? space;
  String? region;
  String? user_name;
  int? size;
  String? image_1;
  String? image_2;
  String? image_3;
  int? swimming_pool;
  int? is_owner;
  int? payment_type_id;
  int? furniture_type_id;
  int? bedroom_furniture;
  int? sitting_room_furniture;
  int? kitchen_furniture;
  int? bathroom_furniture;
  String? address;
  FarmModel(
      {this.direction,
      this.max_size,
      this.min_size,
      this.max_price,
      this.min_price,
      this.rent_or_buy,
      this.region_id,
      this.num_of_floors,
      this.user_name});

  FarmModel.addFarm({
    this.swimming_pool,
    this.is_owner,
    this.rent_or_buy,
    this.payment_type_id,
    this.furniture_type_id,
    this.bathroom_furniture,
    this.bedroom_furniture,
    this.sitting_room_furniture,
    this.kitchen_furniture,
    this.region_id,
    this.address,
    this.price,
    this.size,
    this.num_of_floors,
    this.direction,
  });
  FarmModel.addShop(
      {this.is_owner,
      this.rent_or_buy,
      this.payment_type_id,
      this.region_id,
      this.address,
      this.price,
      this.size,
      this.direction});

  FarmModel.fromJson(Map<String, dynamic> map) {
    direction = int.parse(map['direction'].toString());
    price = int.parse(map['price'].toString());
    region = map['region'].toString();
    num_of_floors = int.parse(map['num_of_floors'].toString());
    user_name = map['user_name'].toString();
    size = int.parse(map['size'].toString());
    image_1 = map['image_1'].toString();
    image_2 = map['image_2'].toString();
    image_3 = map['image_3'].toString();
    swimming_pool = int.parse(map['swimming_pool'].toString());
  }
  FarmModel.fromJsonFarmUser(Map<String, dynamic> map) {
    price = map['price'];
    created_date = map['created_date'].toString();
    image_1 = map['image_1'].toString();
    image_2 = map['image_2'].toString();
    image_3 = map['image_3'].toString();
  }
  FarmModel.fromjsonshop(Map<String, dynamic> map) {
    direction = int.parse(map['direction'].toString());
    price = int.parse(map['price'].toString());
    region = map['region'].toString();
    user_name = map['user_name'].toString();
    size = int.parse(map['size'].toString());
    image_1 = map['image_1'].toString();
    image_2 = map['image_2'].toString();
    image_3 = map['image_3'].toString();
  }
  FarmModel.fromJsonShopUser(Map<String, dynamic> map) {
    price = int.parse(map['price'].toString());
    created_date = map['created_date'].toString();
    image_1 = map['image_1'].toString();
    image_3 = map['image_3'].toString();
  }

  toJson() {
    return {
      'num_of_floors': num_of_floors,
      'swimming_pool': swimming_pool,
      'is_owner': is_owner,
      'payment_type_id': payment_type_id,
      'rent_or_buy': rent_or_buy,
      'furniture_type_id': furniture_type_id,
      'sitting_room_furniture': sitting_room_furniture,
      'bedroom_furniture': bedroom_furniture,
      'bathroom_furniture': bathroom_furniture,
      'kitchen_furniture': kitchen_furniture,
      'region_id': region_id,
      'address': address,
      'price': price,
      'size': size,
      'direction': direction,
    };
  }

  toJsonShop() {
    return {
      'is_owner': is_owner,
      'rent_or_buy': rent_or_buy,
      'payment_type_id': payment_type_id,
      'region_id': region_id,
      'address': address,
      'price': price,
      'size': size,
      'direction': direction,
    };
  }
}
