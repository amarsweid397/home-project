class ServicesModels {
  String? date;
  String? time;
  int? materials;
  int? num_of_employees;
  int? working_hours;
  int? repeat_time;
  ServicesModels(
      {this.date = '',
      this.time = '',
      this.materials = 0,
      this.num_of_employees = 0,
      this.repeat_time = 0,
      this.working_hours = 0});
  toJson() {
    return {
      'date': date,
      'time': time,
      'materials': materials,
      'num_of_employees': num_of_employees,
      'working_hours': working_hours,
      'repeat_time': repeat_time,
    };
  }
}
