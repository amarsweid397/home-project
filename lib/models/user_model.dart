class UserMModel {
  String? name;
  String? email;
  String? phone;
  String? password;
  String? c_password;

  UserMModel({
    required this.name,
    required this.email,
    this.password,
    this.c_password,
    this.phone,
  });

  UserMModel.fromJson(Map<String, dynamic> map) {
    name = map['name'].toString();
    email = map['email'].toString();
    password = map['password'].toString();
  }
  toJsonEmailPassword() {
    return {
      'name': name,
      'email': email,
    };
  }

  toJsonAllInfromation() {
    return {
      'name': name,
      'email': email,
      'password': password,
      'password_confirmation': c_password,
      'phone': phone,
    };
  }
}

class UserToken {
  String? name;
  String? email;
  String? phone;
  String? token;
  UserToken({this.name, this.email, this.token, this.phone});
  UserToken.fromJson(Map<String, dynamic> map) {
    name = map['name'];
    email = map['email'];
    token = map['token'];
    phone = map['phone'];
  }
  toJson() {
    return {
      'name': name,
      'email': email,
      'token': token,
    };
  }
}
