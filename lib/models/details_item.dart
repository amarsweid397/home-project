class DetailsItem {
  int? typeOfItem;
  int? price;
  String? space;
  String? title;
  String? destination;
  String? floorNumber;
  int? type;
  int? decor;
  int? buildingNumber;
  String? ownerName;
  String? landType;
  int? swimmingPool;
  DetailsItem(
      {this.typeOfItem,
      this.price,
      this.space,
      this.title,
      this.destination,
      this.floorNumber,
      this.type,
      this.decor,
      this.buildingNumber,
      this.ownerName,
      this.landType,
      this.swimmingPool});
  DetailsItem.fromJson(Map<String, dynamic> map) {}
}
