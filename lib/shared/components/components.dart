import 'package:card_swiper/card_swiper.dart';
import 'package:chips_choice_null_safety/chips_choice_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/models/hous/house_model.dart';

import '../../modules/realestate_app/Rent/home/home_details.dart';

Widget info({
  required String text,
  required String text1,
}) {
  return Padding(
    padding: const EdgeInsets.only(left: 15, right: 15),
    child: Row(children: [
      Text(text,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 23,
            color: Color.fromARGB(255, 23, 71, 81),
          )),
      const Spacer(),
      Expanded(
        flex: 2,
        child: Text(text1,
            textAlign: TextAlign.end,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 19,
              color: Color(0xFF2AA8C0),
            )),
      ),
    ]),
  );
}

Widget bild(
        {required String content,
        required IconData icon,
        required String images,
        required myff}) =>
    Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Card(
        child: ListTile(
          leading: CircleAvatar(
            radius: 30,
            backgroundImage: NetworkImage(images),
          ),
          title: Text(
            content,
            style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
          ),
          trailing: Icon(
            icon,
            color: const Color(0xFF2AA8C0),
            size: 20,
          ),
          onTap: myff,
        ),
      ),
    );

Widget textContract(
        {required String textService,
        required String textNameService,
        required String textNameDate,
        required String textDate,
        required String textNameTime,
        required String textTime,
        context}) =>
    Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(textService,
                style: const TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF2AA8C0))),
            SizedBox(
              width: 5,
            ),
            Text(textNameService,
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          children: [
            Text(textNameDate,
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF2AA8C0))),
            SizedBox(
              width: 5,
            ),
            Text(textDate,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          children: [
            Text(textNameTime,
                style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF2AA8C0))),
            SizedBox(
              width: 5,
            ),
            Text(textTime,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          ],
        ),
        MaterialButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Text(
              'Delete',
              style: TextStyle(fontSize: 23, color: Colors.red),
            ),
            height: 40,
            minWidth: 100,
            color: Colors.white,
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Row(
                        children: const [
                          Icon(
                            Icons.done,
                            color: Colors.green,
                          ),
                          Text(
                            'Delete Done',
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.green),
                          ),
                        ],
                      ),
                    );
                  });
            }),
      ],
    );
Widget mostVisited({
  String? text,
  String? text1,
  String? image,
  HouseModel? houseModel,
}) =>
    GestureDetector(
      onTap: () {
        Get.to(detailsHome(
          houseModel: houseModel!,
        ));
      },
      child: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image(
                image: NetworkImage(image!),
                width: double.infinity,
                height: 100,
                fit: BoxFit.fill,
              ),
              // Image.network(
              //   '${image}',
              //   width: 220,
              //   height: 100,
              // ),
              const SizedBox(
                height: 10,
              ),
              Text(
                text!,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF2AA8C0)),
              ),
              Row(
                children: [
                  Text(
                    text1!,
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 192, 65, 42)),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );

Widget defaultButton({
  required String text,
  required function,
  double radius = 15.0,
  bool isUpperCase = true,
}) =>
    Container(
      width: 120,
      height: 40,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radius),
        color: Colors.amber,
      ),
      child: MaterialButton(
        onPressed: function,
        child: Text(isUpperCase ? text.toUpperCase() : text,
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
      ),
    );

Widget defaultTextFormField(
        {required TextEditingController controller,
        required TextInputType type,
        required String label,
        required IconData prefix,
        required Color color,
        required Color color1,
        required Color color2,
        required Color color3,
        required Color color4,
        IconData? sufix,
        onSubmit,
        validate,
        ontap,
        isPasswordVisible,
        sufixPressed,
        obscureText}) =>
    TextFormField(
      validator: validate,
      controller: controller,
      onFieldSubmitted: onSubmit,
      keyboardType: type,
      onTap: ontap,
      style: TextStyle(color: color),
      decoration: InputDecoration(
        labelText: label,
        labelStyle: TextStyle(color: color1),
        prefixIcon: Icon(
          prefix,
          color: color2,
        ),
        suffixIcon: IconButton(
          icon: Icon(
            sufix,
            color: Colors.white,
          ),
          onPressed: () => sufixPressed,
        ),
        border: OutlineInputBorder(),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: color3, width: 1.6),
          borderRadius: BorderRadius.circular(20.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: color4, width: 1.6),
          borderRadius: BorderRadius.circular(20.0),
        ),
      ),
    );
Widget changeButton({
  required String text,
  required String text1,
  required Color color,
  required Color textcolor,
  required onpressed,
}) {
  return Padding(
    padding: const EdgeInsets.only(left: 15, top: 35, right: 10),
    child: MaterialButton(
      onPressed: onpressed,
      child: Row(
        children: [
          Text(text),
          Spacer(),
          Text(
            text1,
            style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            width: 4,
          ),
          Icon(
            Icons.arrow_forward_ios,
            size: 18,
          ),
        ],
      ),
      color: color,
      textColor: textcolor,
      height: 50,
    ),
  );
}

Padding Swiperpp(
    {List? images,
    required String imageSrc,
    required String content1,
    required IconData icon,
    required String content2,
    context}) {
  return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
      child: Column(children: [
        Stack(
          children: [
            Container(
              width: double.infinity,
              height: 200, // لل swiper
              child: Swiper(
                itemCount: 3,
                //autoplay: true,
                customLayoutOption:
                    CustomLayoutOption(startIndex: 0, stateCount: 3),
                autoplay: false,
                itemHeight: 1000.0,
                layout: SwiperLayout.STACK,
                pagination: const SwiperPagination(
                    margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 30.0),
                    builder: DotSwiperPaginationBuilder(
                        color: Colors.white30,
                        activeColor: Colors.white,
                        size: 10.0,
                        activeSize: 10.0)),
                scrollDirection: Axis.horizontal,
                itemWidth: double.infinity,
                loop: true,
                itemBuilder: (BuildContext context, int index) => ClipRRect(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10)),
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(images![index]),
                          fit: BoxFit.fill),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ]));
}

ElevatedButton tripBtn({
  required double size,
  required String content,
  required Color color,
  required myFunc,
  required double width,
  required double height,
  icon,
  textcolor,
}) {
  return ElevatedButton(
    style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(color),
        shape: MaterialStateProperty.all(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)))),
    child: Container(
      //padding: EdgeInsets.all(15),
      width: width,
      height: height,
      child: Column(
        children: [
          Icon(
            icon,
            size: 30,
            color: Colors.lightBlueAccent,
          ),
          Expanded(
            child: Text(
              content,
              style: TextStyle(color: textcolor, fontSize: size),
            ),
          ),
        ],
      ),
    ),
    onPressed: myFunc,
  );
}

Widget Line() {
  return Container(
    color: Colors.black,
    height: 1,
    width: double.infinity,
  );
}

Widget type({
  required String text,
  required myf,
  required IconData icon,
}) {
  return Padding(
    padding: const EdgeInsets.only(top: 20),
    child: Column(
      children: [
        MaterialButton(
          onPressed: () {
            myf();
          },
          minWidth: double.infinity,
          height: 50,
          color: Colors.white,
          textColor: Colors.black,
          child: Row(
            children: [
              Icon(
                icon,
                color: const Color(0xFF2AA8C0),
                size: 30,
              ),
              const SizedBox(
                width: 15,
              ),
              Expanded(
                child: Text(
                  text,
                  style: const TextStyle(
                      fontSize: 25, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    ),
  );
}

Widget Select(
    {required int tagg,
    required int val,
    required List<String> app,
    required ffc,
    required bool? wrap}) {
  return ChipsChoice.single(
    wrapped: wrap,
    value: tagg,
    onChanged: ffc,
    choiceItems: C2Choice.listFrom(
      source: app,
      value: (i, v) => i,
      label: (i, v) => v.toString(),
    ),
    choiceActiveStyle: const C2ChoiceStyle(
        color: Color(0xFF2AA8C0),
        borderColor: Color(0xFF2AA8C0),
        borderRadius: BorderRadius.all(Radius.circular(5))),
    choiceStyle: const C2ChoiceStyle(
        color: Color(0xFF2AA8C0),
        borderRadius: BorderRadius.all(Radius.circular(5))),
  );
}

Widget defaultText({
  required String text,
}) {
  return Padding(
    padding: const EdgeInsets.only(left: 15, top: 15),
    child: Text(
      text,
      style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 24,
          color: Color.fromARGB(255, 23, 71, 81)),
    ),
  );
}

ElevatedButton tripBtn2({
  required String content,
  required IconData icon,
  required Color color,
  required myFunc,
  required double radius,
  required Color iconcolor,
}) {
  return ElevatedButton(
    style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(color),
        shape: MaterialStateProperty.all(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius)))),
    child: Container(
      width: 110,
      height: 50,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        children: [
          Icon(
            icon,
            size: 25,
            color: iconcolor,
          ),
          SizedBox(
            width: 6,
          ),
          Text(
            content,
            style: TextStyle(color: Colors.white, fontSize: 24),
          ),
        ],
      ),
    ),
    onPressed: myFunc,
  );
}

TextButton PopularWidget(
    {required String name, required tapin, required IconData icon}) {
  return TextButton(
    onPressed: tapin,
    child: Container(
      width: 150,
      height: 70,
      color: Color.fromARGB(255, 234, 250, 250),
      child: Padding(
        padding: const EdgeInsets.only(left: 5, top: 10),
        child: Column(
          children: [
            Icon(icon, color: Color(0xFF2AA8C0)),
            Text(
              name,
              style: TextStyle(
                  fontSize: 19,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ],
        ),
      ),
    ),
  );
}

void navigaTor(context, Widget) =>
    Navigator.push(context, MaterialPageRoute(builder: (context) => Widget));

void navigaTAndFinish(context, Widget) => Navigator.pushAndRemoveUntil(
    context, MaterialPageRoute(builder: (context) => Widget), (route) => false);

Text errText(String msg) {
  return Text(msg,
      style: TextStyle(
          color: Colors.red, fontWeight: FontWeight.w700, fontSize: 18));
}
