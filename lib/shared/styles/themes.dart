import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'colors.dart';

ThemeData darkTheme =ThemeData(
        primarySwatch: defaultColor,
         scaffoldBackgroundColor: HexColor('333739'),
          appBarTheme: AppBarTheme(
            iconTheme: IconThemeData(color:Color(0xFF2AA8C0)),
            backwardsCompatibility: false,
            systemOverlayStyle:SystemUiOverlayStyle(
              statusBarColor:  HexColor('333739'),
              statusBarIconBrightness: Brightness.dark,
              
            ),
            textTheme: TextTheme(
              bodyText1: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w600,
                color: Colors.white,
                backgroundColor:  HexColor('333739')
              )
            ),
          backgroundColor:HexColor('333739'),
          elevation: 0,
          titleTextStyle: TextStyle(color:Colors.white,fontSize:20,fontWeight: FontWeight.bold),
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: HexColor('333739'),
          selectedItemColor: Color(0xFF2AA8C0),
          unselectedItemColor: Colors.grey,
          elevation: 20.0,
          type:BottomNavigationBarType.fixed ),
          fontFamily: 'Raleway',
        
       );
ThemeData lightTheme= ThemeData(
        primarySwatch:defaultColor,
          scaffoldBackgroundColor: Colors.white,
          floatingActionButtonTheme: FloatingActionButtonThemeData(
            backgroundColor: Color(0xFF2AA8C0)
          ),
          appBarTheme: AppBarTheme(
            iconTheme: IconThemeData(color:Color(0xFF2AA8C0)),
            backwardsCompatibility: false,
            systemOverlayStyle:SystemUiOverlayStyle(
              statusBarColor: Colors.white,
              statusBarIconBrightness: Brightness.dark,
              
            ),
          backgroundColor: Colors.white,
          elevation: 0,
          titleTextStyle: TextStyle(color:Colors.black,fontSize:20,fontWeight: FontWeight.bold)
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor:  Colors.white,
          selectedItemColor: Color(0xFF2AA8C0),
          unselectedItemColor: Colors.grey,
          elevation: 20.0,
          type:BottomNavigationBarType.fixed),
           textTheme: TextTheme(
              bodyText1: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w600,
                color: Colors.black
              )
            ),
        fontFamily: 'Raleway',
        );