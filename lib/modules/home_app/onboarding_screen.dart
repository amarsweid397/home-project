import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:lottie/lottie.dart';

import '../../models/usermodel.dart';
import '../../shared/components/components.dart';
import '../../shared/styles/colors.dart';
import 'login/home_login.dart';

class onBordingScreen extends StatefulWidget {
  @override
  State<onBordingScreen> createState() => _onBordingScreenState();
}

class _onBordingScreenState extends State<onBordingScreen> {
  var bordController = PageController();

  List<BoardingModel> bord = [
    BoardingModel(
        image: Lottie.network(
            'https://assets10.lottiefiles.com/private_files/lf30_p5tali1o.json',
            height: 300,
            fit: BoxFit.fill,
            width: double.infinity),
        title: 'Hello in our app',
        body: 'Be happy when you choose'),
    BoardingModel(
        image: Lottie.network(
            'https://assets10.lottiefiles.com/private_files/lf30_p5tali1o.json',
            height: 300,
            fit: BoxFit.fill,
            width: double.infinity),
        title: 'Ease of selection',
        body: 'You will find all your ideas'),
    BoardingModel(
        image: Lottie.network(
            'https://assets10.lottiefiles.com/private_files/lf30_p5tali1o.json',
            height: 300,
            fit: BoxFit.fill,
            width: double.infinity),
        title: 'Lets go',
        body: 'May your choice be successful'),
  ];
  bool isLast = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: [
            TextButton(
                onPressed: () {
                  navigaTAndFinish(context, homeLoginScreen());
                },
                child: Text('SKIP'))
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            children: [
              Expanded(
                child: PageView(
                  children: [
                    Column(
                      children: [
                        SizedBox(
                          height: 14,
                        ),
                        LottieBuilder.network(
                          'https://assets5.lottiefiles.com/private_files/lf30_nlydoi3a.json',
                          fit: BoxFit.cover,
                          width: double.infinity,
                        ),
                        SizedBox(
                          height: 50,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Text(
                            'Hello in our app',
                            style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF2AA8C0)),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 15.0),
                          child: Text(
                            'Be happy when you choose Homs',
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.green[900]),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        LottieBuilder.network(
                            'https://assets5.lottiefiles.com/packages/lf20_ppnztcyy.json',
                            fit: BoxFit.cover,
                            width: double.infinity),
                        SizedBox(
                          height: 25,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 15.0),
                          child: Text(
                            'Ease of selection',
                            style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF2AA8C0)),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 15.0),
                          child: Text(
                            'You will find all your ideas',
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.green[900]),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        LottieBuilder.network(
                            'https://assets5.lottiefiles.com/packages/lf20_4ilboxgz.json',
                            fit: BoxFit.cover,
                            width: double.infinity),
                        SizedBox(
                          height: 25,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 15.0),
                          child: Text(
                            'Lets go Guys',
                            style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF2AA8C0)),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 15.0),
                          child: Text(
                            'May your choice be successful',
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.green[900]),
                          ),
                        )
                      ],
                    ),
                  ],

                  controller: bordController,
                  // itemBuilder:((context, index) =>buildBordingItem(bord[index]) ),
                  // itemCount: bord.length,
                  onPageChanged: (index) {
                    if (index == bord.length - 1) {
                      setState(() {
                        isLast = true;
                      });
                    } else {
                      setState(() {
                        isLast = false;
                      });
                    }
                  },
                ),
              ),
              SizedBox(
                height: 35,
              ),
              Row(
                children: [
                  //  SmoothPageIndicator(controller:bordController ,
                  //   count: bord.length,effect: ExpandingDotsEffect(
                  //     dotColor:Color(0xFF2AA8C0),
                  //     activeDotColor:Color(0xFF2AA8C0),
                  //     dotHeight: 10,
                  //     expansionFactor: 4,
                  //     dotWidth: 10,
                  //     spacing: 5.0
                  //   ),),
                  Spacer(),
                  FloatingActionButton(
                    onPressed: () {
                      if (isLast) {
                        navigaTor(context, homeLoginScreen());
                      } else {
                        bordController.nextPage(
                            duration: Duration(
                              milliseconds: 750,
                            ),
                            curve: Curves.fastLinearToSlowEaseIn);
                      }
                    },
                    child: Icon(Icons.arrow_forward_ios),
                  )
                ],
              )
            ],
          ),
        ));
  }
}
