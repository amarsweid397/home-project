import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/controller/register.dart';
import 'package:house_app/layout/RealEstate_app/realestate_layout.dart';
import 'package:house_app/models/user_model.dart';
import 'package:house_app/shared/components/components.dart';
import 'package:lottie/lottie.dart';
import '../../../core/constant/app_strings.dart';
import '../../../models/usermodel.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool isErr = false;
  bool isPasswordVisible = true;
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var c_passwordController = TextEditingController();
  var nameController = TextEditingController();
  var numberController = TextEditingController();
  String nameErr = '';
  String numberErr = '';
  String emailErr = '';
  String passwordErr = '';
  String c_passwordErr = '';
  RegisterScreenController controller = Get.put(RegisterScreenController());
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: const AssetImage('assets/images/home4.png'),
            fit: BoxFit.fill,
            colorFilter: ColorFilter.mode(
              Colors.black.withOpacity(0.5),
              BlendMode.darken,
            ),
          ),
        ),
        child: GetBuilder<RegisterScreenController>(builder: (context) {
          return controller.loading
              ? Center(
                  child: Lottie.asset('assets/images/waiting.json'),
                )
              : Scaffold(
                  backgroundColor: Colors.transparent,
                  body: Center(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                           Padding(
                            padding: EdgeInsets.only(top: 10),
                            child: Text(
                               AppStrings.createAnAccount.tr,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 35.0,
                                color: Color(0xFF42aedc),
                                fontFamily: 'Courgette',
                              ),
                            ),
                          ),
                           Text(
                             AppStrings.welcomeWithUs.tr,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 25.0,
                              color: Color(0xFF42aedc),
                              fontFamily: 'Courgette',
                            ),
                          ),
                          //  SizedBox(height: 10.0,),
                          errText(controller.nameErr == null
                              ? ''
                              : controller.nameErr!),

                          Padding(
                              padding: const EdgeInsets.only(
                                left: 60,
                                right: 60,
                              ),
                              child: defaultTextFormField(
                                  color1: Colors.white,
                                  color2: Colors.white,
                                  color3: Colors.white,
                                  color4: Colors.white,
                                  color: Colors.white,
                                  controller: nameController,
                                  type: TextInputType.text,
                                  label:  AppStrings.name.tr,
                                  prefix: Icons.person)),
                          errText(controller.numberErr == null
                              ? ''
                              : controller.numberErr!),

                          Padding(
                              padding: const EdgeInsets.only(
                                left: 60,
                                right: 60,
                              ),
                              child: defaultTextFormField(
                                  color1: Colors.white,
                                  color2: Colors.white,
                                  color3: Colors.white,
                                  color4: Colors.white,
                                  color: Colors.white,
                                  controller: numberController,
                                  type: TextInputType.number,
                                  label:  AppStrings.phoneNumber.tr,
                                  prefix: Icons.phone)),
                          errText(controller.emailErr == null
                              ? ''
                              : controller.emailErr!),

                          Padding(
                              padding: const EdgeInsets.only(
                                left: 60,
                                right: 60,
                              ),
                              child: defaultTextFormField(
                                  color1: Colors.white,
                                  color2: Colors.white,
                                  color3: Colors.white,
                                  color4: Colors.white,
                                  color: Colors.white,
                                  controller: emailController,
                                  type: TextInputType.emailAddress,
                                  label:  AppStrings.emailAdress.tr,
                                  prefix: Icons.email)),
                          errText(controller.passErr == null
                              ? ''
                              : controller.passErr!),

                          Padding(
                            padding: const EdgeInsets.only(
                              left: 60,
                              right: 60,
                            ),
                            child: TextFormField(
                              controller: passwordController,
                              keyboardType: TextInputType.visiblePassword,
                              style: const TextStyle(
                                color: Colors.white,
                              ),
                              decoration: InputDecoration(
                                labelText:  AppStrings.password.tr,
                                labelStyle: const TextStyle(
                                  color: Colors.white70,
                                ),
                                border: const OutlineInputBorder(),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.white, width: 1.6),
                                    borderRadius: BorderRadius.circular(20.0)),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      color: Colors.white, width: 1.6),
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                prefixIcon: Icon(
                                  Icons.lock,
                                  color: Colors.white,
                                ),
                                suffixIcon: IconButton(
                                  icon: isPasswordVisible
                                      ? Icon(
                                          Icons.visibility_off,
                                          color: Colors.white,
                                        )
                                      : Icon(
                                          Icons.visibility,
                                          color: Colors.white,
                                        ),
                                  onPressed: () => setState(() =>
                                      isPasswordVisible = !isPasswordVisible),
                                ),
                              ),
                              obscureText: isPasswordVisible,
                            ),
                          ),
                          errText(controller.c_passwordErr == null
                              ? ''
                              : controller.c_passwordErr!),

                          Padding(
                            padding: const EdgeInsets.only(
                                left: 60, right: 60, bottom: 0),
                            child: TextFormField(
                              controller: c_passwordController,
                              keyboardType: TextInputType.visiblePassword,
                              style: TextStyle(
                                color: Colors.white,
                              ),
                              decoration: InputDecoration(
                                labelText:  AppStrings.confirmpassword.tr,
                                labelStyle: TextStyle(
                                  color: Colors.white70,
                                ),
                                border: OutlineInputBorder(),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 1.6),
                                    borderRadius: BorderRadius.circular(20.0)),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 1.6),
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                prefixIcon: Icon(
                                  Icons.lock,
                                  color: Colors.white,
                                ),
                                suffixIcon: IconButton(
                                  icon: isPasswordVisible
                                      ? Icon(
                                          Icons.visibility_off,
                                          color: Colors.white,
                                        )
                                      : Icon(
                                          Icons.visibility,
                                          color: Colors.white,
                                        ),
                                  onPressed: () => setState(() =>
                                      isPasswordVisible = !isPasswordVisible),
                                ),
                              ),
                              obscureText: isPasswordVisible,
                            ),
                          ),

                          SizedBox(
                            height: 12.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 20, bottom: 40),
                                child: tripBtn2(
                                    iconcolor: Colors.green,
                                    radius: 30,
                                    content:  AppStrings.done.tr,
                                    icon: Icons.check_circle,
                                    color: const Color(0xFF0C4E57),
                                    myFunc: () async {
                                      controller.Waiting();
                                      if (nameController.text == '') {
                                        print('name is Error here try again');
                                        controller.validateName(
                                             AppStrings.nameIsrequired.tr);
                                      }

                                      if (numberController.text == '') {
                                        controller.validateNumber(
                                            AppStrings.numberIsrequired.tr);
                                      }

                                      if (emailController.text == '') {
                                        controller.validateEmail(
                                            AppStrings.emailIsrequired.tr);
                                      }

                                      if (passwordController.text == '') {
                                        controller.validatePassword(
                                             AppStrings.passwordIsrequired.tr);
                                      }

                                      if (c_passwordController.text == '') {
                                        controller.validateConfirmPass(
                                            AppStrings.confirmYourpassword.tr);
                                      } else if (passwordController.text !=
                                          c_passwordController.text) {
                                        controller.validateConfirmPass(
                                            'Password dose not match   !!');
                                      }

                                      if (!isErr) {
                                        // var user = new User(
                                        //     name: nameController.text,
                                        //     email: emailController.text,
                                        //     password: passwordController.text,
                                        //     c_password:
                                        //         c_passwordController.text,
                                        //     number: numberController.text);
                                        UserMModel user = UserMModel(
                                            name: nameController.text,
                                            email: emailController.text,
                                            password: passwordController.text,
                                            phone: numberController.text,
                                            c_password:
                                                c_passwordController.text);
                                        controller.RegisterApp(user)
                                            .then((value) {
                                          if (value) {
                                            Get.off(RealEstateLayout());
                                            controller.Waiting();
                                          } else {
                                            controller.Waiting();
                                            Get.dialog(AlertDialog(
                                              backgroundColor:
                                                  const Color(0xFF0C4E57),
                                              contentPadding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 15,
                                                      vertical: 10),
                                              content: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.min,
                                                children: const [
                                                  Divider(
                                                    color: Colors.grey,
                                                    thickness: 2,
                                                  ),
                                                  Text(
                                                      'Account is already Exist')
                                                ],
                                              ),
                                              title:  Text(
                                                 AppStrings.error.tr,
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    color: Colors.red,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              actions: [
                                                TextButton(
                                                    onPressed: () {
                                                      Get.back();
                                                    },
                                                    child: const Text('Cancel'))
                                              ],
                                            ));
                                          }
                                        });
                                        // Navigator.of(context).push(MaterialPageRoute(
                                        //     builder: (context) => RealEstateLayout()));
                                      }
                                    }),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                );
        }));
  }
}
