import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:house_app/controller/login.dart';
import 'package:house_app/layout/RealEstate_app/realestate_layout.dart';
import 'package:house_app/models/user_model.dart';
import 'package:house_app/modules/home_app/register/register_screen.dart';
import 'package:lottie/lottie.dart';
import '../../../core/constant/app_strings.dart';
import '../../../models/usermodel.dart';
import '../../../shared/components/components.dart';

class homeLoginScreen extends StatefulWidget {
  const homeLoginScreen({Key? key}) : super(key: key);
  @override
  State<homeLoginScreen> createState() => _Test_LoginState();
}

class _Test_LoginState extends State<homeLoginScreen> {
  String emailErr = '';
  String passErr = '';
  bool isErr = false;
  //bool isPasswordVisible = true;
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    LoginScreenController controller = Get.put(LoginScreenController());

    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: const AssetImage('assets/images/home2.png'),
          //image: NetworkImage('https://i.pinimg.com/originals/a0/99/2a/a0992afa9c1f2b47db51d11f69c897f3.jpg'),
          fit: BoxFit.fill,
          colorFilter: ColorFilter.mode(
            Colors.black.withOpacity(0.4),
            BlendMode.darken,
          ),
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: SingleChildScrollView(
            child: GetBuilder<LoginScreenController>(builder: (context) {
              print('Getbuilder is working');
              print('value of loading is ${controller.loading}');

              return controller.loading
                  ? Center(
                      child: Lottie.asset('assets/images/waiting2.json'),
                    )
                  : Column(
                      children: [
                         Padding(
                          padding: EdgeInsets.only(top: 10, right: 10),
                          child: Text(
                            AppStrings.login.tr,
                            style: TextStyle(
                                fontWeight: FontWeight.w200,
                                fontSize: 90.0,
                                color: Color(0xFF2AA8C0),
                                fontFamily: 'Courgette'),
                          ),
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5.0),
                          child: Text(
                            controller.emailErr,
                            style: const TextStyle(color: Colors.red),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 60, right: 60, bottom: 2, top: 8),
                          child: defaultTextFormField(
                            color1: Colors.white,
                            color2: Colors.white,
                            color3: Colors.white,
                            color4: Colors.white,
                            color: Colors.white,
                            controller: emailController,
                            type: TextInputType.emailAddress,
                            label:AppStrings.emailAdress.tr,
                            prefix: Icons.email,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5.0),
                          child: Text(
                            controller.passErr,
                            style: const TextStyle(color: Colors.red),
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(
                                left: 60, right: 60, top: 2, bottom: 0),
                            child: TextFormField(
                              controller: passwordController,
                              keyboardType: TextInputType.visiblePassword,
                              style: const TextStyle(
                                color: Colors.white,
                              ),
                              decoration: InputDecoration(
                                labelText: AppStrings.password.tr,
                                labelStyle: const TextStyle(
                                  color: Colors.white70,
                                ),
                                border: const OutlineInputBorder(),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.white, width: 1.6),
                                    borderRadius: BorderRadius.circular(20.0)),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      color: Colors.white, width: 1.6),
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                prefixIcon: const Icon(
                                  Icons.lock,
                                  color: Colors.white,
                                ),
                                suffixIcon: IconButton(
                                    icon: GetBuilder<LoginScreenController>(
                                        builder: (context) {
                                      return controller.isPasswordVisible
                                          ? const Icon(
                                              Icons.visibility_off,
                                              color: Colors.white,
                                            )
                                          : const Icon(
                                              Icons.visibility,
                                              color: Colors.white,
                                            );
                                    }),
                                    onPressed: () =>
                                        controller.onchangeVisiblePassword()),
                              ),
                              obscureText: controller.isPasswordVisible,
                            )),
                        const SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: MaterialButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25)),
                              height: 50,
                              minWidth: 120,
                              color: const Color.fromARGB(255, 32, 108, 118),
                              onPressed: () async {
                                if (emailController.text == '') {
                                  controller.emailError( AppStrings.emailisempty.tr);
                                }
                                if (passwordController.text == '') {
                                  controller
                                      .passwordError( AppStrings.enteryourpassword.tr);
                                } else if (!isErr) {
                                  controller.Waiting();

                                  var user = new User(
                                    email: emailController.text,
                                    password: passwordController.text,
                                  );

                                  await controller
                                      .getdata(
                                          email: user.email!,
                                          password: user.password!)
                                      .then((value) {
                                    if (value) {
                                      Get.off(RealEstateLayout(),
                                          transition: Transition.fade,
                                          duration: const Duration(seconds: 1));
                                      controller.Waiting();
                                    } else {
                                      controller.Waiting();
                                      Get.dialog(
                                        AlertDialog(
                                          contentPadding:
                                              const EdgeInsets.symmetric(
                                                  horizontal: 15, vertical: 5),
                                          title:  Text(
                                           AppStrings.error.tr,
                                            style: TextStyle(color: Colors.red),
                                          ),
                                          content: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children:  [
                                              Divider(
                                                color: Colors.grey,
                                                thickness: 1,
                                              ),
                                              Text(
                                                   AppStrings.thereareErrorinEmailorPasswordpleaseCheck.tr),
                                            ],
                                          ),
                                          actions: [
                                            TextButton(
                                                onPressed: () {
                                                  Get.back();
                                                },
                                                child: const Text('Cancel'))
                                          ],
                                        ),
                                      );
                                    }
                                  });
                                  //Get.to(RealEstateLayout());
                                }
                              },
                              child:  Text(
                               AppStrings.signIn.tr,
                                style: TextStyle(
                                    fontSize: 23, color: Colors.white),
                              )),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                             Text(
                              AppStrings.donthaveanaacount.tr,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16),
                            ),
                            TextButton(
                              onPressed: () {
                                Get.to(const RegisterScreen());
                              },
                              child:  Text(
                               AppStrings.registerNow.tr,
                                style: TextStyle(
                                    color: Color(0xFF2AA8C0),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16),
                              ),
                            ),
                          ],
                        ),
                      ],
                    );
            }),
          ),
        ),
      ),
    );
  }

  dialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return  AlertDialog(
            title: Text(
             AppStrings.error.tr,
              style: TextStyle(
                  color: Colors.red, fontSize: 20, fontWeight: FontWeight.bold),
            ),
            content: Text( AppStrings.thereareErrorinEmailorPasswordpleaseCheck.tr),
          );
        });
  }
}
// Navigator.of(context).push(MaterialPageRoute(
//                                 builder: (context) => RealEstateLayout()));