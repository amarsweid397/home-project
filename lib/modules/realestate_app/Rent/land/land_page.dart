import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:house_app/modules/realestate_app/Rent/land/details_land.dart';
import 'package:house_app/shared/components/components.dart';
import 'package:lottie/lottie.dart';

import '../../../../controller/land_screen.dart';
import '../../../../core/constant/app_strings.dart';

class landPage extends StatefulWidget {
  const landPage({Key? key}) : super(key: key);

  @override
  State<landPage> createState() => _roomPageState();
}

class _roomPageState extends State<landPage> {
  @override
  Widget build(BuildContext context) {
    LandScreenController controller = Get.find<LandScreenController>();
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          title: Text(
            AppStrings.landsPage.tr,
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: const Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        // body: SingleChildScrollView(
        //     scrollDirection: Axis.vertical,
        //     child: ,
        //         InkWell(
        //           onTap: () {
        //             Get.to(landDetails());
        //           },
        //           child: Column(
        //               crossAxisAlignment: CrossAxisAlignment.start,
        //               children: [
        //                 Swiperpp(
        //                     imageSrc:
        //                         'https://picsum.photos/500/500?random=3',
        //                     content1: 'Rageen Place',
        //                     content2: 'Details :',
        //                     icon: Icons.details_rounded),
        //                 Padding(
        //                   padding: const EdgeInsets.only(
        //                       left: 15, bottom: 10, top: 15),
        //                   child: Text('2,700,000 SYP',
        //                       style: TextStyle(
        //                         fontWeight: FontWeight.bold,
        //                         fontSize: 25,
        //                         color: Colors.red,
        //                       )),
        //                 ),
        //                 Padding(
        //                   padding:
        //                       const EdgeInsets.only(left: 15, bottom: 10),
        //                   child: Text('Martini Area,Shahba Rose',
        //                       style: TextStyle(
        //                         fontWeight: FontWeight.bold,
        //                         fontSize: 23,
        //                         color: Colors.black,
        //                       )),
        //                 ),
        //                 Padding(
        //                   padding:
        //                       const EdgeInsets.only(left: 10, bottom: 15),
        //                   child: tripBtn2(
        //                       content: 'CALL',
        //                       icon: Icons.phone,
        //                       color: Color(0xFF2AA8C0),
        //                       myFunc: () {
        //                         showDialog(
        //                             context: context,
        //                             builder: (BuildContext context) {
        //                               return AlertDialog(
        //                                 title: Text(
        //                                   'The Phone number: 0991022354',
        //                                   style: TextStyle(
        //                                       fontSize: 20,
        //                                       fontWeight: FontWeight.bold,
        //                                       color: Color(0xFF2AA8C0)),
        //                                 ),
        //                               );
        //                             });
        //                       },
        //                       radius: 10,
        //                       iconcolor: Colors.white),
        //                 ),
        //                 Line()
        //               ]),
        //         ),
        //         SizedBox(
        //           height: 15,
        //         ),
        //         InkWell(
        //           onTap: () {
        //             Get.to(landDetails());
        //           },
        //           child: Column(
        //               crossAxisAlignment: CrossAxisAlignment.start,
        //               children: [
        //                 Swiperpp(
        //                     imageSrc:
        //                         'https://picsum.photos/500/500?random=4',
        //                     content1: 'Rageen Place',
        //                     content2: 'Details :',
        //                     icon: Icons.details_rounded),
        //                 Padding(
        //                   padding: const EdgeInsets.only(
        //                       left: 15, bottom: 10, top: 15),
        //                   child: Text('2,700,000 SYP',
        //                       style: TextStyle(
        //                         fontWeight: FontWeight.bold,
        //                         fontSize: 25,
        //                         color: Colors.red,
        //                       )),
        //                 ),
        //                 Padding(
        //                   padding:
        //                       const EdgeInsets.only(left: 15, bottom: 10),
        //                   child: Text('Martini Area,Shahba Rose',
        //                       style: TextStyle(
        //                         fontWeight: FontWeight.bold,
        //                         fontSize: 23,
        //                         color: Colors.black,
        //                       )),
        //                 ),
        //                 Padding(
        //                   padding:
        //                       const EdgeInsets.only(left: 10, bottom: 15),
        //                   child: tripBtn2(
        //                       content: 'CALL',
        //                       icon: Icons.phone,
        //                       color: Color(0xFF2AA8C0),
        //                       myFunc: () {
        //                         showDialog(
        //                             context: context,
        //                             builder: (BuildContext context) {
        //                               return AlertDialog(
        //                                 title: Text(
        //                                   'The Phone number: 0991022354',
        //                                   style: TextStyle(
        //                                       fontSize: 20,
        //                                       fontWeight: FontWeight.bold,
        //                                       color: Color(0xFF2AA8C0)),
        //                                 ),
        //                               );
        //                             });
        //                       },
        //                       radius: 10,
        //                       iconcolor: Colors.white),
        //                 ),
        //                 Line()
        //               ]),
        //         ),
        //         SizedBox(
        //           height: 15,
        //         ),
        //         InkWell(
        //           onTap: () {
        //             Get.to(landDetails());
        //           },
        //           child: Column(
        //               crossAxisAlignment: CrossAxisAlignment.start,
        //               children: [
        //                 Swiperpp(
        //                     imageSrc:
        //                         'https://picsum.photos/500/500?random=5',
        //                     content1: 'Rageen Place',
        //                     content2: 'Details :',
        //                     icon: Icons.details_rounded),
        //                 Padding(
        //                   padding: const EdgeInsets.only(
        //                       left: 15, bottom: 10, top: 15),
        //                   child: Text('2,700,000 SYP',
        //                       style: TextStyle(
        //                         fontWeight: FontWeight.bold,
        //                         fontSize: 25,
        //                         color: Colors.red,
        //                       )),
        //                 ),
        //                 Padding(
        //                   padding:
        //                       const EdgeInsets.only(left: 15, bottom: 10),
        //                   child: Text('Martini Area,Shahba Rose',
        //                       style: TextStyle(
        //                         fontWeight: FontWeight.bold,
        //                         fontSize: 23,
        //                         color: Colors.black,
        //                       )),
        //                 ),
        //                 Padding(
        //                   padding:
        //                       const EdgeInsets.only(left: 10, bottom: 15),
        //                   child: tripBtn2(
        //                       content: 'CALL',
        //                       icon: Icons.phone,
        //                       color: Color(0xFF2AA8C0),
        //                       myFunc: () {
        //                         showDialog(
        //                             context: context,
        //                             builder: (BuildContext context) {
        //                               return AlertDialog(
        //                                 title: Text(
        //                                   'The Phone number: 0991022354',
        //                                   style: TextStyle(
        //                                       fontSize: 20,
        //                                       fontWeight: FontWeight.bold,
        //                                       color: Color(0xFF2AA8C0)),
        //                                 ),
        //                               );
        //                             });
        //                       },
        //                       radius: 10,
        //                       iconcolor: Colors.white),
        //                 ),
        //                 Line()
        //               ]),
        //         ),
        //       ],
        //     )),
        body: SafeArea(
          child: GetBuilder<LandScreenController>(builder: (context) {
            print('the value of land is ${controller.ListOfLands.length}');
            return controller.ListOfLands.isEmpty
                ? Center(
                    child: Column(
                      children: [
                        Lottie.asset('assets/images/error_result.json',
                            width: 350, height: 350),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text(
                          'No Lands Exist With This Details Yet!!',
                          style: TextStyle(
                            fontSize: 17,
                            color: Color(0xFF2AA8C0),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Icon(
                          Icons.sentiment_dissatisfied_outlined,
                          color: Color(0xFF2AA8C0),
                          size: 32,
                        )
                      ],
                    ),
                  )
                : ListView.builder(
                    itemCount: controller.ListOfLands.length,
                    itemBuilder: (context, index) {
                      print('bakari ri and mayar aweja ');
                      List<String> images = [
                        controller.ListOfLands[index].image_1!,
                        controller.ListOfLands[index].image_2!,
                        controller.ListOfLands[index].image_3!
                      ];
                      return Column(children: [
                        InkWell(
                          onTap: () {
                            Get.to(landDetails(
                              landModel: controller.ListOfLands[index],
                            ));
                          },
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Swiperpp(
                                    images: images,
                                    imageSrc:
                                        controller.ListOfLands[index].image_1!,
                                    content1: 'Rageen Place',
                                    content2: 'Details :',
                                    icon: Icons.details_rounded),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, bottom: 10, top: 15),
                                  child: Text(
                                      '${controller.ListOfLands[index].price!} SYP',
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25,
                                        color: Colors.red,
                                      )),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, bottom: 10),
                                  child: Text(
                                      controller.ListOfLands[index].region!,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 23,
                                        color: Colors.black,
                                      )),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, bottom: 15),
                                  child: tripBtn2(
                                      content:  AppStrings.call.tr,
                                      icon: Icons.phone,
                                      color: const Color(0xFF2AA8C0),
                                      myFunc: () {
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return const AlertDialog(
                                                title: Text(
                                                  'The Phone number: 0991022354',
                                                  style: TextStyle(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Color(0xFF2AA8C0)),
                                                ),
                                              );
                                            });
                                      },
                                      radius: 10,
                                      iconcolor: Colors.white),
                                ),
                                Line()
                              ]),
                        ),
                        const SizedBox(
                          height: 15,
                        )
                      ]);
                    });
          }),
        ));
  }
}
