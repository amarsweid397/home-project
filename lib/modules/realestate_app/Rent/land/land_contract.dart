import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:house_app/controller/land_screen.dart';
import 'package:house_app/core/constant/images.dart';
import 'package:house_app/data/static/static.dart';

import 'package:house_app/models/hous/land_model.dart';
import 'package:house_app/modules/realestate_app/Rent/land/land_page.dart';
import 'package:house_app/shared/components/components.dart';
import 'package:lottie/lottie.dart';

import '../../../../core/constant/app_strings.dart';

class landContract extends StatefulWidget {
  const landContract({Key? key}) : super(key: key);

  @override
  State<landContract> createState() => _roomContractState();
}

class _roomContractState extends State<landContract> {
  TextEditingController? nameController = TextEditingController();
  TextEditingController? priceController = TextEditingController();
  TextEditingController? priceController1 = TextEditingController();
  TextEditingController? spaceController = TextEditingController();
  TextEditingController? spaceController1 = TextEditingController();
  String name = '';
  int? regionId;
  String? typelandI;
  int? directions;
  bool isname = false;
  var tag1 = -1;
  var tag = -1;
  List<String> destination = [
    AppStrings.south.tr,
    AppStrings.western.tr,
    AppStrings.east.tr,
    AppStrings.north.tr,
    AppStrings.southWestern.tr,
    AppStrings.northWestern.tr,
    AppStrings.southEast.tr,
    AppStrings.northEast.tr,
  ];
  List<String> type = [
      AppStrings.agricultural.tr,
     AppStrings.building.tr,
  ];
  String dropdownvalue =  AppStrings.chooseYourarea.tr;
  var items = [
  AppStrings.chooseYourarea.tr,
    AppStrings.alFurkan.tr,
    AppStrings.newAleppo.tr,
    AppStrings.alSabil.tr,
    AppStrings.alSabil.tr,
    AppStrings.almuhandesen.tr,
    AppStrings.alsalhen.tr,
    AppStrings.almartini.tr,
    AppStrings.newShahbaa.tr,
    AppStrings.oldShahbaa.tr,
    AppStrings.almidan.tr,
    AppStrings.sayfAldawla.tr,
    AppStrings.babAlhadid.tr,
   AppStrings.salahAldiyn.tr,
    AppStrings.babAlnaser.tr,
    AppStrings.alshaar.tr,
  ];
  @override
  Widget build(BuildContext context) {
    //RoomScreenController room = Get.put(RoomScreenController());
    LandScreenController controller = Get.put(LandScreenController());
    print('the value of land controller is ${controller.typeofland}');
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppStrings.landContract.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: const Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: GetBuilder<LandScreenController>(builder: (context) {
        return controller.loading
            ? Center(
                child: Lottie.asset(AppImages.LoadingImages),
              )
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    defaultText(text:  AppStrings.location.tr),
                    const SizedBox(
                      height: 25,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: DropdownButton(
                        style: TextStyle(
                            color: Color(0xFF2AA8C0),
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                        // Initial Value
                        value: controller.region,

                        // Down Arrow Icon
                        icon: const Icon(Icons.keyboard_arrow_down,
                            color: Colors.black, size: 30),

                        // Array list of items
                        items: items.map((String items) {
                          return DropdownMenuItem(
                            value: items,
                            child: Text(items),
                          );
                        }).toList(),
                        // After selecting the desired option,it will
                        // change button value to selected value
                        onChanged: (String? newValue) async {
                          setState(() {
                            controller.changeRegion(newValue!);
                            int index = items
                                .indexWhere((element) => element == newValue);
                            if (index == 0) {
                              regionId = null;
                            } else {
                              regionId = index;
                            }
                          });
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    defaultText(text:  AppStrings.price.tr),
                    const SizedBox(
                      height: 25,
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 15),
                          child: SizedBox(
                            width: 150,
                            child: defaultTextFormField(
                                color1: Colors.black,
                                color2: const Color(0xFF2AA8C0),
                                color3: const Color(0xFF2AA8C0),
                                color4: const Color(0xFF2AA8C0),
                                color: Colors.black,
                                controller: priceController!,
                                type: TextInputType.number,
                                label: 'SYP',
                                prefix: Icons.price_check),
                          ),
                        ),
                        defaultText(text:  AppStrings.to.tr),
                        Padding(
                          padding: const EdgeInsets.only(left: 15),
                          child: SizedBox(
                            width: 150,
                            child: defaultTextFormField(
                                color1: Colors.black,
                                color2: Color(0xFF2AA8C0),
                                color3: Color(0xFF2AA8C0),
                                color4: Color(0xFF2AA8C0),
                                color: Colors.black,
                                controller: priceController1!,
                                type: TextInputType.number,
                                label: 'SYP',
                                prefix: Icons.price_check),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    defaultText(text:  AppStrings.landType.tr),
                    const SizedBox(
                      height: 15,
                    ),
                    Select(
                        tagg: controller.typeofland,
                        wrap: false,
                        val: -1,
                        app: type,
                        ffc: (val) {
                          setState(() {
                            tag = val;
                            controller.selectType(val);
                            typelandI = val.toString();
                            //print('the value of landTType id is ${typelandId}');
                          });
                        }),
                    const SizedBox(
                      height: 5,
                    ),
                    defaultText(text:  AppStrings.landSpace.tr),
                    const SizedBox(
                      height: 25,
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 15),
                          child: SizedBox(
                            width: 150,
                            child: defaultTextFormField(
                                color1: Colors.black,
                                color2: Color(0xFF2AA8C0),
                                color3: Color(0xFF2AA8C0),
                                color4: Color(0xFF2AA8C0),
                                color: Colors.black,
                                controller: spaceController!,
                                type: TextInputType.number,
                                label:  AppStrings.squaremeters.tr,
                                prefix: Icons.space_dashboard_outlined),
                          ),
                        ),
                        defaultText(text: 'To'),
                        Padding(
                          padding: const EdgeInsets.only(left: 15),
                          child: SizedBox(
                              width: 150,
                              child: defaultTextFormField(
                                  color1: Colors.black,
                                  color2: Color(0xFF2AA8C0),
                                  color3: Color(0xFF2AA8C0),
                                  color4: Color(0xFF2AA8C0),
                                  color: Colors.black,
                                  controller: spaceController1!,
                                  type: TextInputType.number,
                                  label: AppStrings.squaremeters.tr,
                                  prefix: Icons.space_dashboard_outlined)),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    defaultText(text:  AppStrings.landDestination.tr),
                    const SizedBox(
                      height: 15,
                    ),
                    Select(
                        tagg: controller.direction,
                        wrap: false,
                        val: -1,
                        app: destination,
                        ffc: (val) {
                          setState(() {
                            tag1 = val;
                            controller.selectDirection(val);
                            directions = val;
                            print(
                                'the dirction from Getx is ${controller.direction}  and from state is ${directions}');
                          });
                        }),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: changeButton(
                          text:  AppStrings.showTheResult.tr,
                          text1:  AppStrings.now.tr,
                          color: Colors.amber,
                          textcolor: Colors.black,
                          onpressed: () {
                            print('bakri aweja');
                            controller.Waiting();
                            controller
                                .getAllLand(LandModel(
                              rent_or_buy: rent_buy,
                              region_id: regionId,
                              direction: directions,
                              land_type: typelandI,
                              min_price: priceController!.text == ''
                                  ? null
                                  : int.parse(priceController!.text),
                              max_price: priceController1!.text == ''
                                  ? null
                                  : int.parse(priceController1!.text),
                              min_size: spaceController!.text == ''
                                  ? null
                                  : int.parse(spaceController!.text),
                              max_size: spaceController1!.text == ''
                                  ? null
                                  : int.parse(spaceController1!.text),
                            ))
                                .then((value) {
                              if (value) {
                                Get.to(landPage(),
                                    transition: Transition.upToDown);
                                controller.Waiting();
                              } else {
                                controller.Waiting();
                                Get.dialog(AlertDialog(
                                  title:  Text(
                                     AppStrings.error.tr,
                                    style: TextStyle(
                                        fontSize: 20, color: Colors.red),
                                  ),
                                  content:  Text(
                                       AppStrings.pleasecheckfromconnectionInternet.tr),
                                  actions: [
                                    TextButton(
                                        onPressed: () {
                                          Get.back();
                                        },
                                        child: const Text('Cancel'))
                                  ],
                                ));
                              }
                            });

                            //  controller.getAllLand(LandModel(region_id: re));
                            // Navigator.of(context).push(
                            //     MaterialPageRoute(builder: (context) => landPage()));
                          }),
                    ),
                  ],
                ),
              );
      }),
      // body: SingleChildScrollView(
      //   child: Column(
      //     crossAxisAlignment: CrossAxisAlignment.start,
      //     children: [
      //       defaultText(text: 'Location :'),
      //     const   SizedBox(
      //         height: 25,
      //       ),
      //       Padding(
      //         padding: const EdgeInsets.symmetric(horizontal: 15),
      //         child: DropdownButton(
      //           style: TextStyle(
      //               color: Color(0xFF2AA8C0),
      //               fontSize: 20,
      //               fontWeight: FontWeight.bold),
      //           // Initial Value
      //           value: dropdownvalue,

      //           // Down Arrow Icon
      //           icon: const Icon(Icons.keyboard_arrow_down,
      //               color: Colors.black, size: 30),

      //           // Array list of items
      //           items: items.map((String items) {
      //             return DropdownMenuItem(
      //               value: items,
      //               child: Text(items),
      //             );
      //           }).toList(),
      //           // After selecting the desired option,it will
      //           // change button value to selected value
      //           onChanged: (String? newValue) async {
      //             setState(() {
      //               dropdownvalue = newValue!
      //             });
      //           },
      //         ),
      //       ),
      //       SizedBox(
      //         height: 15,
      //       ),
      //       defaultText(text: 'Price (SYP) :'),
      //       SizedBox(
      //         height: 25,
      //       ),
      //       Row(
      //         children: [
      //           Padding(
      //             padding: const EdgeInsets.only(left: 15),
      //             child: Container(
      //               width: 150,
      //               child: defaultTextFormField(
      //                   color1: Colors.black,
      //                   color2: Color(0xFF2AA8C0),
      //                   color3: Color(0xFF2AA8C0),
      //                   color4: Color(0xFF2AA8C0),
      //                   color: Colors.black,
      //                   controller: priceController,
      //                   type: TextInputType.number,
      //                   label: 'SYP',
      //                   prefix: Icons.price_check),
      //             ),
      //           ),
      //           defaultText(text: 'To'),
      //           Padding(
      //             padding: const EdgeInsets.only(left: 15),
      //             child: Container(
      //               width: 150,
      //               child: defaultTextFormField(
      //                   color1: Colors.black,
      //                   color2: Color(0xFF2AA8C0),
      //                   color3: Color(0xFF2AA8C0),
      //                   color4: Color(0xFF2AA8C0),
      //                   color: Colors.black,
      //                   controller: priceController1,
      //                   type: TextInputType.number,
      //                   label: 'SYP',
      //                   prefix: Icons.price_check),
      //             ),
      //           )
      //         ],
      //       ),
      //       SizedBox(
      //         height: 15,
      //       ),
      //       defaultText(text: 'Land Type :'),
      //       SizedBox(
      //         height: 15,
      //       ),
      //       Select(
      //           tagg: tag,
      //           wrap: false,
      //           val: -1,
      //           app: type,
      //           ffc: (val) {
      //             setState(() {
      //               tag = val;
      //             });
      //           }),
      //       SizedBox(
      //         height: 5,
      //       ),
      //       defaultText(text: 'Land Space :'),
      //       SizedBox(
      //         height: 25,
      //       ),
      //       Row(
      //         children: [
      //           Padding(
      //             padding: const EdgeInsets.only(left: 15),
      //             child: Container(
      //               width: 150,
      //               child: defaultTextFormField(
      //                   color1: Colors.black,
      //                   color2: Color(0xFF2AA8C0),
      //                   color3: Color(0xFF2AA8C0),
      //                   color4: Color(0xFF2AA8C0),
      //                   color: Colors.black,
      //                   controller: spaceController,
      //                   type: TextInputType.number,
      //                   label: 'square foot',
      //                   prefix: Icons.space_dashboard_outlined),
      //             ),
      //           ),
      //           defaultText(text: 'To'),
      //           Padding(
      //             padding: const EdgeInsets.only(left: 15),
      //             child: Container(
      //                 width: 150,
      //                 child: defaultTextFormField(
      //                     color1: Colors.black,
      //                     color2: Color(0xFF2AA8C0),
      //                     color3: Color(0xFF2AA8C0),
      //                     color4: Color(0xFF2AA8C0),
      //                     color: Colors.black,
      //                     controller: spaceController1,
      //                     type: TextInputType.number,
      //                     label: 'square foot',
      //                     prefix: Icons.space_dashboard_outlined)),
      //           )
      //         ],
      //       ),
      //       SizedBox(
      //         height: 15,
      //       ),
      //       defaultText(text: 'Land Destination :'),
      //       SizedBox(
      //         height: 15,
      //       ),
      //       Select(
      //           tagg: tag1,
      //           wrap: false,
      //           val: -1,
      //           app: destination,
      //           ffc: (val) {
      //             setState(() {
      //               tag1 = val;
      //             });
      //           }),
      //       Padding(
      //         padding: const EdgeInsets.only(bottom: 15),
      //         child: changeButton(
      //             text: 'Show the result',
      //             text1: 'now',
      //             color: Colors.amber,
      //             textcolor: Colors.black,
      //             onpressed: () {
      //               controller.getAllLand(LandModel(region_id: ));
      //               // Navigator.of(context).push(
      //               //     MaterialPageRoute(builder: (context) => landPage()));
      //             }),
      //       ),
      //     ],
      //   ),
      // ),
    );
  }
}
