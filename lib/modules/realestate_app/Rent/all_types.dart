import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/modules/realestate_app/Rent/farm/farm_contract.dart';
import 'package:house_app/modules/realestate_app/Rent/home/home_types.dart';
import 'package:house_app/modules/realestate_app/Rent/land/land_contract.dart';
import 'package:house_app/modules/realestate_app/Rent/shop/shope_types.dart';
import 'package:lottie/lottie.dart';
import '../../../core/constant/app_strings.dart';
import '../../../shared/components/components.dart';

class RenttypesScreen extends StatefulWidget {
  const RenttypesScreen({Key? key}) : super(key: key);

  @override
  State<RenttypesScreen> createState() => _cleaningTypesState();
}

class _cleaningTypesState extends State<RenttypesScreen> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
             AppStrings.rentTypes.tr,
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: ListView(
          children: [
            type(
                text:  AppStrings.houseForRent.tr,
                myf: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => homeTypes()));
                },
                icon: Icons.home),
            type(
                text:  AppStrings.landForRent.tr,
                myf: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => landContract()));
                },
                icon: Icons.landscape),
            type(
              text:  AppStrings.farmForRent.tr,
              myf: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => farmContract()));
              },
              icon: Icons.maps_home_work_outlined,
            ),
            type(
                text:  AppStrings.shopForRent.tr,
                myf: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => shopTypes()));
                },
                icon: Icons.home_outlined)
          ],
        ));
  }
}
