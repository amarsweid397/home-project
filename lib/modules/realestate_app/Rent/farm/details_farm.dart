import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';

import 'package:house_app/models/hous/farm_model.dart';
import 'package:house_app/shared/components/components.dart';

import '../../../../core/constant/app_strings.dart';

class farmDetails extends StatelessWidget {
  FarmModel farmModel;
  farmDetails({required this.farmModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> images = [
      farmModel.image_1!,
      farmModel.image_2!,
      farmModel.image_3!
    ];

    return Scaffold(
      appBar: AppBar(
        title:  Text(
         AppStrings.farmDetails.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: const Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Swiperpp(
              images: images,
              imageSrc: farmModel.image_1!,
              content1: 'Rageen Place',
              content2: 'Details :',
              icon: Icons.details_rounded),
          Padding(
            padding: const EdgeInsets.only(left: 15, bottom: 10, top: 15),
            child: Text('${farmModel.price} SYP',
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                  color: Colors.red,
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15, bottom: 10),
            child: Text('${farmModel.region}',
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 23,
                  color: Color(0xFF2AA8C0),
                )),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 10,
            width: double.infinity,
            color: Colors.grey[300],
          ),
          const SizedBox(
            height: 20,
          ),
          defaultText(text: AppStrings.propertyInformation.tr),
          const SizedBox(
            height: 20,
          ),
          info(
            text: AppStrings.price.tr,
            text1: '${farmModel.price} SYP',
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.space.tr,
            text1: '${farmModel.size} fs',
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.title.tr,
            text1: '${farmModel.region}',
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.farmDestination.tr,
            text1: dirct(farmModel.direction!),
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.swimmingPool.tr,
            text1: '${farmModel.swimming_pool == 1 ? 'Yes' : 'NO'}',
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.floorNumber.tr,
            text1: '${farmModel.num_of_floors}',
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.ownerName.tr,
            text1: '${farmModel.user_name}',
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 15),
            child: Center(
              child: tripBtn2(
                  content: AppStrings.call.tr,
                  icon: Icons.phone,
                  color: const Color(0xFF2AA8C0),
                  myFunc: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return const AlertDialog(
                            title: Text(
                              'The Phone number: 0991022354',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF2AA8C0)),
                            ),
                          );
                        });
                  },
                  radius: 10,
                  iconcolor: Colors.white),
            ),
          ),
        ]),
      ),
    );
  }

  String dirct(int index) {
    late String ans;
    for (int i = 0; i < Direction.length; i++) {
      if (i == index) {
        ans = Direction[i];
      }
    }
    return ans;
  }
}
