import 'package:house_app/controller/farm_screen.dart';
import 'package:house_app/modules/realestate_app/Rent/farm/details_farm.dart';
import 'package:house_app/shared/components/components.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import '../../../../core/constant/app_strings.dart';

class farmPage extends StatelessWidget {
  const farmPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    FarmController controller = Get.find<FarmController>();
    return Scaffold(
        appBar: AppBar(
          title:  Text(
           AppStrings.farmsPage.tr,
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: GetBuilder<FarmController>(builder: (context) {
          return controller.listOfFarms.isEmpty
              ? Center(
                  child: Column(
                    children: [
                      Lottie.asset('assets/images/error_result.json',
                          width: 350, height: 350),
                      const SizedBox(
                        height: 10,
                      ),
                       Text(
                         AppStrings.noFarmsExistWithThisDetailsYet.tr,
                        style: TextStyle(
                          fontSize: 17,
                          color: Color(0xFF2AA8C0),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Icon(
                        Icons.sentiment_dissatisfied_outlined,
                        color: Color(0xFF2AA8C0),
                        size: 32,
                      )
                    ],
                  ),
                )
              : ListView.builder(
                  itemCount: controller.listOfFarms.length,
                  itemBuilder: ((context, index) {
                    List<String> images = [
                      controller.listOfFarms[index].image_1!,
                      controller.listOfFarms[index].image_2!,
                      controller.listOfFarms[index].image_3!,
                    ];
                    return SingleChildScrollView(
                        //scrollDirection: Axis.vertical,
                        child: Column(
                      children: [
                        InkWell(
                          onTap: () {
                            Get.to(farmDetails(
                              farmModel: controller.listOfFarms[index],
                            ));
                          },
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Swiperpp(
                                    images: images,
                                    imageSrc:
                                        controller.listOfFarms[index].image_1!,
                                    content1: 'Rageen Place',
                                    content2: 'Details :',
                                    icon: Icons.details_rounded),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, bottom: 10, top: 15),
                                  child: Text(
                                      '${controller.listOfFarms[index].price} SYP',
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25,
                                        color: Colors.red,
                                      )),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 15, bottom: 10),
                                  child: Text(
                                      controller.listOfFarms[index].region!,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 23,
                                        color: Colors.black,
                                      )),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, bottom: 15),
                                  child: tripBtn2(
                                      content: AppStrings.call.tr,
                                      icon: Icons.phone,
                                      color: const Color(0xFF2AA8C0),
                                      myFunc: () {
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return const AlertDialog(
                                                title: Text(
                                                  'The Phone number: 0991022354',
                                                  style: TextStyle(
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Color(0xFF2AA8C0)),
                                                ),
                                              );
                                            });
                                      },
                                      radius: 10,
                                      iconcolor: Colors.white),
                                ),
                                Line()
                              ]),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                      ],
                    ));
                  }),
                );
        }));
  }
}
