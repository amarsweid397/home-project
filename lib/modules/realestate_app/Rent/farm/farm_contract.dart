import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/controller/farm_screen.dart';
import 'package:house_app/core/constant/images.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/models/hous/farm_model.dart';
import 'package:house_app/modules/realestate_app/Rent/farm/farm_page.dart';
import 'package:house_app/shared/components/components.dart';
import 'package:lottie/lottie.dart';

import '../../../../core/constant/app_strings.dart';

class farmContract extends StatefulWidget {
  const farmContract({Key? key}) : super(key: key);

  @override
  State<farmContract> createState() => _roomContractState();
}

class _roomContractState extends State<farmContract> {
  var nameController = TextEditingController();
  var priceController = TextEditingController();
  var priceController1 = TextEditingController();
  var spaceController = TextEditingController();
  var spaceController1 = TextEditingController();
  String name = '';
  bool isname = false;
  var tag = -1;
  var tag1 = -1;
  List<String> floor = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
  ];
  List<String> destination = [
   AppStrings.south.tr,
    AppStrings.western.tr,
    AppStrings.east.tr,
    AppStrings.north.tr,
    AppStrings.southWestern.tr,
    AppStrings.northWestern.tr,
    AppStrings.southEast.tr,
    AppStrings.northEast.tr,
    
  ];
  String dropdownvalue = AppStrings.chooseYourarea.tr;
  var items = [
    AppStrings.chooseYourarea.tr,
    AppStrings.alFurkan.tr,
    AppStrings.newAleppo.tr,
    AppStrings.alSabil.tr,
    AppStrings.alSabil.tr,
    AppStrings.almuhandesen.tr,
    AppStrings.alsalhen.tr,
    AppStrings.almartini.tr,
    AppStrings.newShahbaa.tr,
    AppStrings.oldShahbaa.tr,
    AppStrings.almidan.tr,
    AppStrings.sayfAldawla.tr,
    AppStrings.babAlhadid.tr,
    AppStrings.salahAldiyn.tr,
    AppStrings.babAlnaser.tr,
    AppStrings.alshaar.tr,
  ];
  FarmController controller = Get.put(FarmController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title:  Text(
           AppStrings.farmContract.tr,
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: const Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: GetBuilder<FarmController>(builder: (context) {
          return controller.loading
              ? Center(
                  child: Lottie.asset(AppImages.LoadingImages),
                )
              : SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      defaultText(text: AppStrings.location.tr,),
                      const SizedBox(
                        height: 25,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: DropdownButton(
                          style: const TextStyle(
                              color: Color(0xFF2AA8C0),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                          value: controller.regionName,
                          icon: const Icon(Icons.keyboard_arrow_down,
                              color: Colors.black, size: 30),
                          items: items.map((String items) {
                            return DropdownMenuItem(
                              value: items,
                              child: Text(items),
                            );
                          }).toList(),
                          onChanged: (String? newValue) async {
                            setState(() {
                              dropdownvalue = newValue!;
                            });
                            controller.changeregion(newValue!);
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      defaultText(text: AppStrings.price.tr,),
                      const SizedBox(
                        height: 25,
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: SizedBox(
                              width: 150,
                              child: defaultTextFormField(
                                  color1: Colors.black,
                                  color2: const Color(0xFF2AA8C0),
                                  color3: const Color(0xFF2AA8C0),
                                  color4: const Color(0xFF2AA8C0),
                                  color: Colors.black,
                                  controller: controller.minPrice,
                                  type: TextInputType.number,
                                  label: 'SYP',
                                  prefix: Icons.price_check),
                            ),
                          ),
                          defaultText(text: AppStrings.to.tr,),
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Container(
                              width: 150,
                              child: defaultTextFormField(
                                  color1: Colors.black,
                                  color2: Color(0xFF2AA8C0),
                                  color3: Color(0xFF2AA8C0),
                                  color4: Color(0xFF2AA8C0),
                                  color: Colors.black,
                                  controller: controller.maxPrice,
                                  type: TextInputType.number,
                                  label: 'SYP',
                                  prefix: Icons.price_check),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      defaultText(text: AppStrings.floorNumber.tr,),
                      const SizedBox(
                        height: 25,
                      ),
                      GetBuilder<FarmController>(builder: (context) {
                        return Select(
                            tagg: controller.floorNumber!,
                            wrap: false,
                            app: floor,
                            val: -1,
                            ffc: (val) {
                              controller.changefloornumber(val);
                            });
                      }),
                      const SizedBox(
                        height: 10,
                      ),
                      defaultText(text: AppStrings.farmSpace.tr,),
                      const SizedBox(
                        height: 25,
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: SizedBox(
                              width: 150,
                              child: defaultTextFormField(
                                  color1: Colors.black,
                                  color2: const Color(0xFF2AA8C0),
                                  color3: const Color(0xFF2AA8C0),
                                  color4: const Color(0xFF2AA8C0),
                                  color: Colors.black,
                                  controller: controller.minspaec,
                                  type: TextInputType.number,
                                  label: AppStrings.squaremeters.tr,
                                  prefix: Icons.space_dashboard_outlined),
                            ),
                          ),
                          defaultText(text: AppStrings.to.tr,),
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: SizedBox(
                                width: 150,
                                child: defaultTextFormField(
                                    color1: Colors.black,
                                    color2: Color(0xFF2AA8C0),
                                    color3: Color(0xFF2AA8C0),
                                    color4: Color(0xFF2AA8C0),
                                    color: Colors.black,
                                    controller: controller.maxspace,
                                    type: TextInputType.number,
                                    label: AppStrings.squaremeters.tr,
                                    prefix: Icons.space_dashboard_outlined)),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      defaultText(text: AppStrings.farmDestination.tr,),
                      const SizedBox(
                        height: 25,
                      ),
                      GetBuilder<FarmController>(builder: (context) {
                        return Select(
                            tagg: controller.directions!,
                            wrap: false,
                            val: -1,
                            app: destination,
                            ffc: (val) {
                              controller.changedirection(val);
                            });
                      }),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child: changeButton(
                            text: AppStrings.showTheResult.tr,
                            text1:AppStrings.now.tr,
                            color: Colors.amber,
                            textcolor: Colors.black,
                            onpressed: () {
                              controller.waiting();
                              controller
                                  .getAllFarms(FarmModel(
                                direction: controller.directions,
                                region_id: controller.regionId,
                                num_of_floors: (controller.floorNumber),
                                rent_or_buy: rent_buy,
                                min_price: controller.minPrice.text == ''
                                    ? null
                                    : int.parse(controller.minPrice.text),
                                max_price: controller.maxPrice.text == ''
                                    ? null
                                    : int.parse(controller.maxPrice.text),
                                min_size: controller.minspaec.text == ''
                                    ? null
                                    : int.parse(controller.minspaec.text),
                                max_size: controller.maxspace.text == ''
                                    ? null
                                    : int.parse(controller.maxspace.text),
                              ))
                                  .then((value) {
                                if (value) {
                                  Get.to(const farmPage());
                                  controller.waiting();
                                } else {
                                  controller.waiting();
                                  Get.dialog(AlertDialog(
                                    title: Text(
                                      AppStrings.error.tr,
                                      style: TextStyle(
                                          fontSize: 20, color: Colors.red),
                                    ),
                                    content:  Text(
                                        AppStrings.pleasecheckfromconnectionInternet.tr),
                                    actions: [
                                      TextButton(
                                          onPressed: () {
                                            Get.back();
                                          },
                                          child: const Text('Cancel'))
                                    ],
                                  ));
                                }
                              });
                              // Navigator.of(context).push(MaterialPageRoute(
                              //     builder: (context) => const farmPage()));
                            }),
                      ),
                    ],
                  ),
                );
        }));
  }
}
