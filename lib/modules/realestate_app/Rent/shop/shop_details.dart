import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter/src/foundation/key.dart';
// import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/models/hous/farm_model.dart';
import 'package:house_app/shared/components/components.dart';

import '../../../../core/constant/app_strings.dart';
import '../../../../data/static/static.dart';

class shopDetails extends StatelessWidget {
  FarmModel shopemodel;
  shopDetails({required this.shopemodel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> images = [
      shopemodel.image_1!,
      shopemodel.image_2!,
      shopemodel.image_3!,
    ];
    return Scaffold(
      appBar: AppBar(
        title:  Text(
           AppStrings.shopDetails.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: const Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Swiperpp(
              images: images,
              imageSrc: shopemodel.image_1!,
              content1: 'Rageen Place',
              content2: 'Details :',
              icon: Icons.details_rounded),
          Padding(
            padding: const EdgeInsets.only(left: 15, bottom: 10, top: 15),
            child: Text('${shopemodel.price} SYP',
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                  color: Colors.red,
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15, bottom: 10),
            child: Text(shopemodel.region!,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 23,
                  color: Color(0xFF2AA8C0),
                )),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 10,
            width: double.infinity,
            color: Colors.grey[300],
          ),
          const SizedBox(
            height: 20,
          ),
          defaultText(text:  AppStrings.propertyInformation.tr),
          const SizedBox(
            height: 20,
          ),
          info(
            text:  AppStrings.price.tr,
            text1: '${shopemodel.price} SYP',
          ),
          SizedBox(
            height: 10,
          ),
          Line(),
          SizedBox(
            height: 10,
          ),
          info(
            text:  AppStrings.space.tr,
            text1: '${shopemodel.size} fs',
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.title.tr,
            text1: shopemodel.region!,
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text:  AppStrings.destination.tr,
            text1: dirct(shopemodel.direction!),
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text:  AppStrings.decor.tr,
            text1: 'Yes',
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text:  AppStrings.ownerName.tr,
            text1: shopemodel.user_name!,
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 15),
            child: Center(
              child: tripBtn2(
                  content:  AppStrings.call.tr,
                  icon: Icons.phone,
                  color: const Color(0xFF2AA8C0),
                  myFunc: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return const AlertDialog(
                            title: Text(
                              'The Phone number: 0991022354',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF2AA8C0)),
                            ),
                          );
                        });
                  },
                  radius: 10,
                  iconcolor: Colors.white),
            ),
          ),
        ]),
      ),
    );
  }

  String dirct(int index) {
    late String ans;
    for (int i = 0; i < Direction.length; i++) {
      if (i == index) {
        ans = Direction[i];
      }
    }
    return ans;
  }
}
