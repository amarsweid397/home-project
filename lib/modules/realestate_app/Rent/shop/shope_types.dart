import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
// import 'package:house_app/modules/realestate_app/Cleaning/House/cleaning_price.dart';
import 'package:house_app/modules/realestate_app/Rent/shop/shop_contract.dart';
import 'package:lottie/lottie.dart';
import '../../../../core/constant/app_strings.dart';
import '../../../../shared/components/components.dart';

class shopTypes extends StatefulWidget {
  const shopTypes({Key? key}) : super(key: key);

  @override
  State<shopTypes> createState() => _cleaningTypesState();
}

class _cleaningTypesState extends State<shopTypes> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title:  Text(
             AppStrings.shopTypes.tr,
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: const Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: ListView(
          children: [
            LottieBuilder.network(
              'https://assets10.lottiefiles.com/packages/lf20_8tkhf9ja.json',
              fit: BoxFit.contain,
              height: 180,
            ),
            type(
                text:  AppStrings.officeForRent.tr,
                myf: () {
                  shopeType = 1;
                  Get.to(const shopContract());
                  // Navigator.of(context).push(
                  //     MaterialPageRoute(builder: (context) => shopContract()));
                },
                icon: Icons.home),
            type(
                text:  AppStrings.pharmacyForRent.tr,
                myf: () {
                  shopeType = 2;
                  Get.to(const shopContract());
                  // Navigator.of(context).push(
                  //     MaterialPageRoute(builder: (context) => shopContract()));
                },
                icon: Icons.landscape),
            type(
              text:  AppStrings.commercialForRent.tr,
              myf: () {
                shopeType = 3;
                Get.to(const shopContract());
                // Navigator.of(context).push(MaterialPageRoute(
                //     builder: (context) => const shopContract()));
              },
              icon: Icons.maps_home_work_outlined,
            ),
            type(
                text:  AppStrings.clinicForRent.tr,
                myf: () {
                  shopeType = 4;
                  Get.to(const shopContract());
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => const shopContract()));
                },
                icon: Icons.home_outlined)
          ],
        ));
  }
}
