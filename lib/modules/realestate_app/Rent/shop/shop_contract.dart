import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/controller/shop_screen.dart';
import 'package:house_app/core/constant/images.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/models/hous/farm_model.dart';
//import 'package:flutter/src/foundation/key.dart';
//import 'package:flutter/src/widgets/framework.dart';
import 'package:house_app/modules/realestate_app/Rent/shop/shop_page.dart';
import 'package:house_app/shared/components/components.dart';
import 'package:lottie/lottie.dart';

import '../../../../core/constant/app_strings.dart';

class shopContract extends StatefulWidget {
  const shopContract({Key? key}) : super(key: key);

  @override
  State<shopContract> createState() => _roomContractState();
}

class _roomContractState extends State<shopContract> {
  var nameController = TextEditingController();
  var priceController = TextEditingController();
  var priceController1 = TextEditingController();
  var spaceController = TextEditingController();
  var spaceController1 = TextEditingController();

  var tag1 = -1;
  List<String> destination = [
   AppStrings.south.tr,
    AppStrings.western.tr,
    AppStrings.east.tr,
    AppStrings.north.tr,
    AppStrings.southWestern.tr,
    AppStrings.northWestern.tr,
    AppStrings.southEast.tr,
    AppStrings.northEast.tr,
  ];
  String dropdownvalue =   AppStrings.chooseYourarea.tr;
  var items = [
    AppStrings.chooseYourarea.tr,
    AppStrings.alFurkan.tr,
    AppStrings.newAleppo.tr,
    AppStrings.alSabil.tr,
    AppStrings.alSabil.tr,
    AppStrings.almuhandesen.tr,
    AppStrings.alsalhen.tr,
    AppStrings.almartini.tr,
    AppStrings.newShahbaa.tr,
    AppStrings.oldShahbaa.tr,
    AppStrings.almidan.tr,
    AppStrings.sayfAldawla.tr,
    AppStrings.babAlhadid.tr,
   AppStrings.salahAldiyn.tr,
    AppStrings.babAlnaser.tr,
    AppStrings.alshaar.tr,
  ];
  ShopController controller = Get.put(ShopController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title:  Text(
            AppStrings.shopContract.tr,
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: const Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: GetBuilder<ShopController>(builder: (context) {
          return controller.loading
              ? Center(
                  child: Lottie.asset(AppImages.LoadingImages),
                )
              : SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      defaultText(text:  AppStrings.location.tr),
                      const SizedBox(
                        height: 25,
                      ),
                      Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          child: GetBuilder<ShopController>(builder: (context) {
                            return DropdownButton(
                              style: const TextStyle(
                                  color: Color(0xFF2AA8C0),
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                              // Initial Value
                              value: controller.regionName,
                              icon: const Icon(Icons.keyboard_arrow_down,
                                  color: Colors.black, size: 30),
                              items: items.map((String items) {
                                return DropdownMenuItem(
                                  value: items,
                                  child: Text(items),
                                );
                              }).toList(),

                              onChanged: (String? newValue) async {
                                // setState(() {
                                //   dropdownvalue = newValue!;
                                // });
                                controller.changeregion(newValue!);
                              },
                            );
                          })),
                      const SizedBox(
                        height: 15,
                      ),
                      defaultText(text:  AppStrings.price.tr),
                      const SizedBox(
                        height: 25,
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Container(
                              width: 150,
                              child: defaultTextFormField(
                                  color1: Colors.black,
                                  color2: const Color(0xFF2AA8C0),
                                  color3: const Color(0xFF2AA8C0),
                                  color4: const Color(0xFF2AA8C0),
                                  color: Colors.black,
                                  controller: controller.minprice,
                                  type: TextInputType.number,
                                  label: 'SYP',
                                  prefix: Icons.price_check),
                            ),
                          ),
                          defaultText(text:AppStrings.to.tr),
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: SizedBox(
                              width: 150,
                              child: defaultTextFormField(
                                  color1: Colors.black,
                                  color2: const Color(0xFF2AA8C0),
                                  color3: const Color(0xFF2AA8C0),
                                  color4: const Color(0xFF2AA8C0),
                                  color: Colors.black,
                                  controller: controller.maxprice,
                                  type: TextInputType.number,
                                  label: 'SYP',
                                  prefix: Icons.price_check),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      defaultText(text:  AppStrings.shopSpace.tr),
                      const SizedBox(
                        height: 25,
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: SizedBox(
                              width: 150,
                              child: defaultTextFormField(
                                  color1: Colors.black,
                                  color2: const Color(0xFF2AA8C0),
                                  color3: const Color(0xFF2AA8C0),
                                  color4: const Color(0xFF2AA8C0),
                                  color: Colors.black,
                                  controller: controller.minsize,
                                  type: TextInputType.number,
                                  label: AppStrings.squaremeters.tr,
                                  prefix: Icons.space_dashboard_outlined),
                            ),
                          ),
                          defaultText(text:  AppStrings.to.tr),
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Container(
                                width: 150,
                                child: defaultTextFormField(
                                    color1: Colors.black,
                                    color2: Color(0xFF2AA8C0),
                                    color3: Color(0xFF2AA8C0),
                                    color4: Color(0xFF2AA8C0),
                                    color: Colors.black,
                                    controller: controller.maxsize,
                                    type: TextInputType.number,
                                    label:  AppStrings.squaremeters.tr,
                                    prefix: Icons.space_dashboard_outlined)),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      defaultText(text:  AppStrings.shopDestination.tr),
                      const SizedBox(
                        height: 15,
                      ),
                      GetBuilder<ShopController>(builder: (context) {
                        return Select(
                            tagg: controller.direction,
                            wrap: false,
                            val: -1,
                            app: destination,
                            ffc: (val) {
                              controller.changedirection(val);
                              // setState(() {
                              //   tag1 = val;
                              // });
                            });
                      }),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child: changeButton(
                            text:  AppStrings.showTheResult.tr,
                            text1: AppStrings.now.tr,
                            color: Colors.amber,
                            textcolor: Colors.black,
                            onpressed: () {
                              controller.wiating();
                              controller
                                  .getAllShops(FarmModel(
                                region_id: controller.regionid,
                                min_price: controller.minprice.text == ''
                                    ? null
                                    : int.parse(controller.minprice.text),
                                max_price: controller.maxprice.text == ''
                                    ? null
                                    : int.parse(controller.maxprice.text),
                                min_size: controller.minsize.text == ''
                                    ? null
                                    : int.parse(controller.minsize.text),
                                max_size: controller.maxsize.text == ''
                                    ? null
                                    : int.parse(controller.maxsize.text),
                                direction: controller.direction,
                                rent_or_buy: rent_buy,
                              ))
                                  .then((value) {
                                if (value) {
                                  Get.to(const shopPage());
                                  controller.wiating();
                                } else {
                                  controller.wiating();
                                  Get.dialog(AlertDialog(
                                    title:  Text(
                                       AppStrings.error.tr,
                                      style: TextStyle(
                                          fontSize: 20, color: Colors.red),
                                    ),
                                    content:  Text(
                                         AppStrings.pleasecheckfromconnectionInternet.tr),
                                    actions: [
                                      TextButton(
                                          onPressed: () {
                                            Get.back();
                                          },
                                          child: const Text('Cancel'))
                                    ],
                                  ));
                                }
                              });
                            }),
                      ),
                    ],
                  ),
                );
        }));
  }
}
