import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/modules/realestate_app/Rent/home/bigHome/bighome_contract.dart';
import 'package:house_app/modules/realestate_app/Rent/home/room/room_contract.dart';
import 'package:house_app/modules/realestate_app/Rent/home/smallHome/smallhome_contract.dart';
import 'package:lottie/lottie.dart';
import '../../../../core/constant/app_strings.dart';
import '../../../../shared/components/components.dart';

class homeTypes extends StatefulWidget {
  const homeTypes({Key? key}) : super(key: key);

  @override
  State<homeTypes> createState() => _cleaningTypesState();
}

class _cleaningTypesState extends State<homeTypes> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title:  Text(
            AppStrings.houseTypes.tr,
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: const Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: ListView(
          children: [
            LottieBuilder.network(
              'https://assets9.lottiefiles.com/packages/lf20_qzgnpky3.json',
              fit: BoxFit.contain,
              height: 180,
            ),
            type(
                text: AppStrings.roomForRent.tr,
                myf: () {
                  houseType = 0;

                  Get.off(const roomContract());
                },
                icon: Icons.home_outlined),
            type(
                text: AppStrings.bigHouseForRent.tr,
                myf: () {
                  houseType = 1;
                  print('value of houstype is in second is ${houseType}');
                  Get.off(const bigHomeContract());
                },
                icon: Icons.home_work_rounded),
            type(
                text: AppStrings.smallHouseForRent.tr,
                myf: () {
                  houseType = 2;
                  print('value of click is of small house is  ${houseType}');
                  Get.off(const smallHomeContract());
                },
                icon: Icons.home),
          ],
        ));
  }
}
