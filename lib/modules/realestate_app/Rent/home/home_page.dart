import 'package:house_app/controller/room_screen.dart';
import 'package:house_app/modules/realestate_app/Rent/home/home_details.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'package:get/get.dart';
import 'package:house_app/shared/components/components.dart';
import 'package:lottie/lottie.dart';

import '../../../../core/constant/app_strings.dart';

class homePage extends StatefulWidget {
  const homePage({Key? key}) : super(key: key);

  @override
  State<homePage> createState() => _roomPageState();
}

class _roomPageState extends State<homePage> {
  @override
  Widget build(BuildContext context) {
    RoomScreenController controller = Get.put(RoomScreenController());
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title:  Text(
          AppStrings.housesPage.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: const Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      // body: SingleChildScrollView(
      //     scrollDirection: Axis.vertical,
      //     child: Column(
      //       children: [

      //         SizedBox(
      //           height: 15,
      //         ),
      //         InkWell(
      //           onTap: () {
      //             // Get.to(detailsHome(
      //             //   id: 2,
      //             // ));
      //           },
      //           child: Column(
      //               crossAxisAlignment: CrossAxisAlignment.start,
      //               children: [
      //                 Swiperpp(
      //                     imageSrc: 'https://picsum.photos/500/500?random=3',
      //                     content1: 'Rageen Place',
      //                     content2: 'Details :',
      //                     icon: Icons.details_rounded),
      //                 Padding(
      //                   padding: const EdgeInsets.only(
      //                       left: 15, bottom: 10, top: 15),
      //                   child: Text('2,700,000 SYP',
      //                       style: TextStyle(
      //                         fontWeight: FontWeight.bold,
      //                         fontSize: 25,
      //                         color: Colors.red,
      //                       )),
      //                 ),
      //                 Padding(
      //                   padding: const EdgeInsets.only(left: 15, bottom: 10),
      //                   child: Text('Martini Area,Shahba Rose',
      //                       style: TextStyle(
      //                         fontWeight: FontWeight.bold,
      //                         fontSize: 23,
      //                         color: Colors.black,
      //                       )),
      //                 ),
      //                 Padding(
      //                   padding: const EdgeInsets.only(left: 10, bottom: 15),
      //                   child: tripBtn2(
      //                       content: 'CALL',
      //                       icon: Icons.phone,
      //                       color: Color(0xFF2AA8C0),
      //                       myFunc: () {
      //                         showDialog(
      //                             context: context,
      //                             builder: (BuildContext context) {
      //                               return AlertDialog(
      //                                 title: Text(
      //                                   'The Phone number: 0991022354',
      //                                   style: TextStyle(
      //                                       fontSize: 20,
      //                                       fontWeight: FontWeight.bold,
      //                                       color: Color(0xFF2AA8C0)),
      //                                 ),
      //                               );
      //                             });
      //                       },
      //                       radius: 10,
      //                       iconcolor: Colors.white),
      //                 ),
      //                 Line()
      //               ]),
      //         ),
      //         SizedBox(
      //           height: 15,
      //         ),
      //         InkWell(
      //           onTap: () {
      //             // Get.to(detailsHome(
      //             //   id: 1,
      //             // ));
      //           },
      //           child: Column(
      //               crossAxisAlignment: CrossAxisAlignment.start,
      //               children: [
      //                 Swiperpp(
      //                     imageSrc: 'https://picsum.photos/500/500?random=4',
      //                     content1: 'Rageen Place',
      //                     content2: 'Details :',
      //                     icon: Icons.details_rounded),
      //                 Padding(
      //                   padding: const EdgeInsets.only(
      //                       left: 15, bottom: 10, top: 15),
      //                   child: Text('2,700,000 SYP',
      //                       style: TextStyle(
      //                         fontWeight: FontWeight.bold,
      //                         fontSize: 25,
      //                         color: Colors.red,
      //                       )),
      //                 ),
      //                 Padding(
      //                   padding: const EdgeInsets.only(left: 15, bottom: 10),
      //                   child: Text('Martini Area,Shahba Rose',
      //                       style: TextStyle(
      //                         fontWeight: FontWeight.bold,
      //                         fontSize: 23,
      //                         color: Colors.black,
      //                       )),
      //                 ),
      //                 Padding(
      //                   padding: const EdgeInsets.only(left: 10, bottom: 15),
      //                   child: tripBtn2(
      //                       content: 'CALL',
      //                       icon: Icons.phone,
      //                       color: Color(0xFF2AA8C0),
      //                       myFunc: () {
      //                         showDialog(
      //                             context: context,
      //                             builder: (BuildContext context) {
      //                               return AlertDialog(
      //                                 title: Text(
      //                                   'The Phone number: 0991022354',
      //                                   style: TextStyle(
      //                                       fontSize: 20,
      //                                       fontWeight: FontWeight.bold,
      //                                       color: Color(0xFF2AA8C0)),
      //                                 ),
      //                               );
      //                             });
      //                       },
      //                       radius: 10,
      //                       iconcolor: Colors.white),
      //                 ),
      //                 Line()
      //               ]),
      //         ),
      //         SizedBox(
      //           height: 15,
      //         ),
      //         InkWell(
      //           onTap: () {
      //             // Get.to(detailsHome(
      //             //   id: 1,
      //             // ));
      //           },
      //           child: Column(
      //               crossAxisAlignment: CrossAxisAlignment.start,
      //               children: [
      //                 Swiperpp(
      //                     imageSrc: 'https://picsum.photos/500/500?random=5',
      //                     content1: 'Rageen Place',
      //                     content2: 'Details :',
      //                     icon: Icons.details_rounded),
      //                 Padding(
      //                   padding: const EdgeInsets.only(
      //                       left: 15, bottom: 10, top: 15),
      //                   child: Text('2,700,000 SYP',
      //                       style: TextStyle(
      //                         fontWeight: FontWeight.bold,
      //                         fontSize: 25,
      //                         color: Colors.red,
      //                       )),
      //                 ),
      //                 Padding(
      //                   padding: const EdgeInsets.only(left: 15, bottom: 10),
      //                   child: Text('Martini Area,Shahba Rose',
      //                       style: TextStyle(
      //                         fontWeight: FontWeight.bold,
      //                         fontSize: 23,
      //                         color: Colors.black,
      //                       )),
      //                 ),
      //                 Padding(
      //                   padding: const EdgeInsets.only(left: 10, bottom: 15),
      //                   child: tripBtn2(
      //                       content: 'CALL',
      //                       icon: Icons.phone,
      //                       color: Color(0xFF2AA8C0),
      //                       myFunc: () {
      //                         showDialog(
      //                             context: context,
      //                             builder: (BuildContext context) {
      //                               return AlertDialog(
      //                                 title: Text(
      //                                   'The Phone number: 0991022354',
      //                                   style: TextStyle(
      //                                       fontSize: 20,
      //                                       fontWeight: FontWeight.bold,
      //                                       color: Color(0xFF2AA8C0)),
      //                                 ),
      //                               );
      //                             });
      //                       },
      //                       radius: 10,
      //                       iconcolor: Colors.white),
      //                 ),
      //                 Line()
      //               ]),
      //         ),
      //       ],
      //     )),
      body:
          SafeArea(child: GetBuilder<RoomScreenController>(builder: (context) {
        return controller.listOfRooms.isEmpty
            ? Center(
                child: Column(
                  children: [
                    Lottie.asset('assets/images/error_result.json',
                        width: 350, height: 350),
                    const SizedBox(
                      height: 10,
                    ),
                     Text(
                      AppStrings.nohousesExistWithThisDetailsYet.tr,
                      style: TextStyle(
                        fontSize: 17,
                        color: Color(0xFF2AA8C0),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Icon(
                      Icons.sentiment_dissatisfied_outlined,
                      color: Color(0xFF2AA8C0),
                      size: 32,
                    )
                  ],
                ),
              )
            : ListView.builder(
                itemCount: controller.listOfRooms.length,
                itemBuilder: (context, index) {
                  List<String> images = [
                    controller.listOfRooms[index].image_1!,
                    controller.listOfRooms[index].image_2!,
                    controller.listOfRooms[index].image_3!
                  ];
                  return Column(
                    children: [
                      InkWell(
                        onTap: () {
                          Get.to(detailsHome(
                            houseModel: controller.listOfRooms[index],
                          ));
                        },
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Swiperpp(
                                  images: images,
                                  imageSrc:
                                      controller.listOfRooms[index].image_1!,
                                  content1: 'Rageen Place',
                                  content2: 'Details:',
                                  icon: Icons.details_rounded),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 15, bottom: 10, top: 15),
                                child: Text(
                                    '${controller.listOfRooms[index].price} SYP',
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 25,
                                      color: Colors.red,
                                    )),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 15, bottom: 10),
                                child:
                                    Text(controller.listOfRooms[index].region!,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 23,
                                          color: Colors.black,
                                        )),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 10, bottom: 15),
                                child: tripBtn2(
                                    content: AppStrings.call.tr,
                                    icon: Icons.phone,
                                    color: const Color(0xFF2AA8C0),
                                    myFunc: () {
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return const AlertDialog(
                                              title: Text(
                                                'The Phone number: 0991022354',
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.bold,
                                                    color: Color(0xFF2AA8C0)),
                                              ),
                                            );
                                          });
                                    },
                                    radius: 10,
                                    iconcolor: Colors.white),
                              ),
                              Line()
                            ]),
                      ),
                      const SizedBox(
                        height: 15,
                      )
                    ],
                  );
                });
      })),
    );
  }
}
