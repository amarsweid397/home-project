import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/controller/room_screen.dart';
import 'package:house_app/modules/realestate_app/Rent/home/home_page.dart';
import 'package:house_app/shared/components/components.dart';
import 'package:lottie/lottie.dart';

import '../../../../../core/constant/app_strings.dart';
import '../../../../../data/static/static.dart';
import '../../../../../models/hous/house_model.dart';
import '../../../Sale/home/homesale_types.dart';

class bigHomeContract extends StatefulWidget {
  const bigHomeContract({Key? key}) : super(key: key);

  @override
  State<bigHomeContract> createState() => _roomContractState();
}

class _roomContractState extends State<bigHomeContract> {
  TextEditingController? nameController = TextEditingController();
  TextEditingController? priceController = TextEditingController();
  TextEditingController? priceController1 = TextEditingController();
  TextEditingController? spaceController = TextEditingController();
  TextEditingController? spaceController1 = TextEditingController();
  String name = '';
  bool isname = false;
  var tag = -1;
  var tag1 = -1;
  var tag2 = -1;
  int? auxDropvalue;
  List<String> floor = [
    AppStrings.ground.tr,
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
  ];
  List<String> numberRoom = [
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
  ];
  List<String> destination = [
     AppStrings.south.tr,
    AppStrings.western.tr,
    AppStrings.east.tr,
    AppStrings.north.tr,
    AppStrings.southWestern.tr,
    AppStrings.northWestern.tr,
    AppStrings.southEast.tr,
    AppStrings.northEast.tr,
  ];
  String dropdownvalue = AppStrings.chooseYourarea.tr;
  var items = [
    AppStrings.chooseYourarea.tr,
    AppStrings.alFurkan.tr,
    AppStrings.newAleppo.tr,
    AppStrings.alSabil.tr,
    AppStrings.alSabil.tr,
    AppStrings.almuhandesen.tr,
    AppStrings.alsalhen.tr,
    AppStrings.almartini.tr,
    AppStrings.newShahbaa.tr,
    AppStrings.oldShahbaa.tr,
    AppStrings.almidan.tr,
    AppStrings.sayfAldawla.tr,
    AppStrings.babAlhadid.tr,
   AppStrings.salahAldiyn.tr,
    AppStrings.babAlnaser.tr,
    AppStrings.alshaar.tr,
  ];

  @override
  Widget build(BuildContext context) {
    RoomScreenController controller = Get.put(RoomScreenController());
    return Scaffold(
        appBar: AppBar(
          title: Text(
            AppStrings.bigHouseContract.tr,
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: const Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: GetBuilder<RoomScreenController>(builder: (context) {
          return controller.loading
              ? Center(
                  // child: CircularProgressIndicator(),
                  child: Lottie.asset('assets/images/loading.json'),
                )
              : SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      defaultText(text: AppStrings.location.tr,),
                      const SizedBox(
                        height: 25,
                      ),
                      Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          child: GetBuilder<RoomScreenController>(
                              builder: (context) {
                            return DropdownButton(
                              style: const TextStyle(
                                  color: Color(0xFF2AA8C0),
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                              // Initial Value
                              value: controller.dropdownvalue,

                              // Down Arrow Icon
                              icon: const Icon(Icons.keyboard_arrow_down,
                                  color: Colors.black, size: 30),

                              // Array list of items
                              items: places.map((String items) {
                                return DropdownMenuItem(
                                  value: items,
                                  child: Text(items),
                                );
                              }).toList(),
                              // After selecting the desired option,it will
                              // change button value to selected value

                              onChanged: (String? newValue) async {
                                int ind = items.indexWhere(
                                    (element) => element == newValue);
                                auxDropvalue = ind;
                                controller.changeRegion(newValue!);
                                controller.changeRegion(newValue);
                              },
                            );
                          })),
                      const SizedBox(
                        height: 15,
                      ),
                      defaultText(text: AppStrings.numberOfRooms.tr,),
                      const SizedBox(
                        height: 15,
                      ),
                      GetBuilder<RoomScreenController>(builder: (context) {
                        return Select(
                            tagg: controller.roomNumber!,
                            wrap: false,
                            app: numberRoom,
                            val: -1,
                            ffc: (val) {
                              controller.selectRoomNumber(val);
                            });
                      }),
                      const SizedBox(
                        height: 5,
                      ),
                      defaultText(text: AppStrings.price.tr,),
                      const SizedBox(
                        height: 25,
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Container(
                              width: 150,
                              child: defaultTextFormField(
                                  color1: Colors.black,
                                  color2: Color(0xFF2AA8C0),
                                  color3: Color(0xFF2AA8C0),
                                  color4: Color(0xFF2AA8C0),
                                  color: Colors.black,
                                  controller: priceController!,
                                  type: TextInputType.number,
                                  label: 'SYP',
                                  prefix: Icons.price_check),
                            ),
                          ),
                          defaultText(text:AppStrings.to.tr,),
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Container(
                              width: 150,
                              child: defaultTextFormField(
                                  color1: Colors.black,
                                  color2: Color(0xFF2AA8C0),
                                  color3: Color(0xFF2AA8C0),
                                  color4: Color(0xFF2AA8C0),
                                  color: Colors.black,
                                  controller: priceController1!,
                                  type: TextInputType.number,
                                  label: 'SYP',
                                  prefix: Icons.price_check),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      defaultText(text: AppStrings.chooseFloor.tr,),
                      const SizedBox(
                        height: 15,
                      ),
                      GetBuilder<RoomScreenController>(builder: (context) {
                        return Select(
                            tagg: controller.floor!,
                            wrap: false,
                            app: floor,
                            val: -1,
                            ffc: (val) {
                              controller.selectFloor(val);
                            });
                      }),
                      const SizedBox(
                        height: 5,
                      ),
                      defaultText(text: AppStrings.HouseSpace.tr,),
                      const SizedBox(
                        height: 25,
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Container(
                              width: 150,
                              child: defaultTextFormField(
                                  color1: Colors.black,
                                  color2: Color(0xFF2AA8C0),
                                  color3: Color(0xFF2AA8C0),
                                  color4: Color(0xFF2AA8C0),
                                  color: Colors.black,
                                  controller: spaceController!,
                                  type: TextInputType.number,
                                  label: AppStrings.squaremeters.tr,
                                  prefix: Icons.space_dashboard_outlined),
                            ),
                          ),
                          defaultText(text: AppStrings.to.tr,),
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Container(
                                width: 150,
                                child: defaultTextFormField(
                                    color1: Colors.black,
                                    color2: Color(0xFF2AA8C0),
                                    color3: Color(0xFF2AA8C0),
                                    color4: Color(0xFF2AA8C0),
                                    color: Colors.black,
                                    controller: spaceController1!,
                                    type: TextInputType.number,
                                    label: AppStrings.squaremeters.tr,
                                    prefix: Icons.space_dashboard_outlined)),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      defaultText(text: AppStrings.houseDestination.tr,),
                      const SizedBox(
                        height: 15,
                      ),
                      GetBuilder<RoomScreenController>(builder: (context) {
                        return Select(
                            tagg: controller.direction!,
                            wrap: false,
                            val: -1,
                            app: destination,
                            ffc: (val) {
                              controller.selectDirection(val);
                            });
                      }),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child: changeButton(
                            text: AppStrings.showTheResult.tr,
                            text1: AppStrings.now.tr,
                            color: Colors.amber,
                            textcolor: Colors.black,
                            onpressed: () {
                              controller.Waiting();

                              controller
                                  .getAllRooms(HouseModel(
                                direction: controller.direction == -1
                                    ? null
                                    : controller.direction,
                                region_id: auxDropvalue,
                                min_price: priceController!.text == ''
                                    ? null
                                    : int.parse(priceController!.text),
                                max_price: priceController1!.text == ''
                                    ? null
                                    : int.parse(priceController1!.text),
                                floor: controller.floor == -1
                                    ? null
                                    : controller.floor,
                                num_of_rooms: controller.roomNumber == -1
                                    ? null
                                    : controller.roomNumber,
                                min_size: spaceController!.text == ''
                                    ? null
                                    : int.parse(spaceController!.text),
                                max_size: spaceController1!.text == ''
                                    ? null
                                    : int.parse(spaceController1!.text),
                                rent_or_buy: rent_buy,
                              ))
                                  .then((value) {
                                if (value) {
                                  Get.to(const homePage());
                                  controller.Waiting();
                                } else {
                                  controller.Waiting();
                                  Get.dialog(AlertDialog(
                                    title:  Text(
                                      AppStrings.error.tr,
                                      style: TextStyle(
                                          fontSize: 20, color: Colors.red),
                                    ),
                                    content: Text(
                                       AppStrings.pleasecheckfromconnectionInternet.tr),
                                    actions: [
                                      TextButton(
                                          onPressed: () {
                                            Get.back();
                                          },
                                          child: const Text('Cancel'))
                                    ],
                                  ));
                                }
                              });

                              // Navigator.of(context).push(MaterialPageRoute(
                              //     builder: (context) => homePage()));
                            }),
                      ),
                    ],
                  ),
                );
        }));
  }
}
