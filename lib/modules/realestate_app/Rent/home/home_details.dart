import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/shared/components/components.dart';
import '../../../../core/constant/app_strings.dart';
import '../../../../models/hous/house_model.dart';

class detailsHome extends StatelessWidget {
  late HouseModel houseModel;
  detailsHome({required this.houseModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> imageList = [
      houseModel.image_1!,
      houseModel.image_2!,
      houseModel.image_3!
    ];
    return Scaffold(
      appBar: AppBar(
        title:  Text(
          AppStrings.houseDetails.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: const Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Swiperpp(
              images: imageList,
              imageSrc: imageList[0],
              content1: 'Rageen Place',
              content2: 'Details :',
              icon: Icons.details_rounded),
          Padding(
            padding: const EdgeInsets.only(left: 15, bottom: 10, top: 15),
            child: Text('${houseModel.price} SYP',
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                  color: Colors.red,
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15, bottom: 10),
            child: Text(houseModel.region!,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 23,
                  color: Color(0xFF2AA8C0),
                )),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 10,
            width: double.infinity,
            color: Colors.grey[300],
          ),
          const SizedBox(
            height: 20,
          ),
          defaultText(text: AppStrings.propertyInformation.tr),
          const SizedBox(
            height: 20,
          ),
          info(
            text: AppStrings.price.tr,
            text1: '${houseModel.price} SYP',
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.space.tr,
            text1: '${houseModel.size!} fs',
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.area.tr,
            text1: houseModel.region!,
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.destination.tr,
            text1: dirct(houseModel.direction!),
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.floorNumber.tr,
            text1: '${houseModel.floor!}',
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.numberOfRooms.tr,
            text1: '${houseModel.num_of_rooms}',
          ),
          SizedBox(
            height: 10,
          ),
          Line(),
          SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.decor.tr,
            text1: '${houseModel.dicoration == 1 ? AppStrings.yes.tr : AppStrings.no.tr}',
          ),
          SizedBox(
            height: 10,
          ),
          Line(),
          SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.buildingNumber.tr,
            text1: '${houseModel.building_id!}',
          ),
          const SizedBox(
            height: 10,
          ),
          Line(),
          const SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.paymentType.tr,
            text1: '${houseModel.payment_type}',
          ),
          SizedBox(
            height: 10,
          ),
          Line(),
          SizedBox(
            height: 10,
          ),
          info(
            text: AppStrings.ownerName.tr,
            text1: houseModel.user_name!,
          ),
          SizedBox(
            height: 10,
          ),
          Line(),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 15),
            child: Center(
              child: tripBtn2(
                  content: AppStrings.call.tr,
                  icon: Icons.phone,
                  color: Color(0xFF2AA8C0),
                  myFunc: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text(
                              'The Phone number: 0991022354',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF2AA8C0)),
                            ),
                          );
                        });
                  },
                  radius: 10,
                  iconcolor: Colors.white),
            ),
          ),
        ]),
      ),
    );
  }

  String dirct(int index) {
    late String ans;
    for (int i = 0; i < Direction.length; i++) {
      if (i == index) {
        ans = Direction[i];
      }
    }
    return ans;
  }

  String PaymentType(int index) {
    late String ans;
    for (int i = 0; i < paymenttype.length; i++) {
      if (i == index - 1) {
        ans = paymenttype[i];
      }
    }
    return ans;
  }
}
