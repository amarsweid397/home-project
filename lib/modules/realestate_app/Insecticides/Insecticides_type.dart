import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/modules/realestate_app/Insecticides/Insecticides_contract.dart';
import '../../../core/constant/app_strings.dart';
import '../../../shared/components/components.dart';

class insectTypes extends StatefulWidget {
  const insectTypes({Key? key}) : super(key: key);

  @override
  State<insectTypes> createState() => _cleaningTypesState();
}

class _cleaningTypesState extends State<insectTypes> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          AppStrings.sterilizationService.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: const Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: ListView(children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
          ),
          child: ListView.separated(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) => Column(
                    children: [
                      bild(
                          content: AppStrings.pestControl.tr,
                          icon: Icons.arrow_forward_ios,
                          images: 'https://picsum.photos/500/500?random=1',
                          myff: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => insectContarct()));
                          }),
                      bild(
                          content: AppStrings.disinfectionAndSterilization.tr,
                          icon: Icons.arrow_forward_ios,
                          images: 'https://picsum.photos/500/500?random=2',
                          myff: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => insectContarct()));
                          }),
                    ],
                  ),
              separatorBuilder: (context, index) => SizedBox(
                    height: 8.0,
                  ),
              itemCount: 1),
        ),
      ]),
    );
  }
}
