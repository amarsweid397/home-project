import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/controller/services.dart';

import '../../../../shared/components/components.dart';
import '../../../core/constant/app_strings.dart';

class insectChange extends StatefulWidget {
  const insectChange({Key? key}) : super(key: key);

  @override
  State<insectChange> createState() => _SettingPageState();
}

class _SettingPageState extends State<insectChange> {
  String? gender;
  ServicesController controller = Get.find<ServicesController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 1,
          title:  Text(
           AppStrings.changeScreen.tr,
            style: TextStyle(
                fontSize: 23,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: const Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: SingleChildScrollView(
          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            const SizedBox(
              height: 10,
            ),
            defaultText(text:  AppStrings.choosefrequency.tr),
            const SizedBox(
              height: 50,
            ),
            ListTile(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStrings.once.tr,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                     AppStrings.theSameSopecOnce.tr,
                    style: TextStyle(fontSize: 19, color: Color(0xFF2AA8C0)),
                  ),
                ],
              ),
              leading: GetBuilder<ServicesController>(builder: (context) {
                return Radio(
                    value: 1,
                    activeColor: const Color(0xFF2AA8C0),
                    groupValue: controller.frequency,
                    onChanged: (int?value) {
                      controller.choosefrequency(value!);
                    });
              }),
            ),
            SizedBox(
              height: 15,
            ),
            ListTile(
                title: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                           AppStrings.weekly.tr,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "(Discount 10%)",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                              color: Colors.red),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                     AppStrings.theSameSopecWeekly.tr,
                      style: TextStyle(fontSize: 19, color: Color(0xFF2AA8C0)),
                    ),
                  ],
                ),
                leading: GetBuilder<ServicesController>(builder: (context) {
                  return Radio(
                      value: 2,
                      activeColor: const Color(0xFF2AA8C0),
                      groupValue: gender,
                      onChanged: (value) {
                        setState(() {
                          gender = value.toString();
                        });
                      });
                })),
            SizedBox(
              height: 15,
            ),
            ListTile(
                title: Column(
                  children: [
                    Row(
                      children: [
                        Text( AppStrings.everyToWeeks.tr,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "(Discount 5%)",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                              color: Colors.red),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                     Text(
                       AppStrings.theSameSopecEveryWeeks.tr,
                      style: TextStyle(fontSize: 19, color: Color(0xFF2AA8C0)),
                    ),
                  ],
                ),
                leading: GetBuilder<ServicesController>(builder: (context) {
                  return Radio(
                      value: 3,
                      activeColor: const Color(0xFF2AA8C0),
                      groupValue: gender,
                      onChanged: (value) {
                        setState(() {
                          gender = value.toString();
                        });
                      });
                })),
          ]),
        ));
  }
}
