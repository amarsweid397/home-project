import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/controller/services.dart';

import '../../../../shared/components/components.dart';
import '../../../core/constant/app_strings.dart';

class insectPrice extends StatefulWidget {
  const insectPrice({Key? key}) : super(key: key);

  @override
  State<insectPrice> createState() => _CancelScreenState();
}

class _CancelScreenState extends State<insectPrice> {
  bool isBottomSheet = false;
  var scaffoldKey = GlobalKey<ScaffoldState>();
  ServicesController controller = Get.find<ServicesController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title:  Text(
          AppStrings.insectPrice.tr,
          style: TextStyle(
              fontSize: 23,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: const Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              AppStrings.totalPrice.tr,
              style: TextStyle(
                  color: Color.fromARGB(255, 23, 71, 81),
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
            Text(
              '${controller.price}\$',
              style: TextStyle(
                  color: Color.fromARGB(255, 189, 42, 42),
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
          ],
        ),
      ),
    );
  }
}
