import 'package:get/get.dart';
import 'package:house_app/controller/house_screen.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/modules/realestate_app/Cleaning/cleaning_types.dart';
import 'package:house_app/modules/realestate_app/Insecticides/Insecticides_type.dart';
import 'package:house_app/modules/realestate_app/Rent/all_types.dart';
import 'package:house_app/modules/realestate_app/Repair/Repair_types.dart';
import 'package:house_app/modules/realestate_app/Sale/all_typeSale.dart';

import 'package:carousel_slider/carousel_slider.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../controller/house_most_visited.dart';
import '../../../core/constant/app_strings.dart';
import '../../../shared/components/components.dart';

class mainScreen extends StatelessWidget {
  const mainScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    HouseMostVisitedController controller =
        Get.put(HouseMostVisitedController());
    Size _size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        CarouselSlider(
            items: [
              Image.network(
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTMDWRz7gvSj8lZpHzkqGNzePFdrp4w9bnY8w&usqp=CAU',
                width: double.infinity,
                fit: BoxFit.cover,
              ),
              Image.network(
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQc7w1aVLsp9wqpScCiYcoPOqXk6HfPkKR-gg&usqp=CAU',
                width: double.infinity,
                fit: BoxFit.cover,
              ),
              Image.network(
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQFBzbDE96avepANBNjLMBNZRtjmRIKDvu7Lg&usqp=CAU',
                width: double.infinity,
                fit: BoxFit.cover,
              ),
              Image.network(
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBCcTwysXc7WfgGGsyqEgajoncrDrIE6TETQ&usqp=CAU',
                width: double.infinity,
                fit: BoxFit.cover,
              ),
            ],
            options: CarouselOptions(
                height: 200,
                initialPage: 0,
                viewportFraction: 1.0,
                enableInfiniteScroll: true,
                reverse: false,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 3),
                autoPlayAnimationDuration: Duration(seconds: 1),
                autoPlayCurve: Curves.fastOutSlowIn,
                scrollDirection: Axis.horizontal)),
        Padding(
          padding: const EdgeInsets.all(15),
          child: GridView.count(
            crossAxisCount: 3,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            mainAxisSpacing: 20,
            crossAxisSpacing: 25,
            children: [
              Expanded(
                child: tripBtn(
                    size: 18,
                    textcolor: Colors.black,
                    content: AppStrings.sale.tr,
                    myFunc: () {
                      rent_buy = 1;
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => const SaletypesScreen()));
                    },
                    icon: Icons.home_filled,
                    width: 50,
                    height: 60,
                    color: const Color.fromARGB(255, 234, 250, 250)),
              ),
              tripBtn(
                  size: 18,
                  textcolor: Colors.black,
                  content: AppStrings.rent.tr,
                  myFunc: () {
                    rent_buy = 0;
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const RenttypesScreen()));
                  },
                  icon: Icons.home_work_outlined,
                  width: 50,
                  height: 60,
                  color: Color.fromARGB(255, 234, 250, 250)),
              tripBtn(
                  size: 18,
                  content: AppStrings.repair.tr,
                  textcolor: Colors.black,
                  myFunc: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => repairTypes()));
                  },
                  icon: CupertinoIcons.repeat,
                  width: 70,
                  height: 60,
                  color: Color.fromARGB(255, 234, 250, 250)),
              tripBtn(
                  size: 18,
                  textcolor: Colors.black,
                  content: AppStrings.clean.tr,
                  myFunc: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => cleaningTypes()));
                  },
                  icon: Icons.clean_hands,
                  width: 70,
                  height: 60,
                  color: const Color.fromARGB(255, 234, 250, 250)),
              tripBtn(
                  size: 18,
                  textcolor: Colors.black,
                  content: AppStrings.insect.tr,
                  myFunc: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => insectTypes()));
                  },
                  icon: Icons.sanitizer_sharp,
                  width: 70,
                  height: 60,
                  color: const Color.fromARGB(255, 234, 250, 250)),
            ],
          ),
        ),
         Padding(
          padding: EdgeInsets.only(left: 10.0, bottom: 10),
          child: Text(
            AppStrings.mostVisitedRentalProperties.tr,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Color(0xFF2AA8C0)),
          ),
        ),
        Container(
            color: Colors.grey[200],
            child: GetBuilder<HouseMostVisitedController>(builder: (context) {
              return Expanded(
                child: GridView.builder(
                  itemCount: controller.listOfHouse.length,
                  //crossAxisCount: 2,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  //mainAxisSpacing: 5,
                  //crossAxisSpacing: 3,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 3,
                      mainAxisSpacing: 5),
                  itemBuilder: (BuildContext context, int index) {
                    return mostVisited(
                        houseModel: controller.listOfHouse[index],
                        text: '${controller.listOfHouse[index].region}',
                        text1: '${controller.listOfHouse[index].price} SYP',
                        image: controller.listOfHouse[index].image_2);
                  },
                ),
              );
            }))
      ],
    ));
  }
}
