import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/modules/realestate_app/Rent/shop/shope_types.dart';
import 'package:house_app/modules/realestate_app/advertise/advShopdetails.dart';
import 'package:lottie/lottie.dart';

import '../../../../shared/components/components.dart';
import '../../../core/constant/app_strings.dart';

class advshopTypes extends StatefulWidget {
  const advshopTypes({Key? key}) : super(key: key);

  @override
  State<advshopTypes> createState() => _cleaningTypesState();
}

class _cleaningTypesState extends State<advshopTypes> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            AppStrings.adYourAd.tr,
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: ListView(
          children: [
            type(
                text:  AppStrings.pharmacy.tr,
                myf: () {
                  shopType = 2;
                  Get.to(advShopdetailsScreen());
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => advShopdetailsScreen()));
                },
                icon: Icons.home),
            type(
                text: AppStrings.office.tr,
                myf: () {
                  shopType = 1;
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => advShopdetailsScreen()));
                  Get.to(advShopdetailsScreen());
                },
                icon: Icons.landscape),
            type(
              text: AppStrings.clinic.tr,
              myf: () {
                shopType = 4;
                Get.to(advShopdetailsScreen());
                // Navigator.of(context).push(MaterialPageRoute(
                //     builder: (context) => advShopdetailsScreen()));
              },
              icon: Icons.maps_home_work_outlined,
            ),
            type(
                text: AppStrings.commercialShop.tr,
                myf: () {
                  shopType = 3;
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => advShopdetailsScreen()));
                  Get.to(advShopdetailsScreen());
                },
                icon: Icons.home_outlined)
          ],
        ));
  }
}
