import 'dart:io';
import 'package:get/get.dart';
import 'package:house_app/controller/land_screen.dart';
import 'package:house_app/core/constant/images.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/models/hous/land_model.dart';
import 'package:house_app/modules/realestate_app/Settings/my_ads.dart';

import 'package:house_app/modules/realestate_app/advertise/imagPicker.dart';
import 'package:house_app/shared/components/components.dart';
import 'package:image_picker/image_picker.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter/src/foundation/key.dart';
// import 'package:flutter/src/widgets/framework.dart';
import 'package:lottie/lottie.dart';

import '../../../core/constant/app_strings.dart';

class advLanddetailsScreen extends StatefulWidget {
  const advLanddetailsScreen({Key? key}) : super(key: key);

  @override
  State<advLanddetailsScreen> createState() => _roomContractState();
}

class _roomContractState extends State<advLanddetailsScreen> {
  bool gender = true;
  var nameController = TextEditingController();
  var priceController = TextEditingController();
  var priceController1 = TextEditingController();
  var spaceController = TextEditingController();
  var spaceController1 = TextEditingController();
  bool brush = false;
  bool brush1 = false;
  bool brush2 = false;
  bool brush3 = false;
  String name = '';
  bool isname = false;
  var tag = -1;
  var tag1 = -1;
  var tag2 = -1;
  var tag3 = -1;
  var tag4 = -1;
  var tag5 = -1;
  var tag6 = -1;
  var tag7 = -1;

  List<String> type = [
     AppStrings.agricultural.tr,
     AppStrings.building.tr,
  ];
  List<String> By = [
    AppStrings.mediator.tr,
    AppStrings.owner.tr,
  ];
  List<String> State = [
     AppStrings.rent.tr,
    AppStrings.sale.tr,
  ];
  List<String> destination = [
    AppStrings.south.tr,
    AppStrings.western.tr,
    AppStrings.east.tr,
    AppStrings.north.tr,
    AppStrings.southWestern.tr,
    AppStrings.northWestern.tr,
    AppStrings.southEast.tr,
    AppStrings.northEast.tr,
  ];
  List<String> payment = [AppStrings.annual.tr, AppStrings.semiAnnual.tr, AppStrings.quarterly.tr, AppStrings.monthly.tr];

  String dropdownvalue =  AppStrings.chooseYourarea.tr;
  var items = [
    AppStrings.chooseYourarea.tr,
    AppStrings.newAleppo.tr,
    AppStrings.almartini.tr,
    AppStrings.alsalhen.tr,
    AppStrings.newShahbaa.tr,
    AppStrings.oldShahbaa.tr
  ];
  LandScreenController controller = Get.put(LandScreenController());
  @override
  Widget build(BuildContext context) {
    var length;
    return Scaffold(
      appBar: AppBar(
        title: Text(
           AppStrings.adYourAd.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: GetBuilder<LandScreenController>(builder: (context) {
        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Line(),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Center(
                    child: Text(
                    AppStrings.addAsMany.tr,
                  style: TextStyle(
                      color: Color.fromARGB(255, 25, 21, 21),
                      fontSize: 25,
                      fontWeight: FontWeight.bold),
                )),
              ),
              SizedBox(
                height: 15,
              ),
              Line(),
              defaultText(text: AppStrings.landType.tr),
              const SizedBox(
                height: 15,
              ),
              GetBuilder<LandScreenController>(builder: (context) {
                return Select(
                    tagg: controller.typeofland,
                    wrap: false,
                    app: type,
                    val: -1,
                    ffc: (val) {
                      // setState(() {
                      //   tag3 = val;
                      //   print(tag3.toString());
                      // });
                      controller.selectType(val);
                    });
              }),
              defaultText(text: AppStrings.by.tr),
              SizedBox(
                height: 15,
              ),
              GetBuilder<LandScreenController>(builder: (context) {
                return Select(
                    tagg: controller.isOwner!,
                    wrap: false,
                    app: By,
                    val: -1,
                    ffc: (val) {
                      // setState(() {
                      //   tag4 = val;
                      //   print(tag4.toString());
                      // });
                      controller.selectOwner(val);
                    });
              }),
              defaultText(text: AppStrings.homeState.tr),
              SizedBox(
                height: 15,
              ),
              GetBuilder<LandScreenController>(builder: (context) {
                return Select(
                    tagg: controller.stateRentOrSale!,
                    wrap: false,
                    app: State,
                    val: -1,
                    ffc: (val) {
                      // setState(() {
                      //   tag5 = val;
                      // });
                      controller.selectState(val);
                    });
              }),
              defaultText(text: AppStrings.payment.tr),
              SizedBox(
                height: 15,
              ),
              GetBuilder<LandScreenController>(builder: (context) {
                return Select(
                    tagg: controller.payment!,
                    wrap: false,
                    app: paymenttype,
                    val: -1,
                    ffc: (val) {
                      // setState(() {
                      //   tag6 = val;
                      //   print(tag6.toString());
                      // });
                      controller.selectPayment(val);
                    });
              }),
              defaultText(text:  AppStrings.addImages.tr),
              const SizedBox(
                height: 15,
              ),
              MaterialButton(
                onPressed: () {
                  Get.to(const ImagePickerScreen());
                },
                color: Colors.white,
                textColor: Colors.black,
                minWidth: double.infinity,
                child: Text(
                     AppStrings.pickYourPhotos.tr,
                  style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
                ),
              ),
              defaultText(text: AppStrings.area.tr),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: DropdownButton(
                  style: TextStyle(
                      color: Color(0xFF2AA8C0),
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                  // Initial Value
                  value: controller.region,

                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down,
                      color: Colors.black, size: 30),

                  // Array list of items
                  items: places.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Text(items),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged: (String? newValue) async {
                    // setState(() {
                    //   dropdownvalue = newValue!;
                    // });
                    controller.changeRegion(newValue!);
                  },
                ),
              ),
              defaultText(text: AppStrings.title.tr),
              SizedBox(
                height: 25,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Container(
                  width: double.infinity,
                  child: defaultTextFormField(
                      color1: Colors.black,
                      color2: Color(0xFF2AA8C0),
                      color3: Color(0xFF2AA8C0),
                      color4: Color(0xFF2AA8C0),
                      color: Colors.black,
                      controller: controller.texttitle,
                      type: TextInputType.name,
                      label:  AppStrings.yourTitle.tr,
                      prefix: Icons.title),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              defaultText(text:AppStrings.price.tr),
              SizedBox(
                height: 25,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Container(
                  width: double.infinity,
                  child: defaultTextFormField(
                      color1: Colors.black,
                      color2: Color(0xFF2AA8C0),
                      color3: Color(0xFF2AA8C0),
                      color4: Color(0xFF2AA8C0),
                      color: Colors.black,
                      controller: controller.textprice,
                      type: TextInputType.number,
                      label: 'SYP',
                      prefix: Icons.price_check),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              defaultText(text:AppStrings.landSpace.tr),
              SizedBox(
                height: 25,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Container(
                  width: double.infinity,
                  child: defaultTextFormField(
                      color1: Colors.black,
                      color2: Color(0xFF2AA8C0),
                      color3: Color(0xFF2AA8C0),
                      color4: Color(0xFF2AA8C0),
                      color: Colors.black,
                      controller: controller.textspace,
                      type: TextInputType.number,
                      label:    AppStrings.squaremeters.tr,
                      prefix: Icons.space_dashboard_outlined),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              defaultText(text:  AppStrings.landDestination.tr),
              SizedBox(
                height: 15,
              ),
              GetBuilder<LandScreenController>(builder: (context) {
                return Select(
                    tagg: controller.direction,
                    wrap: false,
                    val: -1,
                    app: destination,
                    ffc: (val) {
                      controller.selectDirection(val);
                    });
              }),
              Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: changeButton(
                    text: AppStrings.showTheResult.tr,
                    text1: AppStrings.now.tr,
                    color: Colors.amber,
                    textcolor: Colors.black,
                    onpressed: () {
                      List<File> imagelist = [];
                      print('bakri aweja is heree now stop');
                      if (controller.imageOne == null ||
                          controller.imageTwo == null ||
                          controller.imageThree == null) {
                        Get.snackbar(AppStrings.warning.tr, AppStrings.thereareSomeMissingValues.tr,
                            colorText: Colors.white,
                            backgroundColor: Colors.teal);
                        return;
                      } else {
                        print('the lenght size is ${imagelist.length}');
                        if (controller.typeofland == -1 ||
                            controller.isOwner == -1 ||
                            controller.stateRentOrSale == -1 ||
                            controller.payment == -1 ||
                            controller.region_id == null ||
                            controller.texttitle.text == '' ||
                            controller.textprice == '' ||
                            controller.textspace == '' ||
                            controller.direction == -1) {
                          Get.snackbar(
                              AppStrings.warning.tr, AppStrings.thereareSomeMissingValues.tr,
                              colorText: Colors.white,
                              backgroundColor: Colors.teal);
                          return;
                        }
                      }
                      imagelist.add(controller.imageOne!);
                      imagelist.add(controller.imageTwo!);
                      imagelist.add(controller.imageThree!);

                      // controller.Waiting();

                      Get.dialog(Lottie.asset(AppImages.LoadingImages));

                      controller
                          .addLand(
                              LandModel.addLand(
                                land_type: controller.typeofland.toString(),
                                is_owner: controller.isOwner,
                                rent_or_buy: controller.stateRentOrSale,
                                payment_type_id: controller.payment! + 1,
                                region_id: controller.region_id,
                                region: controller.texttitle.text,
                                price: int.parse(controller.textprice.text),
                                size: int.parse(controller.textspace.text),
                                direction: controller.direction,
                              ),
                              imagelist)
                          .then((value) {
                        if (value) {
                          Get.back();
                          Get.to(myadsScreen());
                          controller.Waiting();
                        } else {
                          Get.back();
                          Get.snackbar(
                              AppStrings.error.tr, AppStrings.pleasecheckfromconnectionInternet.tr,
                              colorText: Colors.white,
                              backgroundColor: Colors.red);
                          controller.Waiting();
                        }
                      });
                      // Navigator.of(context).push(
                      //     MaterialPageRoute(builder: (context) => myadsScreen()));
                    }),
              ),
            ],
          ),
        );
      }),
    );
  }
}
