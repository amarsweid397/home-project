import 'dart:io';
import 'package:get/get.dart';
import 'package:house_app/controller/farm_screen.dart';
import 'package:house_app/core/constant/app_strings.dart';
import 'package:house_app/core/constant/images.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/models/hous/farm_model.dart';
import 'package:house_app/modules/realestate_app/Settings/my_ads.dart';
import 'package:house_app/modules/realestate_app/advertise/imagPicker.dart';
import 'package:house_app/shared/components/components.dart';
import 'package:image_picker/image_picker.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:lottie/lottie.dart';

class advFarmdetailsScreen extends StatefulWidget {
  const advFarmdetailsScreen({Key? key}) : super(key: key);

  @override
  State<advFarmdetailsScreen> createState() => _roomContractState();
}

class _roomContractState extends State<advFarmdetailsScreen> {
  bool gender = true;
  var nameController = TextEditingController();
  var priceController = TextEditingController();
  var priceController1 = TextEditingController();
  var spaceController = TextEditingController();
  var spaceController1 = TextEditingController();
  bool brush = false;
  bool brush1 = false;
  bool brush2 = false;
  bool brush3 = false;
  String name = '';
  bool isname = false;
  var tag = -1;
  var tag1 = -1;
  var tag2 = -1;
  var tag3 = -1;
  var tag4 = -1;
  var tag5 = -1;
  var tag6 = -1;
  var tag7 = -1;
  List<String> floor = [
    AppStrings.ground.tr,
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
  ];

  List<String> Pool = [
    AppStrings.no.tr,
    AppStrings.yes.tr,
  ];
  List<String> By = [
    AppStrings.mediator.tr,
    AppStrings.owner.tr,
  ];
  List<String> State = [
    AppStrings.rent.tr,
    AppStrings.sale.tr,
  ];
  List<String> destination = [
    AppStrings.south.tr,
    AppStrings.western.tr,
    AppStrings.east.tr,
    AppStrings.north.tr,
    AppStrings.southWestern.tr,
    AppStrings.northWestern.tr,
    AppStrings.southEast.tr,
    AppStrings.northEast.tr,
  ];
  List<String> payment = [AppStrings.annual.tr, AppStrings.semiAnnual.tr, AppStrings.quarterly.tr, AppStrings.monthly.tr];
  List<String> brushes = [
    AppStrings.furnished.tr,
    AppStrings.partiallyFurnished.tr,
    AppStrings.unfurnished.tr,
  ];
  String dropdownvalue = AppStrings.chooseYourarea.tr;
  var items = [
    AppStrings.chooseYourarea.tr,
    AppStrings.newAleppo.tr,
    AppStrings.almartini.tr,
    AppStrings.alsalhen.tr,
    AppStrings.newShahbaa.tr,
    AppStrings.oldShahbaa.tr
  ];
  FarmController controller = Get.put(FarmController());
  @override
  Widget build(BuildContext context) {
    var length;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppStrings.adYourAd.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: const Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Line(),
             Padding(
              padding: EdgeInsets.only(top: 10),
              child: Center(
                  child: Text(
                AppStrings.addAsMany.tr,
                style: TextStyle(
                    color: Color.fromARGB(255, 25, 21, 21),
                    fontSize: 25,
                    fontWeight: FontWeight.bold),
              )),
            ),
            const SizedBox(
              height: 15,
            ),
            Line(),
            defaultText(text: AppStrings.swimmingPool.tr),
            const SizedBox(
              height: 15,
            ),
            GetBuilder<FarmController>(builder: (context) {
              return Select(
                  tagg: controller.pool,
                  wrap: false,
                  app: Pool,
                  val: -1,
                  ffc: (val) {
                    // setState(() {
                    //   tag3 = val;
                    //   print(tag3.toString());
                    // });
                    controller.selectPool(val);
                  });
            }),
            defaultText(text: AppStrings.by.tr),
            SizedBox(
              height: 15,
            ),
            GetBuilder<FarmController>(builder: (context) {
              return Select(
                  tagg: controller.isOnwer,
                  wrap: false,
                  app: By,
                  val: -1,
                  ffc: (val) {
                    controller.selectOwner(val);
                  });
            }),
            defaultText(text: AppStrings.homeState.tr),
            const SizedBox(
              height: 15,
            ),
            GetBuilder<FarmController>(builder: (context) {
              return Select(
                  tagg: controller.stateRentOrSale,
                  wrap: false,
                  app: State,
                  val: -1,
                  ffc: (val) {
                    // setState(() {
                    //   tag5 = val;
                    // });
                    controller.selectState(val);
                  });
            }),
            defaultText(text: AppStrings.payment.tr),
            SizedBox(
              height: 15,
            ),
            GetBuilder<FarmController>(builder: (context) {
              return Select(
                  tagg: controller.payment,
                  wrap: false,
                  app: paymenttype,
                  val: -1,
                  ffc: (val) {
                    // setState(() {
                    //   tag6 = val;
                    //   print(tag6.toString());
                    // });
                    controller.selectPayment(val);
                  });
            }),
            defaultText(text: AppStrings.addImages.tr),
            SizedBox(
              height: 15,
            ),
            MaterialButton(
              onPressed: () {
                Get.to(ImagePickerScreen());
              },
              color: Colors.white,
              textColor: Colors.black,
              minWidth: double.infinity,
              child:  Text(
                AppStrings.pickYourPhotos.tr,
                style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
              ),
            ),
            defaultText(text: AppStrings.furnituers.tr),
            const SizedBox(
              height: 15,
            ),
            GetBuilder<FarmController>(builder: (context) {
              return Select(
                  tagg: controller.burshes,
                  wrap: false,
                  app: brushes,
                  val: -1,
                  ffc: (val) {
                    controller.selectburshes(val);
                  });
            }),
            defaultText(text: AppStrings.furnituersType.tr),
            const SizedBox(
              height: 15,
            ),
            Column(
              children: [
                CheckboxListTile(
                  value: brush,
                  onChanged: (value) {
                    if (value!) {
                      controller.sitting_burshes = 1;
                    } else {
                      controller.sitting_burshes = 0;
                    }

                    setState(() {
                      brush = value;
                    });
                  },
                  activeColor: Color(0xFF2AA8C0),
                  title: Text(
                    AppStrings.sittingRoomFurnituers.tr,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
                  ),
                ),
                CheckboxListTile(
                  value: brush1,
                  onChanged: (value) {
                    if (value!) {
                      controller.bedroom_burshes = 1;
                    } else {
                      controller.bedroom_burshes = 0;
                    }
                    setState(() {
                      brush1 = value;
                    });
                  },
                  activeColor: Color(0xFF2AA8C0),
                  title: Text(AppStrings.bedRoomFurnituers.tr,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 19)),
                ),
                CheckboxListTile(
                  value: brush2,
                  onChanged: (value) {
                    if (value!) {
                      controller.bathroom_burshes = 1;
                    } else {
                      controller.bathroom_burshes = 0;
                    }

                    setState(() {
                      brush2 = value;
                    });
                  },
                  activeColor: Color(0xFF2AA8C0),
                  title: Text(AppStrings.bathRoomFurnituers.tr,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 19)),
                ),
                CheckboxListTile(
                  value: brush3,
                  onChanged: (value) {
                    if (value!) {
                      controller.kitchen_burshes = 1;
                    } else {
                      controller.kitchen_burshes = 0;
                    }
                    setState(() {
                      brush3 = value;
                    });
                  },
                  activeColor: const Color(0xFF2AA8C0),
                  title: Text(AppStrings.kitchenFurnituers.tr,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 19)),
                ),
              ],
            ),
            defaultText(text: AppStrings.area.tr),
            const SizedBox(
              height: 15,
            ),
            GetBuilder<FarmController>(builder: (context) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: DropdownButton(
                  style: TextStyle(
                      color: Color(0xFF2AA8C0),
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                  // Initial Value
                  value: controller.regionName,

                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down,
                      color: Colors.black, size: 30),

                  // Array list of items
                  items: places.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Text(items),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged: (String? newValue) async {
                    controller.changeregion(newValue!);
                    // setState(() {
                    //   dropdownvalue = newValue;
                    // });
                  },
                ),
              );
            }),
            defaultText(text: AppStrings.title.tr),
            SizedBox(
              height: 25,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Container(
                width: double.infinity,
                child: defaultTextFormField(
                    color1: Colors.black,
                    color2: Color(0xFF2AA8C0),
                    color3: Color(0xFF2AA8C0),
                    color4: Color(0xFF2AA8C0),
                    color: Colors.black,
                    controller: controller.texttitle,
                    type: TextInputType.name,
                    label: AppStrings.yourTitle.tr,
                    prefix: Icons.title),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            defaultText(text: AppStrings.price.tr),
            SizedBox(
              height: 25,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Container(
                width: double.infinity,
                child: defaultTextFormField(
                    color1: Colors.black,
                    color2: Color(0xFF2AA8C0),
                    color3: Color(0xFF2AA8C0),
                    color4: Color(0xFF2AA8C0),
                    color: Colors.black,
                    controller: controller.textprice,
                    type: TextInputType.number,
                    label: 'SYP',
                    prefix: Icons.price_check),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            defaultText(text: AppStrings.chooseFloor.tr),
            SizedBox(
              height: 15,
            ),
            GetBuilder<FarmController>(builder: (context) {
              return Select(
                  tagg: controller.floorNumber!,
                  wrap: false,
                  app: floor,
                  val: -1,
                  ffc: (val) {
                    // setState(() {
                    //   tag = val;
                    // });
                    controller.changefloornumber(val);
                  });
            }),
            SizedBox(
              height: 5,
            ),
            defaultText(text: AppStrings.farmSpace.tr),
            const SizedBox(
              height: 25,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Container(
                width: double.infinity,
                child: defaultTextFormField(
                    color1: Colors.black,
                    color2: Color(0xFF2AA8C0),
                    color3: Color(0xFF2AA8C0),
                    color4: Color(0xFF2AA8C0),
                    color: Colors.black,
                    controller: controller.textspace,
                    type: TextInputType.number,
                    label: AppStrings.squaremeters.tr,
                    prefix: Icons.space_dashboard_outlined),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            defaultText(text: AppStrings.farmDestination.tr),
            SizedBox(
              height: 15,
            ),
            GetBuilder<FarmController>(builder: (context) {
              return Select(
                  tagg: controller.directions!,
                  wrap: false,
                  val: -1,
                  app: destination,
                  ffc: (val) {
                    // setState(() {
                    //   tag1 = val;
                    // });
                    controller.changedirection(val);
                  });
            }),
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: changeButton(
                  text: AppStrings.showTheResult.tr,
                  text1: AppStrings.now.tr,
                  color: Colors.amber,
                  textcolor: Colors.black,
                  onpressed: () {
                    List<File> imagefile = [];
                    if (controller.imageOne == null ||
                        controller.imageTwo == null ||
                        controller.imageThree == null) {
                      Get.snackbar( AppStrings.warning.tr,  AppStrings.thereareSomeMissingValues.tr,
                          colorText: Colors.white,
                          backgroundColor: Colors.teal);
                      return;
                    } else {
                      if (controller.pool == -1 ||
                          controller.isOnwer == -1 ||
                          controller.stateRentOrSale == -1 ||
                          controller.payment == -1 ||
                          controller.burshes == -1 ||
                          controller.regionId == null ||
                          controller.textprice.text == '' ||
                          controller.textspace.text == '' ||
                          controller.texttitle.text == '') {
                        Get.snackbar( AppStrings.warning.tr,  AppStrings.thereareSomeMissingValues.tr,
                            colorText: Colors.white,
                            backgroundColor: Colors.teal);
                        return;
                      }
                    }
                    imagefile.add(controller.imageOne!);
                    imagefile.add(controller.imageTwo!);
                    imagefile.add(controller.imageThree!);
                    Get.dialog(Lottie.asset(AppImages.LoadingImages));
                    controller
                        .addFarm(
                            FarmModel.addFarm(
                                swimming_pool: controller.pool,
                                is_owner: controller.isOnwer,
                                rent_or_buy: controller.stateRentOrSale,
                                payment_type_id: controller.payment + 1,
                                furniture_type_id: controller.burshes + 1,
                                sitting_room_furniture:
                                    controller.sitting_burshes,
                                bathroom_furniture: controller.bathroom_burshes,
                                kitchen_furniture: controller.kitchen_burshes,
                                bedroom_furniture: controller.bedroom_burshes,
                                region_id: controller.regionId,
                                address: controller.texttitle.text,
                                price: int.parse(controller.textprice.text),
                                size: int.parse(controller.textspace.text),
                                num_of_floors: controller.floorNumber,
                                direction: controller.directions),
                            imagefile)
                        .then((value) {
                      if (value) {
                        Get.back();
                      } else {
                        Get.back();
                        Get.snackbar(
                            AppStrings.error.tr, AppStrings.pleasecheckfromconnectionInternet.tr,
                            colorText: Colors.white,
                            backgroundColor: Colors.red);
                      }
                    });
                    // FarmController
                    //   controller.
                    // Navigator.of(context).push(
                    //     MaterialPageRoute(builder: (context) => myadsScreen()));
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
