import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/modules/realestate_app/advertise/advFarmdetails.dart';
import 'package:house_app/modules/realestate_app/advertise/advHometype.dart';
import 'package:house_app/modules/realestate_app/advertise/advLanddetails.dart';
import 'package:house_app/modules/realestate_app/advertise/advShoptype.dart';

import '../../../core/constant/app_strings.dart';
import '../../../shared/components/components.dart';
import '../Rent/all_types.dart';
import '../Repair/Repair_types.dart';
import '../Sale/all_typeSale.dart';

class advertiseScreen extends StatelessWidget {
  const advertiseScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          defaultText(text: AppStrings.whatDoYouWantToAdvertise.tr),
          const SizedBox(
            height: 5,
          ),
           Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Text(
              AppStrings.chooseTheAppropriateCategoryForYourAd.tr,
              style: TextStyle(color: Colors.grey, fontSize: 16),
            ),
          ),
          SizedBox(
            height: 35,
          ),
          GridView.count(
            crossAxisCount: 2,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            mainAxisSpacing: 20,
            crossAxisSpacing: 20,
            children: [
              tripBtn(
                  size: 23,
                  content: AppStrings.homee.tr,
                  color: Color.fromARGB(255, 234, 250, 250),
                  myFunc: () {
                    TypeofAdd = 1;
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => advhomeTypes()));
                  },
                  width: 80,
                  height: 80,
                  icon: Icons.home,
                  textcolor: Colors.black),
              tripBtn(
                  size: 23,
                  content: AppStrings.land.tr,
                  color: Color.fromARGB(255, 234, 250, 250),
                  myFunc: () {
                    TypeofAdd = 2;
                    Get.to(advLanddetailsScreen());
                    // Navigator.of(context).push(MaterialPageRoute(
                    //     builder: (context) => advLanddetailsScreen()));
                  },
                  width: 100,
                  height: 100,
                  icon: Icons.landscape,
                  textcolor: Colors.black),
              tripBtn(
                  size: 23,
                  content: AppStrings.farm.tr,
                  color: Color.fromARGB(255, 234, 250, 250),
                  myFunc: () {
                    TypeofAdd = 3;
                    Get.to(const advFarmdetailsScreen());
                    // Navigator.of(context).push(MaterialPageRoute(
                    //     builder: (context) => advFarmdetailsScreen()));
                  },
                  width: 100,
                  height: 100,
                  icon: Icons.maps_home_work_outlined,
                  textcolor: Colors.black),
              tripBtn(
                  size: 23,
                  content: AppStrings.shop.tr,
                  color: Color.fromARGB(255, 234, 250, 250),
                  myFunc: () {
                    TypeofAdd = 4;
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => advshopTypes()));
                  },
                  width: 100,
                  height: 100,
                  icon: Icons.home_outlined,
                  textcolor: Colors.black),
            ],
          ),
        ],
      ),
    ));
  }
}
