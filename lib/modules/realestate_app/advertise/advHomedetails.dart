import 'dart:io';
import 'package:get/get.dart';
import 'package:house_app/controller/room_screen.dart';
import 'package:house_app/core/constant/images.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/models/hous/house_model.dart';
import 'package:house_app/modules/realestate_app/Settings/my_ads.dart';
import 'package:house_app/modules/realestate_app/advertise/imagPicker.dart';
import 'package:house_app/shared/components/components.dart';
import 'package:image_picker/image_picker.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:lottie/lottie.dart';

import '../../../core/constant/app_strings.dart';

class advHomedetailsScreen extends StatefulWidget {
  const advHomedetailsScreen({Key? key}) : super(key: key);

  @override
  State<advHomedetailsScreen> createState() => _roomContractState();
}

class _roomContractState extends State<advHomedetailsScreen> {
  bool gender = true;
  var nameController = TextEditingController();
  var priceController = TextEditingController();
  var priceController1 = TextEditingController();
  var spaceController = TextEditingController();
  var spaceController1 = TextEditingController();
  bool brush = false;
  bool brush1 = false;
  bool brush2 = false;
  bool brush3 = false;
  String name = '';
  bool isname = false;
  var tag = -1;
  var tag1 = -1;
  var tag2 = -1;
  var tag3 = -1;
  var tag4 = -1;
  var tag5 = -1;
  var tag6 = -1;
  var tag7 = -1;
  List<String> floor = [
    AppStrings.ground.tr,
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
  ];
  List<String> numberRoom = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
  ];
  List<String> Decor = [
     AppStrings.no.tr,
    AppStrings.yes.tr,
  ];
  List<String> By = [
    AppStrings.mediator.tr,
    AppStrings.owner.tr,
  ];
  List<String> State = [
    AppStrings.rent.tr,
    AppStrings.sale.tr,
  ];
  List<String> destination = [
    AppStrings.south.tr,
    AppStrings.western.tr,
    AppStrings.east.tr,
    AppStrings.north.tr,
    AppStrings.southWestern.tr,
    AppStrings.northWestern.tr,
    AppStrings.southEast.tr,
    AppStrings.northEast.tr,
  ];
  List<String> payment = [AppStrings.annual.tr, AppStrings.semiAnnual.tr, AppStrings.quarterly.tr, AppStrings.monthly.tr];
  List<String> brushes = [
    AppStrings.furnished.tr,
    AppStrings.partiallyFurnished.tr,
    AppStrings.unfurnished.tr,
  ];
  String dropdownvalue =  AppStrings.chooseYourarea.tr;
  var items = [
    AppStrings.chooseYourarea.tr,
    AppStrings.newAleppo.tr,
    AppStrings.almartini.tr,
    AppStrings.alsalhen.tr,
    AppStrings.newShahbaa.tr,
    AppStrings.oldShahbaa.tr
  ];
  RoomScreenController controller = Get.put(RoomScreenController());
  @override
  Widget build(BuildContext context) {
    var length;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppStrings.adYourAd.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: const Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Get.back(); // Navigator.of(context).pop();
          },
        ),
      ),
      body: GetBuilder<RoomScreenController>(builder: (context) {
        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Line(),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Center(
                    child: Text(
                   AppStrings.addAsMany.tr,
                  style: TextStyle(
                      color: Color.fromARGB(255, 25, 21, 21),
                      fontSize: 25,
                      fontWeight: FontWeight.bold),
                )),
              ),
              const SizedBox(
                height: 15,
              ),
              Line(),
              defaultText(text:  AppStrings.decor.tr,),
              const SizedBox(
                height: 15,
              ),
              GetBuilder<RoomScreenController>(builder: (context) {
                return Select(
                    tagg: controller.decor,
                    wrap: false,
                    app: Decor,
                    val: -1,
                    ffc: (val) {
                      // setState(() {
                      //   tag3 = val;
                      //   print(tag3.toString());
                      // });
                      controller.selectDecor(val);
                    });
              }),
              defaultText(text:  AppStrings.by.tr,),
              const SizedBox(
                height: 15,
              ),
              GetBuilder<RoomScreenController>(builder: (context) {
                return Select(
                    tagg: controller.owner,
                    wrap: false,
                    app: By,
                    val: -1,
                    ffc: (val) {
                      // setState(() {
                      //   tag4 = val;
                      //   print(tag4.toString());
                      // });
                      controller.selectOwner(val);
                    });
              }),
              defaultText(text:  AppStrings.homeState.tr,),
              const SizedBox(
                height: 15,
              ),
              GetBuilder<RoomScreenController>(builder: (context) {
                return Select(
                    tagg: controller.stateSaleOrRent,
                    wrap: false,
                    app: State,
                    val: -1,
                    ffc: (val) {
                      // setState(() {
                      //   tag5 = val;
                      // });
                      controller.selectStateRentOrSale(val);
                    });
              }),
              defaultText(text:  AppStrings.payment.tr,),
              SizedBox(
                height: 15,
              ),
              GetBuilder<RoomScreenController>(builder: (context) {
                return Select(
                    tagg: controller.payment,
                    wrap: false,
                    app: paymenttype,
                    val: -1,
                    ffc: (val) {
                      // setState(() {
                      //   tag6 = val;
                      //   print(tag6.toString());
                      // });
                      controller.selectPayment(val);
                    });
              }),
              defaultText(text: AppStrings.addImages.tr,),
              const SizedBox(
                height: 15,
              ),
              MaterialButton(
                onPressed: () {
                  Get.to(const ImagePickerScreen());
                },
                color: Colors.white,
                textColor: Colors.black,
                minWidth: double.infinity,
                child: Text(
                   AppStrings.pickYourPhotos.tr,
                  style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
                ),
              ),
              defaultText(text:  AppStrings.furnituers.tr),
              const SizedBox(
                height: 15,
              ),
              GetBuilder<RoomScreenController>(builder: (context) {
                return Select(
                    tagg: controller.brushes,
                    wrap: false,
                    app: brushes,
                    val: -1,
                    ffc: (val) {
                      controller.selectBrushes(val);
                    });
              }),
              defaultText(text:  AppStrings.furnituersType.tr,),
              const SizedBox(
                height: 15,
              ),
              Column(
                children: [
                  CheckboxListTile(
                    value: brush,
                    onChanged: (value) {
                      if (value!) {
                        controller.sitting_brushes = 1;
                      } else {
                        controller.sitting_brushes = 0;
                      }
                      setState(() {
                        brush = value;
                      });
                    },
                    activeColor: const Color(0xFF2AA8C0),
                    title:  Text(
                       AppStrings.sittingRoomFurnituers.tr,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
                    ),
                  ),
                  CheckboxListTile(
                    value: brush1,
                    onChanged: (value) {
                      if (value!) {
                        controller.bedroom_brushes = 1;
                      } else {
                        controller.bedroom_brushes = 0;
                      }
                      setState(() {
                        brush1 = value;
                      });
                    },
                    activeColor: const Color(0xFF2AA8C0),
                    title: Text( AppStrings.bedRoomFurnituers.tr,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 19)),
                  ),
                  CheckboxListTile(
                    value: brush2,
                    onChanged: (value) {
                      if (value!) {
                        controller.bathroom_brushes = 1;
                      } else {
                        controller.bathroom_brushes = 0;
                      }
                      setState(() {
                        brush2 = value;
                      });
                    },
                    activeColor: const Color(0xFF2AA8C0),
                    title: Text( AppStrings.bathRoomFurnituers.tr,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 19)),
                  ),
                  CheckboxListTile(
                    value: brush3,
                    onChanged: (value) {
                      if (value!) {
                        controller.kitchen_brushes = 1;
                      } else {
                        controller.kitchen_brushes = 0;
                      }
                      setState(() {
                        brush3 = value;
                      });
                    },
                    activeColor: const Color(0xFF2AA8C0),
                    title: Text( AppStrings.kitchenFurnituers.tr,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 19)),
                  ),
                ],
              ),
              defaultText(text:  AppStrings.buildingNumber.tr,),
              const SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Container(
                  width: double.infinity,
                  child: defaultTextFormField(
                      color1: Colors.black,
                      color2: Color(0xFF2AA8C0),
                      color3: Color(0xFF2AA8C0),
                      color4: Color(0xFF2AA8C0),
                      color: Colors.black,
                      controller: controller.textBuildingNum,
                      type: TextInputType.number,
                      label: AppStrings.yourNum.tr,
                      prefix: Icons.home_mini),
                ),
              ),
              defaultText(text:  AppStrings.area.tr,),
              SizedBox(
                height: 15,
              ),
              GetBuilder<RoomScreenController>(builder: (context) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: DropdownButton(
                    style: TextStyle(
                        color: Color(0xFF2AA8C0),
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                    // Initial Value
                    value: controller.dropdownvalue,

                    // Down Arrow Icon
                    icon: const Icon(Icons.keyboard_arrow_down,
                        color: Colors.black, size: 30),

                    // Array list of items
                    items: places.map((String items) {
                      return DropdownMenuItem(
                        value: items,
                        child: Text(items),
                      );
                    }).toList(),
                    // After selecting the desired option,it will
                    // change button value to selected value
                    onChanged: (String? newValue) async {
                      controller.changeRegion(newValue!);
                      // setState(() {
                      //   dropdownvalue = newValue!;
                      // });
                    },
                  ),
                );
              }),
              defaultText(text:  AppStrings.title.tr,),
              const SizedBox(
                height: 25,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Container(
                  width: double.infinity,
                  child: defaultTextFormField(
                      color1: Colors.black,
                      color2: Color(0xFF2AA8C0),
                      color3: Color(0xFF2AA8C0),
                      color4: Color(0xFF2AA8C0),
                      color: Colors.black,
                      controller: controller.textTitle,
                      type: TextInputType.name,
                      label:  AppStrings.yourTitle.tr,
                      prefix: Icons.title),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              addHomeType != 3
                  ? defaultText(text:AppStrings.numberOfRooms.tr,)
                  : SizedBox(),
              addHomeType != 3
                  ? const SizedBox(
                      height: 15,
                    )
                  : SizedBox(),
              addHomeType != 3
                  ? GetBuilder<RoomScreenController>(builder: (context) {
                      return Select(
                          tagg: controller.roomNumber,
                          wrap: false,
                          app: numberRoom,
                          val: -1,
                          ffc: (val) {
                            controller.selectRoomNumber(val);
                            print('here value is ${controller.roomNumber}');
                          });
                    })
                  : SizedBox(),
              const SizedBox(
                height: 5,
              ),
              defaultText(text:  AppStrings.price.tr,),
              const SizedBox(
                height: 25,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Container(
                  width: double.infinity,
                  child: defaultTextFormField(
                      color1: Colors.black,
                      color2: Color(0xFF2AA8C0),
                      color3: Color(0xFF2AA8C0),
                      color4: Color(0xFF2AA8C0),
                      color: Colors.black,
                      controller: controller.textPrice,
                      type: TextInputType.number,
                      label: 'SYP',
                      prefix: Icons.price_check),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              defaultText(text:  AppStrings.chooseFloor.tr,),
              SizedBox(
                height: 15,
              ),
              GetBuilder<RoomScreenController>(builder: (context) {
                return Select(
                    tagg: controller.floor!,
                    wrap: false,
                    app: floor,
                    val: -1,
                    ffc: (val) {
                      // setState(() {
                      //   tag = val;
                      // });
                      controller.selectFloor(val);
                    });
              }),
              const SizedBox(
                height: 5,
              ),
              defaultText(text: AppStrings.HouseSpace.tr,),
              SizedBox(
                height: 25,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Container(
                  width: double.infinity,
                  child: defaultTextFormField(
                      color1: Colors.black,
                      color2: Color(0xFF2AA8C0),
                      color3: Color(0xFF2AA8C0),
                      color4: Color(0xFF2AA8C0),
                      color: Colors.black,
                      controller: controller.textSize,
                      type: TextInputType.number,
                      label:  AppStrings.squaremeters.tr,
                      prefix: Icons.space_dashboard_outlined),
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              defaultText(text:  AppStrings.houseDestination.tr,),
              const SizedBox(
                height: 15,
              ),
              GetBuilder<RoomScreenController>(builder: (context) {
                return Select(
                    tagg: controller.direction!,
                    wrap: false,
                    val: -1,
                    app: destination,
                    ffc: (val) {
                      controller.selectDirection(val);
                    });
              }),
              Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: changeButton(
                    text:  AppStrings.showTheResult.tr,
                    text1:  AppStrings.now.tr,
                    color: Colors.amber,
                    textcolor: Colors.black,
                    onpressed: () {
                      List<File> listfile = [];
                      if (controller.imageOne == null ||
                          controller.imageTwo == null ||
                          controller.imageThree == null) {
                        Get.snackbar( AppStrings.warning.tr, AppStrings.thereareSomeMissingValues.tr,
                            colorText: Colors.white,
                            backgroundColor: Colors.teal);
                        return;
                      } else {
                        if (controller.decor == -1 ||
                            controller.owner == -1 ||
                            controller.stateSaleOrRent == -1 ||
                            controller.payment == -1 ||
                            controller.payment == -1 ||
                            controller.textBuildingNum.text == '' ||
                            controller.textPrice.text == '' ||
                            controller.textSize.text == '' ||
                            controller.textTitle.text == '' ||
                            controller.regionId == null ||
                            controller.roomNumber == -1 ||
                            controller.floor == -1 ||
                            controller.direction == -1) {
                          Get.snackbar(
                              AppStrings.warning.tr,AppStrings.thereareSomeMissingValues.tr,
                              colorText: Colors.white,
                              backgroundColor: Colors.teal);
                          return;
                        }
                      }

                      listfile.add(controller.imageOne!);
                      listfile.add(controller.imageTwo!);
                      listfile.add(controller.imageThree!);
                      Get.dialog(Lottie.asset(AppImages.LoadingImages));
                      controller.Waiting();
                      controller
                          .addHouse(
                              HouseModel.addAddverst(
                                dicoration: controller.decor,
                                is_owner: controller.owner,
                                rent_or_buy: controller.stateSaleOrRent,
                                payment_type_id: controller.payment + 1,
                                image1: controller.imageOne,
                                image2: controller.imageTwo,
                                image3: controller.imageThree,
                                furniture_type_id: controller.brushes + 1,
                                sitting_room_furniture:
                                    controller.sitting_brushes,
                                bedroom_furniture: controller.bedroom_brushes,
                                bathroom_furniture: controller.bathroom_brushes,
                                kitchen_furniture: controller.kitchen_brushes,
                                building_id:
                                    int.parse(controller.textBuildingNum.text),
                                region_id: controller.regionId,
                                address: controller.textTitle.text,
                                numOfRooms: controller.roomNumber,
                                price: int.parse(controller.textPrice.text),
                                floor: controller.floor,
                                size: int.parse(controller.textSize.text),
                                direction: controller.direction,
                              ),
                              listfile)
                          .then((value) {
                        if (value) {
                          // Get.to(const myadsScreen());
                          Get.back();
                          //controller.Waiting();
                        } else {
                          Get.back();
                          Get.snackbar(
                              AppStrings.error.tr,AppStrings.pleasecheckfromconnectionInternet.tr,
                              colorText: Colors.white,
                              backgroundColor: Colors.red);
                          // controller.Waiting();
                          // Get.dialog(AlertDialog(
                          //   title: const Text(
                          //     'Error',
                          //     style: TextStyle(color: Colors.red),
                          //   ),
                          //   content: const Text(
                          //     'Can\'t Add post Please check your Internet',
                          //     style: TextStyle(color: Colors.black),
                          //   ),
                          //   actions: [
                          //     TextButton(
                          //       child: const Text('Ok'),
                          //       onPressed: () {
                          //         Get.back();
                          //       },
                          //     )
                          //   ],
                          // ));
                        }
                      });
                      // Navigator.of(context).push(
                      //     MaterialPageRoute(builder: (context) => myadsScreen()));
                    }),
              ),
            ],
          ),
        );
      }),
    );
  }
}
