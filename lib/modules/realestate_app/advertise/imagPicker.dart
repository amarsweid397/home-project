import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/controller/farm_screen.dart';
import 'package:house_app/controller/land_screen.dart';
import 'package:house_app/controller/room_screen.dart';
import 'package:house_app/controller/shop_screen.dart';
import 'package:house_app/data/static/static.dart';
import 'package:image_picker/image_picker.dart';
import '../../../core/constant/app_strings.dart';
import '../../../shared/components/components.dart';

class ImagePickerScreen extends StatefulWidget {
  const ImagePickerScreen({Key? key}) : super(key: key);

  @override
  State<ImagePickerScreen> createState() => _ImagePickerScreenState();
}

class _ImagePickerScreenState extends State<ImagePickerScreen> {
  // RoomScreenController controller = Get.find<RoomScreenController>();

//   if(dependecyType==1){
//   var controller = Get.find();
//  }
  var controller;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (TypeofAdd == 1) {
      controller = Get.find<RoomScreenController>();
    } else if (TypeofAdd == 2) {
      controller = Get.find<LandScreenController>();
    } else if (TypeofAdd == 3) {
      controller = Get.find<FarmController>();
    } else {
      controller = Get.find<ShopController>();
    }
  }

  final ImagePicker imagePicker = ImagePicker();
  List<XFile> imageFileList = [];
  void selectImage() async {
    final List<XFile> selectedImage = await imagePicker.pickMultiImage();
    if (selectedImage.isNotEmpty && selectedImage.length <= 3) {
      //String file1 = selectedImage[0].path.split('/').last;
      //print('the Result choosing is ${file1}');
      // File aux = File(file1);
      // print('the value after filling is ${aux}');

      controller.imageOne = File(selectedImage[0].path);
      //controller.imageOne = File(selectedImage[0].path);
      //String file2 = selectedImage[1].path.split('/').last;
      controller.imageTwo = File(selectedImage[1].path);
      //String file3 = selectedImage[2].path.split('/').last;
      controller.imageThree = File(selectedImage[2].path);

      for (int i = 0; i < selectedImage.length; i++) {
        imageFileList.add(selectedImage[i]);
      }
      //  imageFileList.addAll(selectedImage);

    } else {
      Get.snackbar(AppStrings.warning.tr, AppStrings.thereareSomeMissingValues.tr,
          backgroundColor: Colors.teal, colorText: Colors.white);
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppStrings.addImages.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
              child: Padding(
            padding: EdgeInsets.all(8.0),
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3, crossAxisSpacing: 5),
              itemCount: imageFileList.length,
              itemBuilder: (BuildContext context, int index) {
                return Image.file(
                  File(imageFileList[index].path),
                  fit: BoxFit.cover,
                );
              },
            ),
          )),
          changeButton(
              text: AppStrings.pickYourPhotos.tr,
              text1: '',
              color: Colors.amber,
              textcolor: Colors.black,
              onpressed: () {
                selectImage();
              })
        ],
      ),
    );
  }
}
