import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
import 'package:lottie/lottie.dart';
import '../../../../shared/components/components.dart';
import '../../../core/constant/app_strings.dart';
import 'advHomedetails.dart';

class advhomeTypes extends StatefulWidget {
  const advhomeTypes({Key? key}) : super(key: key);

  @override
  State<advhomeTypes> createState() => _cleaningTypesState();
}

class _cleaningTypesState extends State<advhomeTypes> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
           AppStrings.adYourAd.tr,
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: ListView(
          children: [
            type(
                text: AppStrings.room.tr,
                myf: () {
                  addHomeType = 3;
                  Get.to(advHomedetailsScreen());
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => advHomedetailsScreen()));
                },
                icon: Icons.home_outlined),
            type(
                text:  AppStrings.bighouse.tr,
                myf: () {
                  addHomeType = 2;
                  Get.to(advHomedetailsScreen());
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => advHomedetailsScreen()));
                },
                icon: Icons.home_work_rounded),
            type(
                text: AppStrings.smallhouse.tr,
                myf: () {
                  addHomeType = 1;
                  Get.to(advHomedetailsScreen());
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => advHomedetailsScreen()));
                },
                icon: Icons.home),
          ],
        ));
  }
}
