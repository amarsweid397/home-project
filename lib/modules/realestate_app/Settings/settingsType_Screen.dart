import 'package:get/get.dart';
import 'package:house_app/controller/getInfoUser.dart';
import 'package:house_app/modules/realestate_app/Settings/accountScreen.dart';
import 'package:house_app/modules/realestate_app/Settings/my_ads.dart';
import 'package:house_app/modules/realestate_app/Settings/settingScreen.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../../shared/components/components.dart';
import '../../../core/constant/app_strings.dart';

class settingTypeScreen extends StatefulWidget {
  const settingTypeScreen({Key? key}) : super(key: key);

  @override
  State<settingTypeScreen> createState() => _cleaningTypesState();
}

class _cleaningTypesState extends State<settingTypeScreen> {
  int index = 0;
  InfoUser controller = Get.put(InfoUser());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      children: [
        type(
            text: AppStrings.myAds.tr,
            myf: () {
              controller.getFarm();
              controller.getHouses();
              controller.getLand();
              controller.getShops();
              Get.to(myadsScreen());
              // Navigator.of(context)
              //     .push(MaterialPageRoute(builder: (context) => myadsScreen()));
            },
            icon: Icons.add_home_outlined),
        type(
          text: AppStrings.myAccount.tr,
          myf: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => accountScreen()));
          },
          icon: Icons.account_box_outlined,
        ),
        type(
            text: AppStrings.settings.tr,
            myf: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => settingScreen()));
            },
            icon: Icons.settings_applications_outlined),
        type(
            text: AppStrings.whoAreWe.tr,
            myf: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text(
                        AppStrings.theAplication.tr,
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF2AA8C0)),
                      ),
                    );
                  });
            },
            icon: Icons.question_mark),
      ],
    ));
  }
}
