import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/controller/getInfoUser.dart';
import 'package:house_app/shared/components/components.dart';

import '../../../core/constant/app_strings.dart';

class myadsScreen extends StatefulWidget {
  const myadsScreen({Key? key}) : super(key: key);

  @override
  State<myadsScreen> createState() => _myadsScreenState();
}

class _myadsScreenState extends State<myadsScreen>
    with SingleTickerProviderStateMixin {
  int currentIndex = 0;
  bool load = false;
  InfoUser controller = Get.find<InfoUser>();
  @override
  initState() {
    load = true;
    super.initState();
    Future.delayed(Duration(seconds: 5), () {
      load = false;
      setState(() {});
    });
    // controller.waiting();
    // controller.getHouses().then((value) {
    //   controller.waiting();
    // });
  }

  List<dynamic> listOfItems = [];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 5,
        child: Scaffold(
            appBar: AppBar(
              bottom: TabBar(
                  onTap: (int value) {
                    currentIndex = value;
                    if (currentIndex == 0) {
                      // controller.waiting();
                      // controller.getHouses().then((value) {
                      //   controller.waiting();
                      // });
                    } else if (currentIndex == 1) {
                      //controller.getLand();
                    } else if (currentIndex == 2) {
                      //controller.getFarm();
                    } else if (currentIndex == 3) {
                      //controller.getShops();
                    }
                    setState(() {});
                  },
                  tabs: [
                    Tab(
                      text: AppStrings.home.tr,
                    ),
                    Tab(
                      text: AppStrings.land.tr,
                    ),
                    Tab(
                      text: AppStrings.farm.tr,
                    ),
                    Tab(
                      text: AppStrings.shop.tr,
                    ),
                    Tab(
                      text: AppStrings.contract.tr,
                    ),
                  ]),
              title: Text(AppStrings.myAds.tr),
              backgroundColor: Color(0xFF2AA8C0),
              leading: IconButton(
                icon: Icon(
                  CupertinoIcons.back,
                  size: 30,
                  color: Colors.black,
                ),
                onPressed: () {
                  Get.back();
                  // Navigator.of(context).pop();
                },
              ),
            ),
            body: TabBarView(
                physics: BouncingScrollPhysics(),
                dragStartBehavior: DragStartBehavior.down,
                children: [
                  GetBuilder<InfoUser>(builder: (context) {
                    print('typr of controller ${controller.loading}');
                    print('first of GetBuilder is here ');
                    return load
                        ? Column(
                            children: [
                              Text('bakri aweja '),
                              Container(
                                color: Colors.yellow,
                                child: Center(
                                  child: CircularProgressIndicator(),
                                ),
                              ),
                            ],
                          )
                        : ListView.builder(
                            itemCount: currentIndex == 0
                                ? controller.listOfHouseUser.length
                                : currentIndex == 1
                                    ? controller.listOfLandUser.length
                                    : currentIndex == 2
                                        ? controller.listOfFarmUser.length
                                        : currentIndex == 3
                                            ? controller.listOfShopUser.length
                                            : controller
                                                .listOfContractUser.length,
                            itemBuilder: (context, index) {
                              print(
                                  'the lenght of house is ${controller.listOfHouseUser.length}');
                              print(
                                  'the lenght of land is ${controller.listOfLandUser.length}');
                              print(
                                  'the lenght of Farm is ${controller.listOfFarmUser.length}');
                              print(
                                  'the lenght of house is ${controller.listOfShopUser.length}');
                              print(
                                  'bakri aweja and messsi and carrasco is here');
                              return Column(
                                children: [
                                  Swiperpp(
                                      imageSrc:
                                          'https://picsum.photos/500/500?random=4',
                                      content1: 'Rageen Place',
                                      content2: 'Details :',
                                      icon: Icons.details_rounded),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 15, top: 15),
                                    child: Text('2,700,000 SYP',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 25,
                                          color: Colors.red,
                                        )),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 15, bottom: 10),
                                    child: Row(
                                      children: [
                                        Text('Martini Area,Shahba Rose',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 23,
                                              color: Colors.black,
                                            )),
                                        Spacer(),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 5),
                                          child: MaterialButton(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15)),
                                              child: Text(
                                                AppStrings.delete.tr,
                                                style: TextStyle(
                                                    fontSize: 23,
                                                    color: Colors.red),
                                              ),
                                              height: 40,
                                              minWidth: 100,
                                              color: Colors.white,
                                              onPressed: () {
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return AlertDialog(
                                                        title: Row(
                                                          children: [
                                                            Icon(
                                                              Icons.done,
                                                              color:
                                                                  Colors.green,
                                                            ),
                                                            Text(
                                                              'Delete Done',
                                                              style: TextStyle(
                                                                  fontSize: 20,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: Colors
                                                                      .green),
                                                            ),
                                                          ],
                                                        ),
                                                      );
                                                    });
                                              }),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Line(),
                                ],
                              );
                            });
                  })
                ])));
  }
}
              //       Swiperpp(
              //           imageSrc: 'https://picsum.photos/500/500?random=4',
              //           content1: 'Rageen Place',
              //           content2: 'Details :',
              //           icon: Icons.details_rounded),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, top: 15),
              //         child: Text('2,700,000 SYP',
              //             style: TextStyle(
              //               fontWeight: FontWeight.bold,
              //               fontSize: 25,
              //               color: Colors.red,
              //             )),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, bottom: 10),
              //         child: Row(
              //           children: [
              //             Text('Martini Area,Shahba Rose',
              //                 style: TextStyle(
              //                   fontWeight: FontWeight.bold,
              //                   fontSize: 23,
              //                   color: Colors.black,
              //                 )),
              //             Spacer(),
              //             Padding(
              //               padding: const EdgeInsets.only(right: 5),
              //               child: MaterialButton(
              //                   shape: RoundedRectangleBorder(
              //                       borderRadius: BorderRadius.circular(15)),
              //                   child: Text(
              //                     'DELETE',
              //                     style: TextStyle(
              //                         fontSize: 23, color: Colors.red),
              //                   ),
              //                   height: 40,
              //                   minWidth: 100,
              //                   color: Colors.white,
              //                   onPressed: () {
              //                     showDialog(
              //                         context: context,
              //                         builder: (BuildContext context) {
              //                           return AlertDialog(
              //                             title: Row(
              //                               children: [
              //                                 Icon(
              //                                   Icons.done,
              //                                   color: Colors.green,
              //                                 ),
              //                                 Text(
              //                                   'Delete Done',
              //                                   style: TextStyle(
              //                                       fontSize: 20,
              //                                       fontWeight: FontWeight.bold,
              //                                       color: Colors.green),
              //                                 ),
              //                               ],
              //                             ),
              //                           );
              //                         });
              //                   }),
              //             ),
              //           ],
              //         ),
              //       ),
              //       Line(),
              //       Swiperpp(
              //           imageSrc: 'https://picsum.photos/500/500?random=4',
              //           content1: 'Rageen Place',
              //           content2: 'Details :',
              //           icon: Icons.details_rounded),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, top: 15),
              //         child: Text('2,700,000 SYP',
              //             style: TextStyle(
              //               fontWeight: FontWeight.bold,
              //               fontSize: 25,
              //               color: Colors.red,
              //             )),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, bottom: 10),
              //         child: Row(
              //           children: [
              //             Text('Martini Area,Shahba Rose',
              //                 style: TextStyle(
              //                   fontWeight: FontWeight.bold,
              //                   fontSize: 23,
              //                   color: Colors.black,
              //                 )),
              //             Spacer(),
              //             Padding(
              //               padding: const EdgeInsets.only(right: 5),
              //               child: MaterialButton(
              //                   shape: RoundedRectangleBorder(
              //                       borderRadius: BorderRadius.circular(15)),
              //                   child: Text(
              //                     'DELETE',
              //                     style: TextStyle(
              //                         fontSize: 23, color: Colors.red),
              //                   ),
              //                   height: 40,
              //                   minWidth: 100,
              //                   color: Colors.white,
              //                   onPressed: () {
              //                     showDialog(
              //                         context: context,
              //                         builder: (BuildContext context) {
              //                           return AlertDialog(
              //                             title: Row(
              //                               children: [
              //                                 Icon(
              //                                   Icons.done,
              //                                   color: Colors.green,
              //                                 ),
              //                                 Text(
              //                                   'Delete Done',
              //                                   style: TextStyle(
              //                                       fontSize: 20,
              //                                       fontWeight: FontWeight.bold,
              //                                       color: Colors.green),
              //                                 ),
              //                               ],
              //                             ),
              //                           );
              //                         });
              //                   }),
              //             ),
              //           ],
              //         ),
              //       ),
              //       Line(),
              //     ],
              //   ),
              // ),
              // SingleChildScrollView(
              //   child: Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Swiperpp(
              //           imageSrc: 'https://picsum.photos/500/500?random=4',
              //           content1: 'Rageen Place',
              //           content2: 'Details :',
              //           icon: Icons.details_rounded),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, top: 15),
              //         child: Text('2,700,000 SYP',
              //             style: TextStyle(
              //               fontWeight: FontWeight.bold,
              //               fontSize: 25,
              //               color: Colors.red,
              //             )),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, bottom: 10),
              //         child: Row(
              //           children: [
              //             Text('Martini Area,Shahba Rose',
              //                 style: TextStyle(
              //                   fontWeight: FontWeight.bold,
              //                   fontSize: 23,
              //                   color: Colors.black,
              //                 )),
              //             Spacer(),
              //             Padding(
              //               padding: const EdgeInsets.only(right: 5),
              //               child: MaterialButton(
              //                   shape: RoundedRectangleBorder(
              //                       borderRadius: BorderRadius.circular(15)),
              //                   child: Text(
              //                     'DELETE',
              //                     style: TextStyle(
              //                         fontSize: 23, color: Colors.red),
              //                   ),
              //                   height: 40,
              //                   minWidth: 100,
              //                   color: Colors.white,
              //                   onPressed: () {
              //                     showDialog(
              //                         context: context,
              //                         builder: (BuildContext context) {
              //                           return AlertDialog(
              //                             title: Row(
              //                               children: [
              //                                 Icon(
              //                                   Icons.done,
              //                                   color: Colors.green,
              //                                 ),
              //                                 Text(
              //                                   'Delete Done',
              //                                   style: TextStyle(
              //                                       fontSize: 20,
              //                                       fontWeight: FontWeight.bold,
              //                                       color: Colors.green),
              //                                 ),
              //                               ],
              //                             ),
              //                           );
              //                         });
              //                   }),
              //             ),
              //           ],
              //         ),
              //       ),
              //       Line(),
              //       Swiperpp(
              //           imageSrc: 'https://picsum.photos/500/500?random=4',
              //           content1: 'Rageen Place',
              //           content2: 'Details :',
              //           icon: Icons.details_rounded),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, top: 15),
              //         child: Text('2,700,000 SYP',
              //             style: TextStyle(
              //               fontWeight: FontWeight.bold,
              //               fontSize: 25,
              //               color: Colors.red,
              //             )),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, bottom: 10),
              //         child: Row(
              //           children: [
              //             Text('Martini Area,Shahba Rose',
              //                 style: TextStyle(
              //                   fontWeight: FontWeight.bold,
              //                   fontSize: 23,
              //                   color: Colors.black,
              //                 )),
              //             Spacer(),
              //             Padding(
              //               padding: const EdgeInsets.only(right: 5),
              //               child: MaterialButton(
              //                   shape: RoundedRectangleBorder(
              //                       borderRadius: BorderRadius.circular(15)),
              //                   child: Text(
              //                     'DELETE',
              //                     style: TextStyle(
              //                         fontSize: 23, color: Colors.red),
              //                   ),
              //                   height: 40,
              //                   minWidth: 100,
              //                   color: Colors.white,
              //                   onPressed: () {
              //                     showDialog(
              //                         context: context,
              //                         builder: (BuildContext context) {
              //                           return AlertDialog(
              //                             title: Row(
              //                               children: [
              //                                 Icon(
              //                                   Icons.done,
              //                                   color: Colors.green,
              //                                 ),
              //                                 Text(
              //                                   'Delete Done',
              //                                   style: TextStyle(
              //                                       fontSize: 20,
              //                                       fontWeight: FontWeight.bold,
              //                                       color: Colors.green),
              //                                 ),
              //                               ],
              //                             ),
              //                           );
              //                         });
              //                   }),
              //             ),
              //           ],
              //         ),
              //       ),
              //       Line(),
              //       Swiperpp(
              //           imageSrc: 'https://picsum.photos/500/500?random=4',
              //           content1: 'Rageen Place',
              //           content2: 'Details :',
              //           icon: Icons.details_rounded),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, top: 15),
              //         child: Text('2,700,000 SYP',
              //             style: TextStyle(
              //               fontWeight: FontWeight.bold,
              //               fontSize: 25,
              //               color: Colors.red,
              //             )),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, bottom: 10),
              //         child: Row(
              //           children: [
              //             Text('Martini Area,Shahba Rose',
              //                 style: TextStyle(
              //                   fontWeight: FontWeight.bold,
              //                   fontSize: 23,
              //                   color: Colors.black,
              //                 )),
              //             Spacer(),
              //             Padding(
              //               padding: const EdgeInsets.only(right: 5),
              //               child: MaterialButton(
              //                   shape: RoundedRectangleBorder(
              //                       borderRadius: BorderRadius.circular(15)),
              //                   child: Text(
              //                     'DELETE',
              //                     style: TextStyle(
              //                         fontSize: 23, color: Colors.red),
              //                   ),
              //                   height: 40,
              //                   minWidth: 100,
              //                   color: Colors.white,
              //                   onPressed: () {
              //                     showDialog(
              //                         context: context,
              //                         builder: (BuildContext context) {
              //                           return AlertDialog(
              //                             title: Row(
              //                               children: [
              //                                 Icon(
              //                                   Icons.done,
              //                                   color: Colors.green,
              //                                 ),
              //                                 Text(
              //                                   'Delete Done',
              //                                   style: TextStyle(
              //                                       fontSize: 20,
              //                                       fontWeight: FontWeight.bold,
              //                                       color: Colors.green),
              //                                 ),
              //                               ],
              //                             ),
              //                           );
              //                         });
              //                   }),
              //             ),
              //           ],
              //         ),
              //       ),
              //       Line(),
              //     ],
              //   ),
              // ),
              // SingleChildScrollView(
              //   child: Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Swiperpp(
              //           imageSrc: 'https://picsum.photos/500/500?random=4',
              //           content1: 'Rageen Place',
              //           content2: 'Details :',
              //           icon: Icons.details_rounded),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, top: 15),
              //         child: Text('2,700,000 SYP',
              //             style: TextStyle(
              //               fontWeight: FontWeight.bold,
              //               fontSize: 25,
              //               color: Colors.red,
              //             )),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, bottom: 10),
              //         child: Row(
              //           children: [
              //             Text('Martini Area,Shahba Rose',
              //                 style: TextStyle(
              //                   fontWeight: FontWeight.bold,
              //                   fontSize: 23,
              //                   color: Colors.black,
              //                 )),
              //             Spacer(),
              //             Padding(
              //               padding: const EdgeInsets.only(right: 5),
              //               child: MaterialButton(
              //                   shape: RoundedRectangleBorder(
              //                       borderRadius: BorderRadius.circular(15)),
              //                   child: Text(
              //                     'DELETE',
              //                     style: TextStyle(
              //                         fontSize: 23, color: Colors.red),
              //                   ),
              //                   height: 40,
              //                   minWidth: 100,
              //                   color: Colors.white,
              //                   onPressed: () {
              //                     showDialog(
              //                         context: context,
              //                         builder: (BuildContext context) {
              //                           return AlertDialog(
              //                             title: Row(
              //                               children: [
              //                                 Icon(
              //                                   Icons.done,
              //                                   color: Colors.green,
              //                                 ),
              //                                 Text(
              //                                   'Delete Done',
              //                                   style: TextStyle(
              //                                       fontSize: 20,
              //                                       fontWeight: FontWeight.bold,
              //                                       color: Colors.green),
              //                                 ),
              //                               ],
              //                             ),
              //                           );
              //                         });
              //                   }),
              //             ),
              //           ],
              //         ),
              //       ),
              //       Line(),
              //       Swiperpp(
              //           imageSrc: 'https://picsum.photos/500/500?random=4',
              //           content1: 'Rageen Place',
              //           content2: 'Details :',
              //           icon: Icons.details_rounded),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, top: 15),
              //         child: Text('2,700,000 SYP',
              //             style: TextStyle(
              //               fontWeight: FontWeight.bold,
              //               fontSize: 25,
              //               color: Colors.red,
              //             )),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, bottom: 10),
              //         child: Row(
              //           children: [
              //             Text('Martini Area,Shahba Rose',
              //                 style: TextStyle(
              //                   fontWeight: FontWeight.bold,
              //                   fontSize: 23,
              //                   color: Colors.black,
              //                 )),
              //             Spacer(),
              //             Padding(
              //               padding: const EdgeInsets.only(right: 5),
              //               child: MaterialButton(
              //                   shape: RoundedRectangleBorder(
              //                       borderRadius: BorderRadius.circular(15)),
              //                   child: Text(
              //                     'DELETE',
              //                     style: TextStyle(
              //                         fontSize: 23, color: Colors.red),
              //                   ),
              //                   height: 40,
              //                   minWidth: 100,
              //                   color: Colors.white,
              //                   onPressed: () {
              //                     showDialog(
              //                         context: context,
              //                         builder: (BuildContext context) {
              //                           return AlertDialog(
              //                             title: Row(
              //                               children: [
              //                                 Icon(
              //                                   Icons.done,
              //                                   color: Colors.green,
              //                                 ),
              //                                 Text(
              //                                   'Delete Done',
              //                                   style: TextStyle(
              //                                       fontSize: 20,
              //                                       fontWeight: FontWeight.bold,
              //                                       color: Colors.green),
              //                                 ),
              //                               ],
              //                             ),
              //                           );
              //                         });
              //                   }),
              //             ),
              //           ],
              //         ),
              //       ),
              //       Line(),
              //       Swiperpp(
              //           imageSrc: 'https://picsum.photos/500/500?random=4',
              //           content1: 'Rageen Place',
              //           content2: 'Details :',
              //           icon: Icons.details_rounded),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, top: 15),
              //         child: Text('2,700,000 SYP',
              //             style: TextStyle(
              //               fontWeight: FontWeight.bold,
              //               fontSize: 25,
              //               color: Colors.red,
              //             )),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, bottom: 10),
              //         child: Row(
              //           children: [
              //             Text('Martini Area,Shahba Rose',
              //                 style: TextStyle(
              //                   fontWeight: FontWeight.bold,
              //                   fontSize: 23,
              //                   color: Colors.black,
              //                 )),
              //             Spacer(),
              //             Padding(
              //               padding: const EdgeInsets.only(right: 5),
              //               child: MaterialButton(
              //                   shape: RoundedRectangleBorder(
              //                       borderRadius: BorderRadius.circular(15)),
              //                   child: Text(
              //                     'DELETE',
              //                     style: TextStyle(
              //                         fontSize: 23, color: Colors.red),
              //                   ),
              //                   height: 40,
              //                   minWidth: 100,
              //                   color: Colors.white,
              //                   onPressed: () {
              //                     showDialog(
              //                         context: context,
              //                         builder: (BuildContext context) {
              //                           return AlertDialog(
              //                             title: Row(
              //                               children: [
              //                                 Icon(
              //                                   Icons.done,
              //                                   color: Colors.green,
              //                                 ),
              //                                 Text(
              //                                   'Delete Done',
              //                                   style: TextStyle(
              //                                       fontSize: 20,
              //                                       fontWeight: FontWeight.bold,
              //                                       color: Colors.green),
              //                                 ),
              //                               ],
              //                             ),
              //                           );
              //                         });
              //                   }),
              //             ),
              //           ],
              //         ),
              //       ),
              //       Line(),
              //     ],
              //   ),
              // ),
              // SingleChildScrollView(
              //   child: Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Swiperpp(
              //           imageSrc: 'https://picsum.photos/500/500?random=4',
              //           content1: 'Rageen Place',
              //           content2: 'Details :',
              //           icon: Icons.details_rounded),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, top: 15),
              //         child: Text('2,700,000 SYP',
              //             style: TextStyle(
              //               fontWeight: FontWeight.bold,
              //               fontSize: 25,
              //               color: Colors.red,
              //             )),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, bottom: 10),
              //         child: Row(
              //           children: [
              //             Text('Martini Area,Shahba Rose',
              //                 style: TextStyle(
              //                   fontWeight: FontWeight.bold,
              //                   fontSize: 23,
              //                   color: Colors.black,
              //                 )),
              //             Spacer(),
              //             Padding(
              //               padding: const EdgeInsets.only(right: 5),
              //               child: MaterialButton(
              //                   shape: RoundedRectangleBorder(
              //                       borderRadius: BorderRadius.circular(15)),
              //                   child: Text(
              //                     'DELETE',
              //                     style: TextStyle(
              //                         fontSize: 23, color: Colors.red),
              //                   ),
              //                   height: 40,
              //                   minWidth: 100,
              //                   color: Colors.white,
              //                   onPressed: () {
              //                     showDialog(
              //                         context: context,
              //                         builder: (BuildContext context) {
              //                           return AlertDialog(
              //                             title: Row(
              //                               children: [
              //                                 Icon(
              //                                   Icons.done,
              //                                   color: Colors.green,
              //                                 ),
              //                                 Text(
              //                                   'Delete Done',
              //                                   style: TextStyle(
              //                                       fontSize: 20,
              //                                       fontWeight: FontWeight.bold,
              //                                       color: Colors.green),
              //                                 ),
              //                               ],
              //                             ),
              //                           );
              //                         });
              //                   }),
              //             ),
              //           ],
              //         ),
              //       ),
              //       Line(),
              //       Swiperpp(
              //           imageSrc: 'https://picsum.photos/500/500?random=4',
              //           content1: 'Rageen Place',
              //           content2: 'Details :',
              //           icon: Icons.details_rounded),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, top: 15),
              //         child: Text('2,700,000 SYP',
              //             style: TextStyle(
              //               fontWeight: FontWeight.bold,
              //               fontSize: 25,
              //               color: Colors.red,
              //             )),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, bottom: 10),
              //         child: Row(
              //           children: [
              //             Text('Martini Area,Shahba Rose',
              //                 style: TextStyle(
              //                   fontWeight: FontWeight.bold,
              //                   fontSize: 23,
              //                   color: Colors.black,
              //                 )),
              //             Spacer(),
              //             Padding(
              //               padding: const EdgeInsets.only(right: 5),
              //               child: MaterialButton(
              //                   shape: RoundedRectangleBorder(
              //                       borderRadius: BorderRadius.circular(15)),
              //                   child: Text(
              //                     'DELETE',
              //                     style: TextStyle(
              //                         fontSize: 23, color: Colors.red),
              //                   ),
              //                   height: 40,
              //                   minWidth: 100,
              //                   color: Colors.white,
              //                   onPressed: () {
              //                     showDialog(
              //                         context: context,
              //                         builder: (BuildContext context) {
              //                           return AlertDialog(
              //                             title: Row(
              //                               children: [
              //                                 Icon(
              //                                   Icons.done,
              //                                   color: Colors.green,
              //                                 ),
              //                                 Text(
              //                                   'Delete Done',
              //                                   style: TextStyle(
              //                                       fontSize: 20,
              //                                       fontWeight: FontWeight.bold,
              //                                       color: Colors.green),
              //                                 ),
              //                               ],
              //                             ),
              //                           );
              //                         });
              //                   }),
              //             ),
              //           ],
              //         ),
              //       ),
              //       Line(),
              //       Swiperpp(
              //           imageSrc: 'https://picsum.photos/500/500?random=4',
              //           content1: 'Rageen Place',
              //           content2: 'Details :',
              //           icon: Icons.details_rounded),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, top: 15),
              //         child: Text('2,700,000 SYP',
              //             style: TextStyle(
              //               fontWeight: FontWeight.bold,
              //               fontSize: 25,
              //               color: Colors.red,
              //             )),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 15, bottom: 10),
              //         child: Row(
              //           children: [
              //             Text('Martini Area,Shahba Rose',
              //                 style: TextStyle(
              //                   fontWeight: FontWeight.bold,
              //                   fontSize: 23,
              //                   color: Colors.black,
              //                 )),
              //             Spacer(),
              //             Padding(
              //               padding: const EdgeInsets.only(right: 5),
              //               child: MaterialButton(
              //                   shape: RoundedRectangleBorder(
              //                       borderRadius: BorderRadius.circular(15)),
              //                   child: Text(
              //                     'DELETE',
              //                     style: TextStyle(
              //                         fontSize: 23, color: Colors.red),
              //                   ),
              //                   height: 40,
              //                   minWidth: 100,
              //                   color: Colors.white,
              //                   onPressed: () {
              //                     showDialog(
              //                         context: context,
              //                         builder: (BuildContext context) {
              //                           return AlertDialog(
              //                             title: Row(
              //                               children: [
              //                                 Icon(
              //                                   Icons.done,
              //                                   color: Colors.green,
              //                                 ),
              //                                 Text(
              //                                   'Delete Done',
              //                                   style: TextStyle(
              //                                       fontSize: 20,
              //                                       fontWeight: FontWeight.bold,
              //                                       color: Colors.green),
              //                                 ),
              //                               ],
              //                             ),
              //                           );
              //                         });
              //                   }),
              //             ),
              //           ],
              //         ),
              //       ),
              //       Line(),
              //     ],
              //   ),
              // ),
              
              
//               Padding(
//                 padding: const EdgeInsets.all(15.0),
//                 child: SingleChildScrollView(
//                   child: Column(
//                     children: [
//                       textContract(
//                           textService: 'Service Type:',
//                           textNameService: 'Bedroom bruches cleaning',
//                           textNameDate: 'Contract Date:',
//                           textDate: '12/6/2023',
//                           textNameTime: 'Contract Time:',
//                           textTime: '2:00 Am',
//                           context: context),
//                       SizedBox(
//                         height: 10,
//                       ),
//                       Line(),
//                       SizedBox(
//                         height: 10,
//                       ),
//                       textContract(
//                           textService: 'Service Type:',
//                           textNameService: 'Bedroom bruches cleaning',
//                           textNameDate: 'Contract Date:',
//                           textDate: '12/6/2023',
//                           textNameTime: 'Contract Time:',
//                           textTime: '2:00 Am',
//                           context: context),
//                       SizedBox(
//                         height: 10,
//                       ),
//                       Line(),
//                       SizedBox(
//                         height: 10,
//                       ),
//                       textContract(
//                           textService: 'Service Type:',
//                           textNameService: 'Bedroom bruches cleaning',
//                           textNameDate: 'Contract Date:',
//                           textDate: '12/6/2023',
//                           textNameTime: 'Contract Time:',
//                           textTime: '2:00 Am',
//                           context: context),
//                       SizedBox(
//                         height: 10,
//                       ),
//                       Line(),
//                       SizedBox(
//                         height: 10,
//                       ),
//                       textContract(
//                           textService: 'Service Type:',
//                           textNameService: 'Bedroom bruches cleaning',
//                           textNameDate: 'Contract Date:',
//                           textDate: '12/6/2023',
//                           textNameTime: 'Contract Time:',
//                           textTime: '2:00 Am',
//                           context: context),
//                     ],
//                   ),
//                 ),
//               )
//             ]),
//       ),
//     ])));
//   }
// }
