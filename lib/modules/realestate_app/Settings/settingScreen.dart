import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/shared/components/components.dart';
import 'package:house_app/translations/app_translations.dart';
import '../../../core/constant/app_strings.dart';
import '../../../data/static/static.dart';
import '../../../layout/RealEstate_app/cubit/cubit.dart';

class settingScreen extends StatefulWidget {
  const settingScreen({Key? key}) : super(key: key);

  @override
  State<settingScreen> createState() => _settingScreenState();
}

class _settingScreenState extends State<settingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppStrings.settings.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Column(
        children: [
          changeButton(
              text: AppStrings.nightMood.tr,
              text1: '',
              color: Color(0xFF2AA8C0),
              textcolor: Colors.white,
              onpressed: () {
                //  RealCubit.get(context).changeAppMode();
              }),
          changeButton(
              text: AppStrings.signOut.tr,
              text1: '',
              color: Color(0xFF2AA8C0),
              textcolor: Colors.white,
              onpressed: () {
                exit(0);
              }),
          changeButton(
              text: AppStrings.arabic.tr,
              text1: '',
              color: Color(0xFF2AA8C0),
              textcolor: Colors.white,
              onpressed: () {
                 check=true;
                AppTranslations.changeLocale(
                  AppTranslations.arabicLang
                );
              }),
          changeButton(
              text: AppStrings.english.tr,
              text1: '',
              color: Color(0xFF2AA8C0),
              textcolor: Colors.white,
              onpressed: () {
                  check=false;
                  AppTranslations.changeLocale(
                  AppTranslations.englishLang
                );
              }),
        ],
      ),
    );
  }
}
