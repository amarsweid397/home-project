import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/shared/components/components.dart';

import '../../../core/constant/app_strings.dart';

class accountScreen extends StatefulWidget {
  const accountScreen({Key? key}) : super(key: key);

  @override
  State<accountScreen> createState() => _myadsScreenState();
}

class _myadsScreenState extends State<accountScreen>
    with SingleTickerProviderStateMixin {
  var nameController = TextEditingController();
  var numController = TextEditingController();
  var emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(tabs: [
            Tab(
              text: AppStrings.personalData.tr,
            ),
            Tab(
              text: AppStrings.modification.tr,
            ),
          ]),
          title: Text(AppStrings.myAccount.tr),
          backgroundColor: Color(0xFF2AA8C0),
          leading: IconButton(
            icon: Icon(
              CupertinoIcons.back,
              size: 30,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: TabBarView(
            physics: BouncingScrollPhysics(),
            dragStartBehavior: DragStartBehavior.down,
            children: [
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.person,
                          size: 30,
                          color: Color(0xFF2AA8C0),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Text(
                          'Amar Sweid',
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.email_outlined,
                          size: 30,
                          color: Color(0xFF2AA8C0),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Text(
                          'amarsweid@gmail.com',
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.phone_android,
                          size: 30,
                          color: Color(0xFF2AA8C0),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Text(
                          '099143256',
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      defaultText(text: AppStrings.name.tr),
                      SizedBox(
                        height: 25,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: Container(
                          width: double.infinity,
                          child: defaultTextFormField(
                              color1: Colors.black,
                              color2: Color(0xFF2AA8C0),
                              color3: Color(0xFF2AA8C0),
                              color4: Color(0xFF2AA8C0),
                              color: Colors.black,
                              controller: nameController,
                              type: TextInputType.name,
                              label:AppStrings.yourName.tr,
                              prefix: Icons.text_fields_sharp),
                        ),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      defaultText(text: AppStrings.email.tr),
                      const SizedBox(
                        height: 25,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: Container(
                          width: double.infinity,
                          child: defaultTextFormField(
                              color1: Colors.black,
                              color2: const Color(0xFF2AA8C0),
                              color3: const Color(0xFF2AA8C0),
                              color4: const Color(0xFF2AA8C0),
                              color: Colors.black,
                              controller: emailController,
                              type: TextInputType.emailAddress,
                              label:AppStrings.yourEmail.tr,
                              prefix: Icons.email_outlined),
                        ),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      defaultText(text: AppStrings.phoneNumber.tr),
                      const SizedBox(
                        height: 25,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: Container(
                          width: double.infinity,
                          child: defaultTextFormField(
                              color1: Colors.black,
                              color2: const Color(0xFF2AA8C0),
                              color3: const Color(0xFF2AA8C0),
                              color4: const Color(0xFF2AA8C0),
                              color: Colors.black,
                              controller: numController,
                              type: TextInputType.number,
                              label:AppStrings.yourNum.tr,
                              prefix: CupertinoIcons.number_circle),
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      Center(
                          child: tripBtn2(
                              content: AppStrings.save.tr,
                              icon: Icons.check_box,
                              color: const Color(0xFF2AA8C0),
                              myFunc: () {},
                              radius: 20,
                              iconcolor:
                                  const Color.fromARGB(255, 27, 101, 32)))
                    ]),
              ),
            ]),
      ),
    );
  }
}
