import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:house_app/modules/realestate_app/eventcard.dart';
import 'package:timeline_tile/timeline_tile.dart';

class MyTimeLineTile extends StatelessWidget {
  final bool isFirst;
  final bool isLast;
  final bool isPast;
  final eventCard;
  const MyTimeLineTile(
    {
      super.key,
      required this.isFirst,
      required this.isLast,
      required this.isPast,
      required this.eventCard,
    }
  ) ;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 155,
      child: TimelineTile(
        isFirst: isFirst,
        isLast: isLast,
        beforeLineStyle: LineStyle(color:isPast? Color(0xFF2AA8C0):Color.fromARGB(255, 149, 231, 247)),
        indicatorStyle: IndicatorStyle(width: 40,color:isPast? Color(0xFF2AA8C0):Color.fromARGB(255, 149, 231, 247),
        iconStyle: IconStyle(iconData: Icons.done,color:isPast? Colors.white:Color.fromARGB(255, 149, 231, 247)) ),
        endChild: EventCard(child:eventCard ,isPast:isPast ,),
      ),
    );
    
  }
}