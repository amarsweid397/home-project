import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/modules/realestate_app/mytimeline.dart';
import 'package:timeline_tile/timeline_tile.dart';
import '../../../../core/constant/app_strings.dart';
import '../../../../shared/components/components.dart';

class conditionerPrice extends StatefulWidget {
  const conditionerPrice({Key? key}) : super(key: key);

  @override
  State<conditionerPrice> createState() => _PriceScreenState();
}

class _PriceScreenState extends State<conditionerPrice> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text(
          AppStrings.conditionerPrice.tr,
          style: TextStyle(
              fontSize: 23,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      // body:
      //  Center(
      //   child: Row(
      //     mainAxisAlignment: MainAxisAlignment.center,
      //     children: [
      //       Text(
      //         AppStrings.totalPrice.tr,
      //         style: TextStyle(
      //             color: Color.fromARGB(255, 23, 71, 81),
      //             fontWeight: FontWeight.bold,
      //             fontSize: 20),
      //       ),
      //       Text(
      //         ' 876\$',
      //         style: TextStyle(
      //             color: Color.fromARGB(255, 189, 42, 42),
      //             fontWeight: FontWeight.bold,
      //             fontSize: 18),
      //       ),
      //     ],
      //   ),
      // ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 50.0),
        child: ListView(children: [
         MyTimeLineTile(isFirst: true, isLast: false, isPast: true,
         eventCard:Text('The service is being prepared',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,),) ,),
         MyTimeLineTile(isFirst: false, isLast: false, isPast: true,
         eventCard: Text('The service is on the way',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,)),),
         MyTimeLineTile(isFirst: false, isLast: false, isPast: false,
         eventCard: Text('we have arrived',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,))),
         MyTimeLineTile(isFirst: false, isLast: true, isPast: true,
         eventCard: Text('total Price:11000 SYP',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,))),
        ],),
      ),
    );
  }
}
