import 'package:chips_choice_null_safety/chips_choice_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/modules/realestate_app/Cleaning/House/cleaning_price.dart';
import 'package:house_app/modules/realestate_app/Cleaning/conditioner/cleaning_change.dart';
import 'package:house_app/shared/components/components.dart';

import '../../../../core/constant/app_strings.dart';

enum ChangeSelectClean { Week, TowWeek, Once }

class conditionerContarct extends StatefulWidget {
  const conditionerContarct({Key? key}) : super(key: key);

  @override
  State<conditionerContarct> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<conditionerContarct> {
  var tag = -1;
  var tag1 = -1;
  var tag2 = -1;

  ChangeSelectClean? _changeSelectClean;
  String? gender;
  List<String> options = [
    '2023/9/4',
    '2023/10/2',
    '2023/9/7',
    '2023/12/4',
    '2023/6/3',
    '2023/5/4',
    '2023/9/1',
  ];
  List<String> clean = [
    '08:00-08:30',
    '08:30-09:00',
    '09:30-10:00',
    '10:00-10:30',
    '10:30-11:00',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 1,
        title: Text(
          AppStrings.conditionerContarct.tr,
          style: TextStyle(
              fontSize: 23,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          defaultText(text:AppStrings.chooseDate.tr),
          SizedBox(
            height: 25,
          ),
          Select(
              tagg: tag,
              wrap: false,
              app: options,
              val: -1,
              ffc: (val) {
                setState(() {
                  tag = val;
                });
              }),
          SizedBox(
            height: 15,
          ),
          defaultText(text: AppStrings.preferrredTimeToStartTheService.tr),
          const SizedBox(
            height: 25,
          ),
          Select(
              tagg: tag1,
              wrap: false,
              val: -1,
              app: clean,
              ffc: (val) {
                setState(() {
                  tag1 = val;
                });
              }),
          changeButton(
              text: AppStrings.theNumberOfBookingTimes.tr,
              onpressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const conditionerChange()));
              },
              text1: AppStrings.changing.tr,
              color: Color.fromARGB(255, 178, 209, 233),
              textcolor: Colors.black),
          SizedBox(
            height: 140,
          ),
          Center(
            child: defaultButton(
                text: AppStrings.booking.tr,
                function: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => housePrice()));
                }),
          ),
        ],
      ),
    );
  }
}
