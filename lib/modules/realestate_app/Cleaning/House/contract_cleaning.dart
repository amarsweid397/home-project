import 'package:chips_choice_null_safety/chips_choice_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/controller/services.dart';
import 'package:house_app/core/constant/images.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/models/hous/services.dart';
import 'package:house_app/modules/realestate_app/Cleaning/House/cleaning_change.dart';
import 'package:house_app/modules/realestate_app/Cleaning/House/cleaning_price.dart';
import 'package:house_app/shared/components/components.dart';
import 'package:lottie/lottie.dart';

import '../../../../core/constant/app_strings.dart';

enum ChangeSelectClean { Week, TowWeek, Once }

class contarctCleaning extends StatefulWidget {
  const contarctCleaning({Key? key}) : super(key: key);

  @override
  State<contarctCleaning> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<contarctCleaning> {
  var tag = -1;
  var tag1 = -1;
  var tag2 = -1;

  ChangeSelectClean? _changeSelectClean;
  String? gender;
  List<String> options = [
    '2023/9/4',
    '2023/10/2',
    '2023/9/7',
    '2023/12/4',
    '2023/6/3',
    '2023/5/4',
    '2023/9/1',
  ];
  List<String> clean = [
    '08:00-08:30',
    '08:30-09:00',
    '09:30-10:00',
    '10:00-10:30',
    '10:30-11:00',
  ];
  ServicesController controller = Get.find<ServicesController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 1,
          title:  Text(
           AppStrings.householdContract.tr,
            style: TextStyle(
                fontSize: 23,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: const Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: GetBuilder<ServicesController>(builder: (context) {
          return controller.loading
              ? Container(
                  width: double.infinity,
                  height: double.infinity,
                  child: Column(
                    children: [
                      Lottie.asset(
                        AppImages.ContractLoading,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        AppStrings.JustWaitingalittleminutestocompleate.tr,
                        style: TextStyle(fontSize: 20, color: Colors.black),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                )
              : SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      defaultText(text: AppStrings.chooseDate.tr),
                      const SizedBox(
                        height: 25,
                      ),
                      GetBuilder<ServicesController>(builder: (context) {
                        return Select(
                            tagg: controller.chooseDateNumber,
                            wrap: false,
                            app: dateTime,
                            val: -1,
                            ffc: (val) {
                              controller.changeDate(val);
                            });
                      }),
                      const SizedBox(
                        height: 15,
                      ),
                      defaultText(
                          text: AppStrings.preferrredTimeToStartTheService.tr),
                      const SizedBox(
                        height: 25,
                      ),
                      GetBuilder<ServicesController>(builder: (context) {
                        return Select(
                            tagg: controller.choosetimeNumber,
                            wrap: false,
                            val: -1,
                            app: clean,
                            ffc: (val) {
                              controller.changTime(val);
                            });
                      }),
                      changeButton(
                          text: AppStrings.theNumberOfBookingTimes.tr,
                          onpressed: () {
                            Get.to(const cleaningChange());
                          },
                          text1:AppStrings.changing.tr,
                          color: const Color.fromARGB(255, 178, 209, 233),
                          textcolor: Colors.black),
                      const SizedBox(
                        height: 50,
                      ),
                      Center(
                        child: defaultButton(
                            text: AppStrings.booking.tr,
                            function: () {
                              if (controller.chooseDate == null ||
                                  controller.chooseTime == null ||
                                  controller.material == -1 ||
                                  controller.numOfEmployes == -1 ||
                                  controller.frequency == null ||
                                  controller.workingHour == -1) {
                                Get.snackbar(AppStrings.error.tr,
                                    AppStrings.pleaseCompleteallInformaitions.tr,
                                    colorText: Colors.white,
                                    backgroundColor:
                                        const Color.fromARGB(255, 173, 66, 58),
                                    duration: const Duration(seconds: 4));
                                return;
                              }
                              controller.waiting();
                              Future.delayed(const Duration(seconds: 5), () {
                                controller
                                    .addContracts(ServicesModels(
                                        date: controller.chooseDate,
                                        time: controller.chooseTime!
                                            .split('-')[0],
                                        materials: controller.material,
                                        num_of_employees:
                                            controller.numOfEmployes,
                                        repeat_time: controller.frequency,
                                        working_hours: controller.workingHour))
                                    .then((value) {
                                  if (value) {
                                    Get.to(const housePrice());
                                    controller.waiting();
                                  } else {
                                    controller.waiting();
                                    Get.snackbar(AppStrings.error.tr, AppStrings.tryAgainplease.tr,
                                        colorText: Colors.white,
                                        backgroundColor: Colors.black);
                                  }
                                });
                              });
                            }),
                      ),
                    ],
                  ),
                );
        }));
  }
}
