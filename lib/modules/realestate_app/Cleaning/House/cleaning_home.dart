import 'package:chips_choice_null_safety/chips_choice_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:house_app/controller/services.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/modules/realestate_app/Cleaning/House/contract_cleaning.dart';
import 'package:house_app/shared/components/components.dart';

import '../../../../core/constant/app_strings.dart';

class cleaningHome extends StatefulWidget {
  @override
  State<cleaningHome> createState() => _cleaningHomeState();
}

class _cleaningHomeState extends State<cleaningHome> {
  var tag = -1;
  var tag1 = -1;
  var tag2 = -1;
  List<String> options = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
  ];
  List<String> Selects = [
    '1',
    '2',
    '3',
    '4',
  ];
  List<String> clean = [
    AppStrings.yes.tr,
    AppStrings.noIhavethem.tr,
  ];
  ServicesController controller = Get.put(ServicesController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 1,
        title:  Text(
          AppStrings.houseCleaning.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: const Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          defaultText(text: AppStrings.howManyHours.tr),
          SizedBox(
            height: 15,
          ),
          GetBuilder<ServicesController>(builder: (context) {
            return Select(
                tagg: controller.workingHour!,
                wrap: false,
                val: -1,
                app: options,
                ffc: (val) {
                  controller.chooseWorkingHour(val);
                });
          }),
          const SizedBox(
            height: 15,
          ),
          defaultText(text: AppStrings.howManyProfessionals.tr),
          const SizedBox(
            height: 15,
          ),
          GetBuilder<ServicesController>(builder: (context) {
            return Select(
                tagg: controller.numOfEmployes!,
                wrap: false,
                val: -1,
                app: Selects,
                ffc: (val) {
                  controller.changeNumOfEmployes(val);
                });
          }),
          const SizedBox(
            height: 15,
          ),
          defaultText(text: AppStrings.doYouNeedCleaning.tr),
          const SizedBox(
            height: 15,
          ),
          GetBuilder<ServicesController>(builder: (context) {
            return Select(
                tagg: controller.material!,
                wrap: false,
                val: -1,
                app: clean,
                ffc: (val) {
                  controller.changeMaterials(val);
                });
          }),
          const SizedBox(
            height: 80,
          ),
          Center(
            child: defaultButton(
                text: AppStrings.next.tr,
                function: () {
                  Get.to(const contarctCleaning());
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => contarctCleaning()));
                }),
          ),
        ],
      ),
    );
  }
}
