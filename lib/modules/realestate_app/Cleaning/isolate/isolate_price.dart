import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/controller/services.dart';

import '../../../../core/constant/app_strings.dart';
import '../../../../shared/components/components.dart';
import '../../mytimeline.dart';

class isolatePrice extends StatefulWidget {
  const isolatePrice({Key? key}) : super(key: key);

  @override
  State<isolatePrice> createState() => _CancelScreenState();
}

class _CancelScreenState extends State<isolatePrice> {
  bool isBottomSheet = false;
  ServicesController controller = Get.find<ServicesController>();
  var scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text(
         AppStrings.isolatePrice.tr,
          style: TextStyle(
              fontSize: 23,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 50.0),
        child: ListView(children: [
         MyTimeLineTile(isFirst: true, isLast: false, isPast: true,
         eventCard:Text('The service is being prepared',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,),) ,),
         MyTimeLineTile(isFirst: false, isLast: false, isPast: true,
         eventCard: Text('The service is on the way',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,)),),
         MyTimeLineTile(isFirst: false, isLast: false, isPast: false,
         eventCard: Text('we have arrived',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,))),
         MyTimeLineTile(isFirst: false, isLast: true, isPast: true,
         eventCard: Text(' ${controller.price}\$',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,))),
        ],),
      ),
    );
  }
}
