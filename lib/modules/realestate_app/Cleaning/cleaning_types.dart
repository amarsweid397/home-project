import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/modules/realestate_app/Cleaning/Furniture/cleaning_contract.dart';
import 'package:house_app/modules/realestate_app/Cleaning/House/cleaning_home.dart';
import 'package:house_app/modules/realestate_app/Cleaning/conditioner/cleaning_contract.dart';
import 'package:house_app/modules/realestate_app/Cleaning/isolate/isolate_home.dart';
import '../../../core/constant/app_strings.dart';
import '../../../shared/components/components.dart';
import 'isolate/contract_isolate.dart';

class cleaningTypes extends StatefulWidget {
  const cleaningTypes({Key? key}) : super(key: key);

  @override
  State<cleaningTypes> createState() => _cleaningTypesState();
}

class _cleaningTypesState extends State<cleaningTypes> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          AppStrings.generalCleaning.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: ListView(children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
          ),
          child: ListView.separated(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) => Column(
                    children: [
                      bild(
                          content: AppStrings.householdCleaning.tr,
                          icon: Icons.arrow_forward_ios,
                          images: 'https://picsum.photos/500/500?random=1',
                          myff: () {
                            servicesNumberType = 1;
                            Get.to(cleaningHome());
                            // Navigator.of(context).push(MaterialPageRoute(
                            //     builder: (context) => cleaningHome()));
                          }),
                      bild(
                          content:  AppStrings.isolateHouse.tr,
                          icon: Icons.arrow_forward_ios,
                          images: 'https://picsum.photos/500/500?random=2',
                          myff: () {
                            servicesNumberType = 2;
                            Get.to(const furnitureContarct());
                            // Navigator.of(context).push(MaterialPageRoute(
                            //     builder: (context) => furnitureContarct()));
                          }),
                      bild(
                          content: AppStrings.conditionerCleaning.tr,
                          icon: Icons.arrow_forward_ios,
                          images: 'https://picsum.photos/500/500?random=3',
                          myff: () {
                            servicesNumberType = 3;
                            Get.to(isolateHome());
                            // Navigator.of(context).push(MaterialPageRoute(
                            //     builder: (context) => cleaningHome()));
                          }),
                      bild(
                          content: AppStrings.conditionerCleaning.tr,
                          icon: Icons.arrow_forward_ios,
                          images: 'https://picsum.photos/500/500?random=3',
                          myff: () {
                            servicesNumberType = 4;
                            Get.to(cleaningHome());
                            // Navigator.of(context).push(MaterialPageRoute(
                            //     builder: (context) => cleaningHome()));
                          }),
                    ],
                  ),
              separatorBuilder: (context, index) => const SizedBox(
                    height: 8.0,
                  ),
              itemCount: 1),
        ),
      ]),
    );
  }
}
