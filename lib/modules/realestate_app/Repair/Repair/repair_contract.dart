import 'package:chips_choice_null_safety/chips_choice_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:house_app/controller/services.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/models/hous/services.dart';
import 'package:house_app/modules/realestate_app/Cleaning/House/cleaning_price.dart';
import 'package:house_app/modules/realestate_app/Repair/Paint/paint_price.dart';
import 'package:house_app/modules/realestate_app/Repair/Repair/repair_price.dart';
import 'package:house_app/shared/components/components.dart';

import '../../../../core/constant/app_strings.dart';
import 'repair_change.dart';

enum ChangeSelectClean { Week, TowWeek, Once }

class repairContarct extends StatefulWidget {
  const repairContarct({Key? key}) : super(key: key);

  @override
  State<repairContarct> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<repairContarct> {
  var tag = -1;
  var tag1 = -1;
  var tag2 = -1;

  ChangeSelectClean? _changeSelectClean;
  String? gender;
  List<String> options = [
    '2023/9/4',
    '2023/10/2',
    '2023/9/7',
    '2023/12/4',
    '2023/6/3',
    '2023/5/4',
    '2023/9/1',
  ];
  List<String> clean = [
    '08:00-08:30',
    '08:30-09:00',
    '09:30-10:00',
    '10:00-10:30',
    '10:30-11:00',
  ];
  ServicesController controller = Get.put(ServicesController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 1,
        title:  Text(
         AppStrings.repairContract.tr,
          style: TextStyle(
              fontSize: 23,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          defaultText(text:  AppStrings.chooseDate.tr),
          SizedBox(
            height: 25,
          ),
          GetBuilder<ServicesController>(builder: (context) {
            return Select(
                tagg: controller.chooseDateNumber,
                wrap: false,
                app: dateTime,
                val: -1,
                ffc: (val) {
                  controller.changeDate(val);
                });
          }),
          const SizedBox(
            height: 15,
          ),
          defaultText(text:  AppStrings.preferrredTimeToStartTheService.tr),
          const SizedBox(
            height: 25,
          ),
          GetBuilder<ServicesController>(builder: (context) {
            return Select(
                tagg: controller.choosetimeNumber,
                wrap: false,
                val: -1,
                app: time,
                ffc: (val) {
                  controller.changTime(val);
                });
          }),
          changeButton(
              text:  AppStrings.theNumberOfBookingTimes.tr,
              onpressed: () {
                // navigaTor(context,cleaningChange());
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => repairChange()));
              },
              text1:  AppStrings.changing.tr,
              color: Color.fromARGB(255, 178, 209, 233),
              textcolor: Colors.black),
          SizedBox(
            height: 100,
          ),
          Center(
            child: defaultButton(
                text:  AppStrings.booking.tr,
                function: () {
                  if (controller.chooseDate == null ||
                      controller.chooseTime == null ||
                      controller.frequency == null) {
                    Get.snackbar( AppStrings.error.tr, AppStrings.pleaseCompleteallInformaitions.tr,
                        colorText: Colors.white,
                        backgroundColor: const Color.fromARGB(255, 173, 66, 58),
                        duration: const Duration(seconds: 4));
                    return;
                  }
                  controller
                      .addContracts(ServicesModels(
                          date: controller.chooseDate,
                          time: '19:43:03',
                          repeat_time: controller.frequency))
                      .then((value) {
                    if (value) {
                      Get.to(const repairPrice());
                    } else {
                      Get.snackbar( AppStrings.error.tr,  AppStrings.tryAgainplease.tr,
                          colorText: Colors.white,
                          backgroundColor: Colors.black);
                    }
                  });
                }),
          ),
        ],
      ),
    );
  }
}
