import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/modules/realestate_app/Repair/Paint/paint_contract.dart';
import 'package:house_app/modules/realestate_app/Repair/tanks/tanks_contract.dart';
import 'package:lottie/lottie.dart';
import '../../../core/constant/app_strings.dart';
import '../../../shared/components/components.dart';
import 'Repair/repair_contract.dart';

class repairTypes extends StatefulWidget {
  const repairTypes({Key? key}) : super(key: key);

  @override
  State<repairTypes> createState() => _cleaningTypesState();
}

class _cleaningTypesState extends State<repairTypes> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title:  Text(
          AppStrings.generalRepair.tr,
          style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Color(0xFF2AA8C0)),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: const Icon(
            CupertinoIcons.back,
            size: 30,
            color: Color(0xFF2AA8C0),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: ListView(children: [
        LottieBuilder.network(
          'https://assets7.lottiefiles.com/packages/lf20_jpgqazch.json',
          fit: BoxFit.contain,
          height: 200,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
          ),
          child: ListView.separated(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) => Column(
                    children: [
                      bild(
                          content: AppStrings.homePaint.tr,
                          icon: Icons.arrow_forward_ios,
                          images: 'https://picsum.photos/500/500?random=1',
                          myff: () {
                            servicesNumberType = 5;
                            Get.to(const paintContarct());
                            // Navigator.of(context).push(MaterialPageRoute(
                            //     builder: (context) => repairContarct()));
                          }),
                      //  SizedBox(height: 15,),
                      bild(
                          content:AppStrings.cleaningOfWaterTanks.tr,
                          icon: Icons.arrow_forward_ios,
                          images: 'https://picsum.photos/500/500?random=2',
                          myff: () {
                            servicesNumberType = 6;
                            Get.to(const tanksContarct());
                            // Navigator.of(context).push(MaterialPageRoute(
                            //     builder: (context) => paintContarct()));
                          }),
                      //  SizedBox(height: 15,),
                      bild(
                          content: AppStrings.maintenanceAndRepair.tr,
                          icon: Icons.arrow_forward_ios,
                          images: 'https://picsum.photos/500/500?random=3',
                          myff: () {
                            servicesNumberType = 7;
                            Get.to(const repairContarct());
                            // Navigator.of(context).push(MaterialPageRoute(
                            //     builder: (context) => tanksContarct()));
                          }),
                    ],
                  ),
              separatorBuilder: (context, index) => SizedBox(
                    height: 8.0,
                  ),
              itemCount: 1),
        ),
      ]),
    );
  }
}
