import 'package:get/get.dart';
import 'package:house_app/modules/realestate_app/Rent/farm/farm_contract.dart';
import 'package:house_app/modules/realestate_app/Rent/land/land_contract.dart';
import 'package:house_app/modules/realestate_app/Sale/home/homesale_types.dart';
import 'package:house_app/modules/realestate_app/Sale/shop/shope_types.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import '../../../core/constant/app_strings.dart';
import '../../../shared/components/components.dart';

class SaletypesScreen extends StatefulWidget {
  const SaletypesScreen({Key? key}) : super(key: key);

  @override
  State<SaletypesScreen> createState() => _cleaningTypesState();
}

class _cleaningTypesState extends State<SaletypesScreen> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            AppStrings.saleTypes.tr,
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: ListView(
          children: [
            type(
                text: AppStrings.houseForSale.tr,
                myf: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => homeTypesSale()));
                },
                icon: Icons.home),
            type(
                text: AppStrings.landForSale.tr,
                myf: () {
                  Get.to(landContract());
                },
                icon: Icons.landscape),
            type(
              text: AppStrings.farmForSale.tr,
              myf: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => farmContract()));
              },
              icon: Icons.maps_home_work_outlined,
            ),
            type(
                text: AppStrings.shopForSale.tr,
                myf: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => saleShopTypes()));
                },
                icon: Icons.home_outlined)
          ],
        ));
  }
}
