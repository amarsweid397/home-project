import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
//import 'package:house_app/modules/realestate_app/Cleaning/House/cleaning_price.dart';
import 'package:lottie/lottie.dart';
import '../../../../core/constant/app_strings.dart';
import '../../../../shared/components/components.dart';
import '../../Rent/home/bigHome/bighome_contract.dart';
import '../../Rent/home/room/room_contract.dart';
import '../../Rent/home/smallHome/smallhome_contract.dart';

class homeTypesSale extends StatefulWidget {
  const homeTypesSale({Key? key}) : super(key: key);

  @override
  State<homeTypesSale> createState() => cleaningTypesState();
}

class cleaningTypesState extends State<homeTypesSale> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title:  Text(
            AppStrings.houseTypes.tr,
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: const Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: ListView(
          children: [
            LottieBuilder.network(
              'https://assets9.lottiefiles.com/packages/lf20_qzgnpky3.json',
              fit: BoxFit.contain,
              height: 180,
            ),
            type(
                text: AppStrings.roomForSale.tr,
                myf: () {
                  houseType = 0;
                  Get.to(const roomContract());
                  // Navigator.of(context).push(
                  //     MaterialPageRoute(builder: (context) => roomContract()));
                },
                icon: Icons.home_outlined),
            type(
                text: AppStrings.bigHouseForSale.tr,
                myf: () {
                  houseType = 1;
                  Get.to(const bigHomeContract());
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => bigHomeContract()));
                },
                icon: Icons.home_work_rounded),
            type(
                text: AppStrings.smallHouseForSale.tr,
                myf: () {
                  houseType = 2;
                  Get.to(const smallHomeContract());
                  // Navigator.of(context).push(MaterialPageRoute(
                  //     builder: (context) => smallHomeContract()));
                },
                icon: Icons.home),
          ],
        ));
  }
}
