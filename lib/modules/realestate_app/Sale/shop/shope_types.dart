import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
// import 'package:house_app/modules/realestate_app/Rent/shop/shop_contract.dart';
import 'package:house_app/modules/realestate_app/Sale/shop/shop_contract.dart';
//import 'package:house_app/modules/realestate_app/Rent/shop/shope_types.dart';
import 'package:lottie/lottie.dart';
import '../../../../shared/components/components.dart';

class saleShopTypes extends StatefulWidget {
  const saleShopTypes({Key? key}) : super(key: key);

  @override
  State<saleShopTypes> createState() => _cleaningTypesState();
}

class _cleaningTypesState extends State<saleShopTypes> {
  // int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text(
            'Shop Types',
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                color: Color(0xFF2AA8C0)),
          ),
          backgroundColor: Colors.white,
          elevation: 1,
          leading: IconButton(
            icon: const Icon(
              CupertinoIcons.back,
              size: 30,
              color: Color(0xFF2AA8C0),
            ),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        body: ListView(
          children: [
            LottieBuilder.network(
              'https://assets10.lottiefiles.com/packages/lf20_8tkhf9ja.json',
              fit: BoxFit.contain,
              height: 180,
            ),
            type(
                text: 'Office for sale',
                myf: () {
                  shopeType = 1;
                  Get.to(const shopContract());
                  // Navigator.of(context).push(
                  //     MaterialPageRoute(builder: (context) => shopContract()));
                },
                icon: Icons.home),
            type(
                text: 'Pharmacy for sale',
                myf: () {
                  shopeType = 2;
                  Get.to(const shopContract());
                  // Navigator.of(context).push(
                  //     MaterialPageRoute(builder: (context) => shopContract()));
                },
                icon: Icons.landscape),
            type(
              text: 'Commercial shop for sale',
              myf: () {
                shopeType = 3;
                Get.to(const shopContract());
                // Navigator.of(context).push(
                //     MaterialPageRoute(builder: (context) => shopContract()));
              },
              icon: Icons.maps_home_work_outlined,
            ),
            type(
                text: 'Clinic for sale',
                myf: () {
                  shopeType = 4;
                  Get.to(const shopContract());
                  // Navigator.of(context).push(
                  //     MaterialPageRoute(builder: (context) => shopContract()));
                },
                icon: Icons.home_outlined)
          ],
        ));
  }
}
