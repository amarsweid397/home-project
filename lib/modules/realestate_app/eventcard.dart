import 'package:flutter/cupertino.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class EventCard extends StatelessWidget {
  final bool isPast;
  final child;
  const EventCard(
    {
      super.key,
      required this.isPast,
      required this.child,
    }
  ) ;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:EdgeInsets.all(25) ,
      padding:EdgeInsets.all(25) ,
      decoration: BoxDecoration(
        color: isPast? Color(0xFF2AA8C0):Color.fromARGB(255, 149, 231, 247),
        borderRadius: BorderRadius.circular(8),
      ),
      child: child,
    );
    
  }
}