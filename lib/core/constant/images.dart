class AppImages {
  static const String init = 'assets/images/';
  static const String LoadingImages = '${init}loading.json';
  static const String ContractLoading = '${init}contract_loading.json';
}
