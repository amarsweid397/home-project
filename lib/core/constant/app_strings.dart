abstract class AppStrings {
  static const String soporific='soporific';
  static const String sale='sale';
  static const String rent='rent';
  static const String repair='repair';
  static const String clean='clean';
  static const String insect='insect';
  static const String mostVisitedRentalProperties='mostVisitedRentalProperties';
  static const String home='home';
  static const String land='land';
  static const String farm='farm';
  static const String shop='shop';
  static const String contract='contract';
  static const String posters='posters';
  static const String settings='settings';
  static const String houseForSale='houseForSale';
  static const String landForSale='landForSale';
  static const String farmForSale='farmForSale';
  static const String shopForSale='shopForSale';
  static const String houseForRent='houseForRent';
  static const String landForRent='landForRent';
  static const String farmForRent='farmForRent';
  static const String shopForRent='shopForRent';
  static const String maintenanceAndRepair='maintenanceAndRepair';
  static const String homePaint='homePaint';
  static const String cleaningOfWaterTanks='cleaningOfWaterTanks';
  static const String householdCleaning='householdCleaning';
  static const String furnitureCleaning='furnitureCleaning';
  static const String conditionerCleaning='conditionerCleaning';
  static const String isolateHouse='isolateHouse';
  static const String pestControl='pestControl';
  static const String disinfectionAndSterilization='disinfectionAndSterilization';
  static const String saleTypes='saleTypes';
  static const String rentTypes='rentTypes';
  static const String generalRepair='generalRepair';
  static const String generalCleaning='generalCleaning';
  static const String sterilizationService='sterilizationService';
  static const String whatDoYouWantToAdvertise='whatDoYouWantToAdvertise';
  static const String chooseTheAppropriateCategoryForYourAd='chooseTheAppropriateCategoryForYourAd';
  static const String myAds='myAds';
  static const String myAccount='myAccount';
  static const String personalData='personalData';
  static const String modification='modification';
  static const String name='name';
  static const String email='email';
  static const String phoneNumber='phoneNumber';
  static const String yourName='yourName';
  static const String yourEmail='yourEmail';
  static const String yourNum='yourNum';
  static const String save='save';
  static const String whoAreWe='whoAreWe';
  static const String nightMood='nightMood';
  static const String signOut='signOut';
  static const String theAplication='theAplication';//شرح عن التطبيق
  static const String serviceType='serviceType';
  static const String contractDate='contractDate';
  static const String contractTime='contractTime';
  static const String delete='delete';
  static const String roomForSale='roomForSale';
  static const String bigHouseForSale='bigHouseForSale';
  static const String smallHouseForSale='smallHouseForSale';
  static const String pharmacyForSale='pharmacyForSale';
  static const String officeForSale='pharmacyForSale';
  static const String clinicForSale='pharmacyForSale';
  static const String commercialForSale='pharmacyForSale';
  static const String commercialForRent='pharmacyForRent';
  static const String roomForRent='roomForRent';
  static const String bigHouseForRent='bigHouseForRent';
  static const String smallHouseForRent='smallHouseForRent';
  static const String pharmacyForRent='pharmacyForRent';
  static const String officeForRent='officeForRent';
  static const String clinicForRent='clinicForRent';
  static const String location='location';
  static const String chooseYourarea='chooseYourarea';
  static const String price='price';
  static const String chooseFloor='chooseFloor';
  static const String ground='ground';
  static const String roomsNumber='roomsNumber';
  static const String numberOfRooms='numberOfRooms';
  static const String roomSpace='roomSpace';
  static const String HouseSpace='HouseSpace';
  static const String shopSpace='shopSpace';
  static const String landSpace='landSpace';
  static const String farmSpace='farmSpace';
  static const String houseDestination='houseDestination';
  static const String roomDestination='roomDestination';
  static const String shopDestination='shopDestination';
  static const String farmDestination='farmDestination';
  static const String landDestination='landDestination';
  static const String showTheResult='showTheResult';
  static const String now='now';
  static const String roomContract='roomContract';
  static const String bigHouseContract='bigHouseContract';
  static const String smallHouseContract='smallHouseContract';
  static const String landContract='landContract';
  static const String farmContract='farmContract';
  static const String shopContract='shopContract';
  static const String repairContract='shopContract';
  static const String insectContract='isectContract';
  static const String insectPrice='insectPrice';
  static const String repairPrice='repairPrice';
  static const String cleaningContract='shopContract';
  static const String housesPage='housesPage';
  static const String landsPage='landsPage';
  static const String farmsPage='farmsPage';
  static const String shopsPage='farmsPage';
  static const String houseDetails='houseDetails';
  static const String landDetails='houseDetails';
  static const String farmDetails='houseDetails';
  static const String shopDetails='houseDetails';
  static const String space='space';
  static const String title='title';
  static const String rentsale='rent/sale';
  static const String decor='decor';
  static const String floorNumber='floorNumber';
  static const String destination='destination';
  static const String buildingNumber='buildingNumber';
  static const String ownerName='ownerName';
  static const String call='call';
  static const String propertyInformation='propertyInformation';
  static const String swimmingPool='swimmingPool';
  static const String landType='landType';
  static const String to='to';
  static const String chooseDate='chooseDate';
  static const String choosefrequency='choosefrequency';
  static const String preferrredTimeToStartTheService='preferrredTimeToStartTheService';
  static const String theNumberOfBookingTimes='theNumberOfBookingTimes';
  static const String changing='changing';
  static const String changeScreen='changeScreen';
  static const String booking='booking';
  static const String once='once';
  static const String weekly='weekly';
  static const String everyToWeeks='everyToWeeks';
  static const String discount='discount';
  static const String theSameSopecOnce='theSameSopecOnce';
  static const String theSameSopecWeekly='theSameSopecWeekly';
  static const String theSameSopecEveryWeeks='theSameSopecEveryWeeks';
  static const String houseCleaning='houseCleaning';
  static const String howManyHours='howManyHours';
  static const String howManyProfessionals='howManyProfessionals';
  static const String doYouNeedCleaning='doYouNeedCleaning';           
  static const String next='next';           
  static const String totalPrice='totalPrice';       
  static const String adYourAd='adYourAd';       
  static const String by='by';       
  static const String homeState='homeState';       
  static const String payment='payment';       
  static const String addImages='adYourAd';       
  static const String furnituers='furnituers';       
  static const String furnituersType='furnituersType';       
  static const String area='area';             
  static const String arabic='Arabic';             
  static const String english='English';             
  static const String pickYourPhotos='pickYourPhotos';             
  static const String addAsMany='addAsMany';             
  static const String sittingRoomFurnituers='sittingRoomFurnituers';             
  static const String bedRoomFurnituers='bedRoomFurnituers';             
  static const String bathRoomFurnituers='bathRoomFurnituers';             
  static const String kitchenFurnituers='kitchenFurnituers';             
  static const String annual='annual';             
  static const String semiAnnual='semiAnnual';             
  static const String quarterly='quarterly';             
  static const String monthly='monthly';             
  static const String mediator='mediator';             
  static const String owner='owner';             
  static const String furnished='furnished';             
  static const String partiallyFurnished='partiallyFurnished';             
  static const String unfurnished='unfurnished';                      
  static const String south='south';             
  static const String western='western';             
  static const String east='east';             
  static const String north='north';             
  static const String southWestern='southWestern';             
  static const String northWestern='northWestern';             
  static const String southEast='southEast';             
  static const String northEast='northEast';             
  static const String agricultural='agricultural';             
  static const String building='building';             
  static const String alFurkan='alFurkan';             
  static const String alSabil='alSabil';             
  static const String newAleppo='newAleppo';             
  static const String alZahraa='alZahraa';             
  static const String almuhandesen='almuhandesen';             
  static const String alsalhen='alsalhen';             
  static const String almartini='almartini';             
  static const String newShahbaa='newShahbaa';             
  static const String oldShahbaa='oldShahbaa';             
  static const String almidan='almidan';             
  static const String sayfAldawla='sayfAldawla';             
  static const String babAlhadid='babAlhadid';             
  static const String salahAldiyn='salahAldiyn';             
  static const String babAlnaser='babAlnaser';             
  static const String alshaar='alshaar';             
  static const String no='no';             
  static const String yes='yes';             
  static const String yourTitle='yourTitle';             
  static const String squaremeters='squaremeters';             
  static const String warning='warning';             
  static const String error='error';             
  static const String room='room';             
  static const String bighouse='bighouse';             
  static const String smallhouse='smallhouse';             
  static const String pharmacy='pharmacy';             
  static const String paymentType='paymentType';             
  static const String houseTypes='houseTypes';             
  static const String shopTypes='shopTypes';             
  static const String office='office';             
  static const String clinic='clinic';             
  static const String login='login';             
  static const String emailAdress='emailAdress';             
  static const String signIn='signIn';             
  static const String donthaveanaacount='donthaveanaacount';             
  static const String registerNow='registerNow';             
  static const String createAnAccount='createAnAccount';             
  static const String welcomeWithUs='welcomeWithUs';             
  static const String password='password'; 
  static const String confirmpassword='confirmpassword'; 
  static const String done='done'; 
  static const String homee='homee'; 
  static const String noFarmsExistWithThisDetailsYet='noFarmsExistWithThisDetailsYet';             
  static const String nolandsExistWithThisDetailsYet='nolandsExistWithThisDetailsYet';             
  static const String nohousesExistWithThisDetailsYet='nohousesExistWithThisDetailsYet';             
  static const String noshopsExistWithThisDetailsYet='noshopsExistWithThisDetailsYet';                          
  static const String thereareErrorinEmailorPasswordpleaseCheck='ThereareErrorinEmailorPasswordpleaseCheck';                          
  static const String emailisempty='emailisempty';                          
  static const String enteryourpassword="enteryourpassword";                          
  static const String nameIsrequired="nameIsrequired";                          
  static const String numberIsrequired="numberIsrequired";                          
  static const String emailIsrequired="emailIsrequired";                          
  static const String passwordIsrequired="passwordIsrequired";                          
  static const String confirmYourpassword="confirmYourpassword";                          
  static const String noIhavethem= 'noIhavethem';             
  static const String householdContract='householdContract';                         
  static const String furnitureContract='furnitureContract';                         
  static const String conditionerContarct='conditionerContarct';                         
  static const String isolateContract='isolateContract';                         
  static const String householdPrice='householdContract';                         
  static const String furniturePrice='furnitureContract';                         
  static const String conditionerPrice='conditionerContarct';                         
  static const String isolatePrice='isolateContract';                         
  static const String commercialShop='commercialShop';                         
  static const String pleaseCompleteallInformaitions='pleaseCompleteallInformaitions';                         
  static const String tryAgainplease='tryAgainplease ';                         
  static const String JustWaitingalittleminutestocompleate='JustWaitingalittleminutestocompleate';                         
  static const String pleasecheckfromconnectionInternet='pleasecheckfromconnectionInternet';             
  static const String thereareSomeMissingValues='thereareSomeMissingValues';             
             
      












  
 

  














}
