import 'package:sqflite/sqflite.dart';

class DBHelper {
  Database? _db;
  Future<Database> get db async {
    if (_db != null) {
      return _db!;
    }
    _db = await initial();
    return _db!;
  }

  Future<Database> initial() async {
    String database = await getDatabasesPath();
    String path = database + 'info.db';
    return await openDatabase(path, version: 1,
        onCreate: ((Database db, int version) {
      db.execute(
          'CREATE TABLE Details(id INTEGER PRIMARY KEY AUTOINCREMENT , typeOfItem INTEGER, price INTEGER NOT NULL , space STRING ,title STRING , destination STRING , floorNumber STRING , type INTEGER , decor INTEGER ,buildingNumber INTEGER , ownerName STRING ,landType STRING, swimmingPool INTEGER)');
    }));
  }

  insert() async {
    var aux = await db;
    aux.insert('Details', {});
  }
}
