import 'dart:convert';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../models/user_model.dart';

class getStorageServic extends GetxController {
  late GetStorage getstorage;
  onInit() {
    getstorage = GetStorage();
  }

  setValueStorage(UserToken usertoken) {
    String value = json.encode(usertoken.toJson());
    getstorage.write('tokenInfo', value);
  }

  UserToken getvalueStorage() {
    var value = getstorage.read('tokenInfo');
    UserToken userToken =
        UserToken.fromJson(json.decode(getstorage.read('tokenInfo')));

    return userToken;
  }
}
