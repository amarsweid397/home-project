import 'package:get/get.dart';
import 'package:house_app/controller/room_screen.dart';
import 'package:house_app/core/services/getxStorage.dart';

import '../../controller/house_most_visited.dart';

class binding extends Bindings {
  @override
  void dependencies() {
    Get.put(getStorageServic());
    Get.put(HouseMostVisitedController());
    // Get.put(RoomScreenController());
  }
}
