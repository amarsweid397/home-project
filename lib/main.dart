import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:house_app/core/services/binding.dart';
import 'package:house_app/core/services/getxservices.dart';
import 'package:house_app/layout/RealEstate_app/realestate_layout.dart';
import 'package:house_app/modules/realestate_app/Cleaning/House/cleaning_change.dart';
import 'package:house_app/modules/realestate_app/Cleaning/conditioner/cleaning_change.dart';
import 'package:house_app/modules/realestate_app/Cleaning/conditioner/cleaning_price.dart';
import 'package:house_app/modules/realestate_app/Rent/home/room/room_contract.dart';
import 'package:house_app/modules/realestate_app/Rent/shop/shop_contract.dart';

import 'package:house_app/shared/styles/themes.dart';
import 'package:house_app/translations/app_translations.dart';
import 'modules/home_app/login/home_login.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialBinding: binding(),
      theme: lightTheme,
      // darkTheme:darkTheme ,
      // themeMode:RealCubit.get(context).isDark ? ThemeMode.dark:ThemeMode.light,
      debugShowCheckedModeBanner: false,
      home: RealEstateLayout(),
      // Locale
      locale: AppTranslations.getAppLocale(),
      translations: AppTranslations(),
    ); 
  }
}
