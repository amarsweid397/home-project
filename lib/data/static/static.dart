import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../core/constant/app_strings.dart';

List<String> Direction = [
  'SOUTH',
  'WIDTH',
  'EAST',
  'NORTH',
  'SOUTH_WIDTH',
  'NORTH_WIDTH',
  'SOUTH_EAST',
  'NORTH_EAST',
];

var places = [
  AppStrings.chooseYourarea.tr,
    AppStrings.alFurkan.tr,
    AppStrings.newAleppo.tr,
    AppStrings.alSabil.tr,
    AppStrings.almuhandesen.tr,
    AppStrings.alsalhen.tr,
    AppStrings.almartini.tr,
    AppStrings.newShahbaa.tr,
    AppStrings.oldShahbaa.tr,
    AppStrings.almidan.tr,
    AppStrings.sayfAldawla.tr,
    AppStrings.babAlhadid.tr,
    AppStrings.salahAldiyn.tr,
    AppStrings.babAlnaser.tr,
    AppStrings.alshaar.tr,
];
List<String> dateTime = [
  DateFormat('yyy-MM-dd').format(DateTime.now()),
  DateFormat('yyy-MM-dd').format(DateTime.now().add(const Duration(days: 1))),
  DateFormat('yyy-MM-dd').format(DateTime.now().add(const Duration(days: 2))),
  DateFormat('yyy-MM-dd').format(DateTime.now().add(const Duration(days: 3))),
  DateFormat('yyy-MM-dd').format(DateTime.now().add(const Duration(days: 4))),
  DateFormat('yyy-MM-dd').format(DateTime.now().add(const Duration(days: 5))),
  DateFormat('yyy-MM-dd').format(DateTime.now().add(const Duration(days: 6))),
];
List<String> time = [
  '08:00-08:30',
  '08:30-09:00',
  '09:30-10:00',
  '10:00-10:30',
  '10:30-11:00',
];
bool check=false;
List<String> paymenttype = ['Yearly', 'Monthly', 'Quarterly', 'Midterm'];

int? rent_buy;
int houseType = 0;
int shopeType = 1;
int servicesNumberType = 0;
int addHomeType = 0;
int TypeofAdd = 0;
int shopType = 0;
