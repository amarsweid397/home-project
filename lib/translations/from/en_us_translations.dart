import 'package:house_app/core/constant/app_strings.dart';

final Map<String, String> enUS = {
  
  AppStrings.soporific:'Soporific',
  AppStrings.sale:'Sale',
  AppStrings.rent:'Rent',
  AppStrings.repair:'Repair',
  AppStrings.clean:'Clean',
  AppStrings.insect:'insect',
  AppStrings.mostVisitedRentalProperties:'Most Visited Rental Properties:',
  AppStrings.home:'Home',
  AppStrings.land:'Land',
  AppStrings.farm:'Farm',
  AppStrings.shop:'Shop',
  AppStrings.posters:'Posters',
  AppStrings.settings:'Settings',
  AppStrings.houseForSale:'House for sale',
  AppStrings.roomForSale:'Room for sale',
  AppStrings.landForSale:'Land for sale',
  AppStrings.farmForSale:'Farm for sale',
  AppStrings.shopForSale:'Shop for sale',
  AppStrings.houseForRent:'House for rent',
  AppStrings.landForRent:'Land for rent',
  AppStrings.farmForRent:'Farm for rent',
  AppStrings.shopForRent:'Shop for rent',
  AppStrings.roomForRent:'Room for rent',
  AppStrings.bigHouseForRent:'Big house for rent',
  AppStrings.smallHouseForRent:'Small house for rent',
  AppStrings.bigHouseForSale:'Big house for sale',
  AppStrings.smallHouseForSale:'Small house for sale',
  AppStrings.maintenanceAndRepair:'Maintenance and repair',
  AppStrings.homePaint:'Home Paint',
  AppStrings.cleaningOfWaterTanks:'Cleaning of water tanks',
  AppStrings.householdCleaning:'Household Cleaning',
  AppStrings.furnitureCleaning:'Furniture Cleaning',
  AppStrings.conditionerCleaning:'Air Conditioner Cleaning',
  AppStrings.isolateHouse:'Isolate House',
  AppStrings.pestControl:'Pest Control',
  AppStrings.disinfectionAndSterilization:'Disinfection and Sterilization',
  AppStrings.saleTypes:'Sale Types',
  AppStrings.rentTypes:'Rent Types',
  AppStrings.generalRepair:'General Repair',
  AppStrings.generalCleaning:'General Cleaning',
  AppStrings.sterilizationService:'Sterilization Service',
  AppStrings.whatDoYouWantToAdvertise:'what do you want to advertise?',
  AppStrings.chooseTheAppropriateCategoryForYourAd:'Choose the appropriate category for your ad',
  AppStrings.myAds:'My Ads',
  AppStrings.myAccount:'My account',
  AppStrings.whoAreWe:'Who are we!',
  AppStrings.personalData:'Personal Data',
  AppStrings.modification:'modification',
  AppStrings.name:'Name:',
  AppStrings.email:'Email:',
  AppStrings.login:'Login',
  AppStrings.emailAdress:'Email Adress',
  AppStrings.password:'Password',
  AppStrings.signIn:'SIGN IN',
  AppStrings.donthaveanaacount:'Dont have an account',
  AppStrings.registerNow:'Register Now',
  AppStrings.createAnAccount:'Create an account',
  AppStrings.welcomeWithUs:'Welcome with us',
  AppStrings.confirmpassword:'Confirm Password',
  AppStrings.done:'Done',
  AppStrings.emailisempty:'Email is empty',
  AppStrings.enteryourpassword:'Enter your password',
  AppStrings.nameIsrequired:'Name Is required!!',
  AppStrings.numberIsrequired:'Number Is required!!',
  AppStrings.emailIsrequired:'Email Is required!!',
  AppStrings.passwordIsrequired:'Password Is required!!',
  AppStrings.done:'Done',
  AppStrings.done:'Done',
  AppStrings.done:'Done',
  AppStrings.done:'Done',
  AppStrings.done:'Done',
  AppStrings.done:'Done',
  AppStrings.phoneNumber:'Phone Number:',
  AppStrings.yourName:'your name',
  AppStrings.yourNum:'your num',
  AppStrings.yourEmail:'your email',
  AppStrings.save:'SAVE',
  AppStrings.nightMood:'Night Mood',
  AppStrings.signOut:'Sign Out',
  AppStrings.arabic:'Arabic',
  AppStrings.english:'English',
  AppStrings.theAplication:'The application makes it easy for you to search for all the real estate that comes to your mind, including homes, shops, farms, and lands It makes it easier for you to search for a house, whether for rent or for sale, and provides additional services such as cleaning, repair and pesticide services.',
  AppStrings.serviceType:'Service Type:',
  AppStrings.contractDate:'Contract Date:',
  AppStrings.contractTime:'Contract Time:',
  AppStrings.delete:'Delete',
  AppStrings.pharmacyForRent:'Pharmacy for rent',
  AppStrings.officeForRent:'Office for rent',
  AppStrings.clinicForRent:'Clinic for rent',
  AppStrings.commercialForSale:'Commercial shop for sale',
  AppStrings.commercialForRent:'Commercial for sale',
  AppStrings.clinicForRent:'Clinic for sale',
  AppStrings.pharmacyForSale:'Pharmacy for sale',
  AppStrings.officeForSale:'Office for sale',
  AppStrings.location:'location:',
  AppStrings.chooseYourarea:'Choose your area',
  AppStrings.price:'Price(SYP):',
  AppStrings.paymentType:'Payment Types:',
  AppStrings.shopTypes: 'Shops Types',
  AppStrings.houseTypes: 'Houses Types',
  AppStrings.chooseFloor:'Choose Floor:',
  AppStrings.roomsNumber:'Rooms Number:',
  AppStrings.numberOfRooms:'Number of rooms:',
  AppStrings.roomSpace:'Room Space:',
  AppStrings.roomDestination:'Room Destination:',
  AppStrings.showTheResult:'Show the result',
  AppStrings.now:'now',
  AppStrings.roomContract:'Room Contract',
  AppStrings.bigHouseContract:'Big House Contract',
  AppStrings.smallHouseContract:'Small House Contract',
  AppStrings.landContract:'Land Contract',
  AppStrings.farmContract:'Farm Contract',
  AppStrings.shopContract:'Shop Contract',
  AppStrings.homee: 'Home',
  AppStrings.repairContract:'Repair Contract',
  AppStrings.insectContract:'Insect Contract',
  AppStrings.cleaningContract:'Cleaning Contract',
  AppStrings.housesPage:'Houses Page',
  AppStrings.landsPage:'Lands Page',
  AppStrings.farmsPage:'Farms Page',
  AppStrings.shopsPage:'Shops Page',
  AppStrings.insectPrice:'Insect Price',
  AppStrings.repairPrice:'Repair Price',
  AppStrings.houseDetails:'House Details',
  AppStrings.landDetails:'Land Details',
  AppStrings.farmDetails:'Farm Details',
  AppStrings.shopDetails:'Shop Details',
  AppStrings.space:'Space',
  AppStrings.title:'Title',
  AppStrings.rentsale:'Rent/Sale',
  AppStrings.decor:'Decor?',
  AppStrings.destination:'Destination',
  AppStrings.floorNumber:'Floor Number',
  AppStrings.buildingNumber:'Building Number',
  AppStrings.ownerName:'Owner Name',
  AppStrings.call:'call',
  AppStrings.propertyInformation:'Property Information:',
  AppStrings.swimmingPool:'Swimming Pool',
  AppStrings.landType:'Land Type',
  AppStrings.to:'to',
  AppStrings.chooseDate:'Choose Date:',
  AppStrings.preferrredTimeToStartTheService:'Preferrred time to start the service:',
  AppStrings.theNumberOfBookingTimes:'the number of booking times',
  AppStrings.changing:'Changing',
  AppStrings.booking:'booking',
  AppStrings.choosefrequency:'Choose frequency:',
  AppStrings.changeScreen:'Change Screen',
  AppStrings.once:'Once',
  AppStrings.weekly:'weekly',
  AppStrings.everyToWeeks:'Every two weeks',
  AppStrings.discount:'Discount',
  AppStrings.theSameSopecOnce:'The same specialist will provide the service every week, and you can cancel the subscription at any time',
  AppStrings.theSameSopecWeekly:'The same specialist will provide the service every week, and you can cancel the subscription at any time',
  AppStrings.theSameSopecEveryWeeks:'The same specialist will provide the service every two weeks, and you can cancel the subscription at any time',
  AppStrings.houseCleaning:'House Cleaning',
  AppStrings.howManyHours:'How many hours does the cleaning lady need?',
  AppStrings.howManyProfessionals:'tHow many professionals do you need?',
  AppStrings.doYouNeedCleaning:'Do you need cleaning supplies?',
  AppStrings.next:'Next',
  AppStrings.totalPrice:'Total Price:',
  AppStrings.adYourAd:'Add your ad',
  AppStrings.by:'By:',
  AppStrings.homeState:'Home State:',
  AppStrings.payment:'Payment:',
  AppStrings.furnituers:'Furnituers:',
  AppStrings.furnituersType:'Furnituers Type:',
  AppStrings.area:'Area:',
  AppStrings.pickYourPhotos:'Pick your photos',
  AppStrings.addAsMany:'Add as many details and pictures!',
  AppStrings.arabic:'Arabic',
  AppStrings.english:'English',
  AppStrings.sittingRoomFurnituers:'Sitting room furnituers',           
  AppStrings.bedRoomFurnituers:'Bedroom furnituers',            
  AppStrings.bathRoomFurnituers:'Bathroom furnituers',            
  AppStrings.kitchenFurnituers:'Kitchen furnituers',           
  AppStrings.annual:'Annual',             
  AppStrings.semiAnnual:'Semi-Annual',            
  AppStrings.quarterly:'Quarterly',            
  AppStrings.monthly:'Monthly',            
  AppStrings.mediator:'Mediator',            
  AppStrings.owner:'Owner',             
  AppStrings.furnished:'Furnished',             
  AppStrings.partiallyFurnished:'Partially Furnished',             
  AppStrings.unfurnished:'Unfurnished',                      
  AppStrings.south:'South',            
  AppStrings.western:'Western',            
  AppStrings.east:'East',             
  AppStrings.north:'North',             
  AppStrings.southWestern:'South-Western',            
  AppStrings.northWestern:'North-Western',             
  AppStrings.southEast:'South-East',            
  AppStrings.northEast:'North-East',           
  AppStrings.agricultural:'Agricultural',            
  AppStrings.building:'Building',             
  AppStrings.alFurkan:'Al-Furkan',             
  AppStrings.newAleppo:'New Aleppo',            
  AppStrings.alZahraa:'al-Zahraa',             
  AppStrings.almuhandesen:'Al-Muhandesen',             
  AppStrings.alsalhen:'Al-Salhen',             
  AppStrings.almartini:'Al-Martini',             
  AppStrings.newShahbaa:'New Shahbaa',            
  AppStrings.oldShahbaa:'Old Shahbaa',           
  AppStrings.almidan:'Almidan',             
  AppStrings.alSabil:'Al-Sabil',             
  AppStrings.sayfAldawla:'Sayf Aldawla',            
  AppStrings.babAlhadid:'Bab Alhadid',            
  AppStrings.salahAldiyn:'Salah Aldiyn',             
  AppStrings.babAlnaser:'Bab Alnaser',             
  AppStrings.alshaar:'Alshaar',
  AppStrings.no:'No',
  AppStrings.yes:'Yes',
  AppStrings.yourTitle:'your title',
  AppStrings.squaremeters:'Square Meters',
  AppStrings.warning:'Warning',
  AppStrings.error:'Error',
  AppStrings.room:'Room',
  AppStrings.bighouse:'Big House',
  AppStrings.smallhouse:'Small House',
  AppStrings.pharmacy:'Pharmacy',
  AppStrings.office:'Office',
  AppStrings.clinic:'Clinic',
  AppStrings.commercialShop:'Commercial shop',
  AppStrings.shopDestination:'Shop Destination:',
  AppStrings.landDestination:'Land Destination',
  AppStrings.farmDestination:'Farm Destination',
  AppStrings.landSpace:'Land Space',
  AppStrings.farmSpace:'Farm Space',
  AppStrings.shopSpace:'Shop Space',
  AppStrings.householdContract:'Household Contract',
  AppStrings.furnitureContract:'Furniture Contract',
  AppStrings.conditionerContarct:'Conditioner Contarct',
  AppStrings.isolateContract:'Isolate Contract',
  AppStrings.householdPrice:'Household Price',
  AppStrings.furniturePrice:'Furniture Price',
  AppStrings.conditionerPrice:'Conditioner Price',
  AppStrings.isolatePrice:'Isolate Price',
  AppStrings.noIhavethem:'No, I have them',
  AppStrings.pleaseCompleteallInformaitions:'Please Complete all Informaitions',
  AppStrings.tryAgainplease:'Try Again please',
  AppStrings.JustWaitingalittleminutestocompleate:'Just Waiting a little minutes to compleate',
  AppStrings.pleasecheckfromconnectionInternet:'Please check from connection Internet',
  AppStrings.thereareSomeMissingValues:'There are Some Missing Values',
  AppStrings.noFarmsExistWithThisDetailsYet:'No Farms Exist With This Details Yet!!',
  AppStrings.nolandsExistWithThisDetailsYet:'No Lands Exist With This Details Yet!!',
  AppStrings.nohousesExistWithThisDetailsYet:'No Houses Exist With This Details Yet!!',
  AppStrings.noshopsExistWithThisDetailsYet:'No Shops Exist With This Details Yet!!',
  AppStrings.thereareErrorinEmailorPasswordpleaseCheck:'There are Error in Email or Password please Check',


};
