import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/models/hous/farm_model.dart';
// import 'package:house_app/modules/realestate_app/Rent/shop/shope_types.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';

class ShopController extends GetxController {
  int regionid = 0;
  String regionName = places[0];
  TextEditingController minprice = TextEditingController();
  TextEditingController maxprice = TextEditingController();
  TextEditingController minsize = TextEditingController();
  TextEditingController maxsize = TextEditingController();
  int direction = -1;
  bool loading = false;
  List<FarmModel> listOfShops = [];
  int isowner = -1;
  int stateRentOrSale = -1;
  int payment = -1;
  TextEditingController texttitle = TextEditingController();
  TextEditingController textprice = TextEditingController();
  TextEditingController textspace = TextEditingController();
  File? imageOne;
  File? imageTwo;
  File? imageThree;

  Future<bool> addShop(FarmModel farmModel, List<File> file) async {
    try {
      // print('the burshes number is ${houseModel.furniture_type_id}');
      // print('the payment number is ${houseModel.payment_type_id}');
      // print('the value of Add type is ${addHomeType}');
      String url = 'https://le.sy/api/shop/shoptype/${shopType}/store';
      // String url = 'https://le.sy/api/farm/store';
      var request = http.MultipartRequest(
        'POST',
        Uri.parse(url),
      );
      request.headers.addAll(_setHeader());
      for (int i = 0; i < file.length; i++) {
        var stream = http.ByteStream(file[i].openRead());

        var length = await file[i].length();

        var ans = basename(file[i].path);

        var multipartfile = http.MultipartFile('image${i + 1}', stream, length,
            filename: basename(file[i].path));

        request.files.add(multipartfile);
      }
      Map<String, dynamic> map = farmModel.toJsonShop();
      map.forEach((key, value) {
        request.fields[key] = value.toString();
      });

      var myrequest = await request.send();
      var response = await http.Response.fromStream(myrequest);
      print('the request is ${response.body}');
      if (myrequest.statusCode == 200) {
        print(json.decode(response.body));
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print('the are error here ');
      print('statues is false');
      return false;
    }
  }

  Future<bool> getAllShops(FarmModel farmModel) async {
    listOfShops.clear();
    try {
      String url =
          'https://le.sy/api/shop/shoptype/${shopeType}/getShops?filter[direction]=${farmModel.direction == -1 ? '' : farmModel.direction}&filter[max_price]=${farmModel.max_price == null ? '' : farmModel.max_price}&filter[min_price]=${farmModel.min_price == null ? '' : farmModel.min_price}&filter[min_size]=${farmModel.min_size == null ? '' : farmModel.min_size}&filter[max_size]=${farmModel.max_size == null ? '' : farmModel.max_size}&filter[rent_or_buy]=${farmModel.rent_or_buy}&filter[region_id]=${farmModel.region_id == 0 ? '' : farmModel.region_id}';

      var response = await http.get(Uri.parse(url), headers: _setHeader());
      var result = json.decode(response.body);
      if (result['success'] == true) {
        List<dynamic> list = result['data']['shops'];
        for (int i = 0; i < list.length; i++) {
          FarmModel ans = FarmModel.fromjsonshop(list[i]);
          print('');
          print('${list[i]}');
          print('');
          listOfShops.add(ans);
        }
        print('the lenght of listofshops ${listOfShops.length}');
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  wiating() {
    loading = !loading;
    update();
  }

  changeregion(String value) {
    regionName = value;
    regionid = places.indexWhere((element) => element == value);
    update();
  }

  changedirection(int value) {
    if (value == direction) {
      direction = -1;
    } else {
      direction = value;
    }
    update();
  }

  selectOwner(int value) {
    isowner = value;
    update();
  }

  selectState(int value) {
    stateRentOrSale = value;
    update();
  }

  selectPayment(int value) {
    payment = value;
    update();
  }

  _setHeader() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization':
            'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2xlLnN5L2FwaS9sb2dpbiIsImlhdCI6MTY4OTIwMjc3OSwibmJmIjoxNjg5MjAyNzc5LCJqdGkiOiJhS1BzeUg4azFrNzZReFBVIiwic3ViIjoiMSIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.4afG1QjzjM9ToEH8buDnRYnMMIR5sQytc9s_ZDOkVgk',
        'language':check ?'ar':'en'
      };
}
