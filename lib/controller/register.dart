import 'dart:convert';
import 'package:get/get.dart';
import 'package:house_app/core/services/getxStorage.dart';
import 'package:http/http.dart' as http;
import '../models/user_model.dart';

class RegisterScreenController extends GetxController {
  String url = 'https://le.sy/api/register';
  String? nameErr;
  String? numberErr;
  String? emailErr;
  String? passErr;
  String? c_passwordErr;
  bool loading = false;
  getStorageServic controller = Get.find<getStorageServic>();
  Future<bool> RegisterApp(UserMModel userMModel) async {
    var response = await http.post(Uri.parse(url),
        body: jsonEncode(userMModel.toJsonAllInfromation()),
        headers: _setHeader());
    try {
      var ans = json.decode(response.body);
      if (ans['status'] == 'success') {
        UserToken usertoken = UserToken(
            email: userMModel.email,
            name: userMModel.name,
            phone: userMModel.phone,
            token: ans['authorisation']['token']);

        controller.setValueStorage(usertoken);
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print('there error');
      return false;
    }
  }

  validateName(String name) {
    nameErr = name;
    update();
  }

  validatePassword(String password) {
    passErr = password;
    update();
  }

  validateEmail(String email) {
    emailErr = email;
    update();
  }

  validateNumber(String number) {
    numberErr = number;
    update();
  }

  validateConfirmPass(String c_pass) {
    c_passwordErr = c_pass;
  }

  Waiting() {
    loading = !loading;
    update();
  }

  _setHeader() =>
      {'Content-type': 'application/json', 'Accept': 'application/json'};
}
