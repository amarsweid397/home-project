import 'dart:convert';
import 'dart:io';
//import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/models/hous/house_model.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';

class RoomScreenController extends GetxController {
  int decor = -1;
  int owner = -1;
  int stateSaleOrRent = -1;
  int payment = -1;
  int brushes = -1;
  File? imageOne;
  File? imageTwo;
  File? imageThree;
  int sitting_brushes = 0;
  int bedroom_brushes = 0;
  int bathroom_brushes = 0;
  int kitchen_brushes = 0;
  TextEditingController textBuildingNum = TextEditingController();
  TextEditingController textTitle = TextEditingController();
  TextEditingController textPrice = TextEditingController();
  TextEditingController textSize = TextEditingController();
  int? regionId;
  List<HouseModel> listOfRooms = [];
  int? floor = -1;
  int roomNumber = -1;
  int? direction = -1;
  String dropdownvalue = places[0];
  bool loading = false;

  Future<bool> addHouse(HouseModel houseModel, List<File> file) async {
    try {
      print('the burshes number is ${houseModel.furniture_type_id}');
      print('the payment number is ${houseModel.payment_type_id}');
      print('the value of Add type is ${addHomeType}');
      String url = 'https://le.sy/api/home/hometype/${addHomeType}/store';
      var request = http.MultipartRequest(
        'POST',
        Uri.parse(url),
      );
      request.headers.addAll(_setHeader());
      for (int i = 0; i < file.length; i++) {
        var stream = http.ByteStream(file[i].openRead());

        var length = await file[i].length();

        var ans = basename(file[i].path);

        var multipartfile = http.MultipartFile('image${i + 1}', stream, length,
            filename: basename(file[i].path));

        request.files.add(multipartfile);
      }
      Map<String, dynamic> map = houseModel.RoomtoJson();
      map.forEach((key, value) {
        request.fields[key] = value.toString();
      });

      var myrequest = await request.send();
      var response = await http.Response.fromStream(myrequest);
      print('the request is ${response.body}');
      if (myrequest.statusCode == 200) {
        print(json.decode(response.body));
      } else {}
      return true;
    } catch (e) {
      print('the are error here ');
      print('statues is false');
      return false;
    }
  }

  Future<bool> getAllRooms(
    HouseModel houseModel,
  ) async {
    listOfRooms.clear();

    String url = houseType == 0
        ? 'https://le.sy/api/home/getRooms?filter[max_price]=${houseModel.max_price != null ? houseModel.max_price : ''}&filter[min_price]=${houseModel.min_price != null ? houseModel.min_price : ''}&filter[min_size]=${houseModel.min_size != null ? houseModel.min_size : ''}&filter[max_size]=${houseModel.max_size != null ? houseModel.max_size : ''}&filter[rent_or_buy]=${houseModel.rent_or_buy != null ? houseModel.rent_or_buy : ''}&filter[floor]=${houseModel.floor != null ? houseModel.floor : ''}&filter[direction]=${houseModel.direction != null ? houseModel.direction : ''}&filter[region_id]=${houseModel.region_id != null ? houseModel.region_id : ''}'
        : houseType == 2
            ? 'https://le.sy/api/home/getSmallHouses?filter[floor]=${houseModel.floor != null ? houseModel.floor : ''}&filter[direction]=${houseModel.direction != null ? houseModel.direction : ''}&filter[region_id]=${houseModel.region_id != null ? houseModel.region_id : ''}&filter[max_price]=${houseModel.max_price != null ? houseModel.max_price : ''}&filter[min_price]=${houseModel.min_price != null ? houseModel.min_price : ''}&filter[min_size]=${houseModel.min_size != null ? houseModel.min_size : ''}&filter[max_size]=${houseModel.max_size != null ? houseModel.max_size : ''}&filter[rent_or_buy]=${houseModel.rent_or_buy != null ? houseModel.rent_or_buy.toString() : ''}&filter[numOfRooms]=${houseModel.num_of_rooms != null ? houseModel.num_of_rooms : ''}'
            : 'https://le.sy/api/home/getBigHouses?filter[floor]=${houseModel.floor != null ? houseModel.floor : ''}&filter[direction]=${houseModel.direction != null ? houseModel.direction : ''}&filter[region_id]=${houseModel.region_id != null ? houseModel.region_id : ''}&filter[max_price]=${houseModel.max_price != null ? houseModel.max_price : ''}&filter[min_price]=${houseModel.min_price != null ? houseModel.min_price : ''}&filter[min_size]=${houseModel.min_size != null ? houseModel.min_size : ''}&filter[max_size]=${houseModel.max_size != null ? houseModel.max_size : ''}&filter[rent_or_buy]=${houseModel.rent_or_buy != null ? houseModel.rent_or_buy.toString() : ''}&filter[numOfRooms]=${houseModel.num_of_rooms != null ? houseModel.num_of_rooms : ''}';

    try {
      var response = await http.get(Uri.parse(url), headers: _setHeader());
      var ans = json.decode(response.body);
      if (ans['success'] == true) {
        List<dynamic> list = ans['data']['homes'];
        print('the list is ${list}');

        for (int i = 0; i < list.length; i++) {
          print('bakri aweja');
          HouseModel house = HouseModel.fromJson(list[i]);
          print('');
          print('start');
          print('');
          print('price ${house.price}');
          print('size ${house.size}');
          print('region ${house.region}');
          print('direction ${house.direction}');
          print('floor ${house.floor}');
          print('dicoration ${house.dicoration}');
          print('building_id ${house.building_id}');
          print('user_name ${house.user_name}');
          print('price ${house.num_of_rooms}');
          print('payment type ${house.payment_type}');
          print('');
          print('end');
          print('');
          listOfRooms.add(house);
        }
        print('the lenght is   ${list.length}');

        update();
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  @override
  onInit() {
    print('We are in initial getx');
    floor = -1;
    roomNumber = -1;
    direction = -1;
    dropdownvalue = places[0];
  }

  _setHeader() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization':
            'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2xlLnN5L2FwaS9sb2dpbiIsImlhdCI6MTY4OTIwMjc3OSwibmJmIjoxNjg5MjAyNzc5LCJqdGkiOiJhS1BzeUg4azFrNzZReFBVIiwic3ViIjoiMSIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.4afG1QjzjM9ToEH8buDnRYnMMIR5sQytc9s_ZDOkVgk',
        'language':check ?'ar':'en'
      };
  // eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2xlLnN5L2FwaS9sb2dpbiIsImlhdCI6MTY4OTIwMjc3OSwibmJmIjoxNjg5MjAyNzc5LCJqdGkiOiJhS1BzeUg4azFrNzZReFBVIiwic3ViIjoiMSIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.4afG1QjzjM9ToEH8buDnRYnMMIR5sQytc9s_ZDOkVgk

  selectFloor(int value) {
    if (floor == value) {
      floor = -1;
    } else {
      floor = value;
    }

    floor = value;

    update();
  }

  selectDirection(int value) {
    if (direction == value) {
      direction = -1;
    } else {
      direction = value;
    }
    direction = value;
    update();
  }

  selectRoomNumber(int value) {
    if (roomNumber == value) {
      roomNumber = -1;
    } else {
      roomNumber = value;
    }
    print('the room number is ${roomNumber}');

    update();
  }

  changeRegion(String value) {
    dropdownvalue = value;
    regionId = places.indexWhere((element) => element == value);
    print('the value of region id is ${regionId}');
    update();
  }

  selectDecor(int value) {
    if (value == decor) {
      decor = -1;
    } else {
      decor = value;
    }
    update();
  }

  selectOwner(int value) {
    if (owner == value) {
      owner = -1;
    } else {
      owner = value;
    }
    update();
  }

  selectStateRentOrSale(int value) {
    if (stateSaleOrRent == value) {
      stateSaleOrRent = -1;
    } else {
      stateSaleOrRent = value;
    }
    update();
  }

  selectPayment(int value) {
    if (payment == value) {
      payment = -1;
    } else {
      payment = value;
    }
    update();
  }

  selectBrushes(int value) {
    if (brushes == value) {
      brushes = -1;
    } else {
      brushes = value;
    }
    update();
  }

  Waiting() {
    loading = !loading;
    update();
  }
}
