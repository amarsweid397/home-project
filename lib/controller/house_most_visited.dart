import 'dart:convert';

import 'package:get/get.dart';
import 'package:house_app/models/hous/house_model.dart';
import 'package:http/http.dart' as http;

import '../data/static/static.dart';

class HouseMostVisitedController extends GetxController {
  String url = 'https://le.sy/api/home/mostHomesVisited';
  List<HouseModel> listOfHouse = [];
  Future<bool> getMostVisited() async {

    try {
      var response = await http.get(Uri.parse(url), headers: _setHeader());
      var ans = json.decode(response.body);
      print('ans is ${ans}');
      if (ans['success'] == true) {
        List<dynamic> list = ans['data']['homes'];

        for (int i = 0; i < list.length; i++) {
          HouseModel houseModel = HouseModel.fromJson(list[i]);
          listOfHouse.add(houseModel);
        }
        update();
        return true;
      } else {
        update();
        return false;
      }
    } catch (e) {
      print('there are error bakkar ');
      return false;
    }
  }

  @override
  onInit() {
    super.onInit();
    getMostVisited();
  }

  _setHeader() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization':
            'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2xlLnN5L2FwaS9sb2dpbiIsImlhdCI6MTY4OTIwMjc3OSwibmJmIjoxNjg5MjAyNzc5LCJqdGkiOiJhS1BzeUg4azFrNzZReFBVIiwic3ViIjoiMSIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.4afG1QjzjM9ToEH8buDnRYnMMIR5sQytc9s_ZDOkVgk',
        'language':check ?'ar':'en'
      };
}
