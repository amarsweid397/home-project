import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:house_app/models/hous/farm_model.dart';
import 'package:path/path.dart';
import '../data/static/static.dart';
import 'package:http/http.dart' as http;

class FarmController extends GetxController {
  int? regionId = 0;
  String regionName = places[0];
  TextEditingController minPrice = TextEditingController();
  TextEditingController maxPrice = TextEditingController();
  TextEditingController minspaec = TextEditingController();
  TextEditingController maxspace = TextEditingController();
  int? directions = -1;
  int? floorNumber = -1;
  bool loading = false;
  int pool = -1;
  int isOnwer = -1;
  int stateRentOrSale = -1;
  int payment = -1;
  int burshes = -1;
  int sitting_burshes = 0;
  int bedroom_burshes = 0;
  int bathroom_burshes = 0;
  int kitchen_burshes = 0;
  File? imageOne;
  File? imageTwo;
  File? imageThree;
  TextEditingController texttitle = TextEditingController();
  TextEditingController textprice = TextEditingController();
  TextEditingController textspace = TextEditingController();

  List<FarmModel> listOfFarms = [];

  Future<bool> addFarm(FarmModel farmModel, List<File> file) async {
    try {
      // print('the burshes number is ${houseModel.furniture_type_id}');
      // print('the payment number is ${houseModel.payment_type_id}');
      // print('the value of Add type is ${addHomeType}');
      String url = 'https://le.sy/api/farm/store';
      var request = http.MultipartRequest(
        'POST',
        Uri.parse(url),
      );
      request.headers.addAll(_setHeader());
      for (int i = 0; i < file.length; i++) {
        var stream = http.ByteStream(file[i].openRead());

        var length = await file[i].length();

        var ans = basename(file[i].path);

        var multipartfile = http.MultipartFile('image${i + 1}', stream, length,
            filename: basename(file[i].path));

        request.files.add(multipartfile);
      }
      Map<String, dynamic> map = farmModel.toJson();
      map.forEach((key, value) {
        request.fields[key] = value.toString();
      });

      var myrequest = await request.send();
      var response = await http.Response.fromStream(myrequest);
      print('the request is ${response.body}');
      if (myrequest.statusCode == 200) {
        print(json.decode(response.body));
      } else {}
      return true;
    } catch (e) {
      print('the are error here ');
      print('statues is false');
      return false;
    }
  }

  Future<bool> getAllFarms(FarmModel farmModel) async {
    listOfFarms.clear();

    String url =
        'https://le.sy/api/farm/getFarms?filter[direction]=${farmModel.direction == -1 ? '' : farmModel.direction}&filter[rent_or_buy]=${farmModel.rent_or_buy}&filter[num_of_floors]=${farmModel.num_of_floors == -1 ? '' : (farmModel.num_of_floors! + 1)}&filter[region_id]=${farmModel.region_id == 0 ? '' : farmModel.region_id}&filter[max_price]=${farmModel.max_price == null ? '' : farmModel.max_price}&filter[min_price]=${farmModel.min_price == null ? '' : farmModel.min_price}&filter[min_size]=${farmModel.min_size == null ? '' : farmModel.min_size}&filter[max_size]=${farmModel.max_size == null ? '' : farmModel.max_size}';

    var response = await http.get(Uri.parse(url), headers: _setHeader());

    var responsebody = json.decode(response.body);

    if (responsebody['success'] == true) {
      List<dynamic> lis = responsebody['data']['farms'];

      for (int i = 0; i < lis.length; i++) {
        FarmModel result = FarmModel.fromJson(lis[i]);
        listOfFarms.add(result);
      }

      return true;
    } else {
      return false;
    }
  }

  changeregion(String value) {
    regionName = value;
    regionId = places.indexWhere((element) => element == value);

    update();
  }

  changefloornumber(int value) {
    if (value == floorNumber) {
      floorNumber = -1;
    } else {
      floorNumber = value;
    }

    update();
  }

  changedirection(int value) {
    if (value == directions) {
      directions = -1;
    } else {
      directions = value;
    }

    update();
  }

  selectPool(int value) {
    pool = value;
    update();
  }

  selectOwner(int value) {
    isOnwer = value;
    update();
  }

  selectState(int value) {
    stateRentOrSale = value;
    update();
  }

  selectPayment(int value) {
    payment = value;
    update();
  }

  selectburshes(int value) {
    burshes = value;
    update();
  }

  waiting() {
    loading = !loading;
    update();
  }

  _setHeader() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization':
            'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2xlLnN5L2FwaS9sb2dpbiIsImlhdCI6MTY4OTIwMjc3OSwibmJmIjoxNjg5MjAyNzc5LCJqdGkiOiJhS1BzeUg4azFrNzZReFBVIiwic3ViIjoiMSIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.4afG1QjzjM9ToEH8buDnRYnMMIR5sQytc9s_ZDOkVgk',
        'language':check ?'ar':'en'
      };
}
