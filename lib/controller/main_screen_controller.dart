import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../modules/realestate_app/Home/Main_Screen.dart';
import '../modules/realestate_app/Settings/settingsType_Screen.dart';
import '../modules/realestate_app/advertise/advertise_Screen.dart';

class MainScreenController extends GetxController {
  Widget currentPage = const mainScreen();
  int currentIndex = 0;
  onTapBottom(int index) {
    currentIndex = index;
    if (index == 0) {
      currentPage = const mainScreen();
    } else if (index == 1) {
      currentPage = const advertiseScreen();
    } else {
      currentPage = const settingTypeScreen();
    }
    update();
  }
}
