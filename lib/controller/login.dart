import 'dart:convert';

import 'package:get/get.dart';
import 'package:house_app/core/services/getxStorage.dart';

import 'package:house_app/models/user_model.dart';
import 'package:http/http.dart' as http;

class LoginScreenController extends GetxController {
  bool isPasswordVisible = false;
  bool loading = false;
  getStorageServic controller = Get.find<getStorageServic>();
  String emailErr = '';
  String passErr = '';
  bool isErr = false;

  String url = 'https://le.sy/api/login';

  Future<bool> getdata(
      {required String email, required String password}) async {
    try {
      var response = await http.post(Uri.parse(url),
          body: json.encode({'email': email, 'password': password}),
          headers: _setHeader());
      var ans = json.decode(response.body);
      if (ans['status'] == 'success') {
        UserMModel userMModel = UserMModel.fromJson(ans['user']);
        UserToken usertoken = UserToken(
            email: userMModel.email,
            name: userMModel.name,
            phone: userMModel.phone,
            token: ans['authorisation']['token']);
        controller.setValueStorage(usertoken);
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print('there are error try again');
      return false;
    }
  }

  _setHeader() =>
      {'Content-type': 'application/json', 'Accept': 'application/json'};

  onchangeVisiblePassword() {
    isPasswordVisible = !isPasswordVisible;
    update();
  }

  emailError(String value) {
    emailErr = value;
    update();
  }

  passwordError(String value) {
    passErr = value;
    update();
  }

  Waiting() {
    loading = !loading;
    update();
  }
}
