import 'dart:convert';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/models/hous/services.dart';
import 'package:http/http.dart' as http;

class ServicesController extends GetxController {
  int? workingHour = -1;
  int? numOfEmployes = -1;
  int? material = -1;
  int chooseDateNumber = -1;
  String? chooseDate;
  int choosetimeNumber = -1;
  String? chooseTime;
  int? frequency;
  int? price;
  bool loading = false;

  Future<bool> addContracts(ServicesModels servicesModels) async {
    print('the value of date ${servicesModels.date}');
    print('the value of time ${servicesModels.time}');
    print('the value of repeat ${servicesModels.repeat_time}');
    print('the value of materilas ${servicesModels.materials}');
    print('the value of num_of_employees ${servicesModels.num_of_employees}');
    print('the value of working_hours ${servicesModels.working_hours}');
    print('the number of type is ${servicesNumberType}');

    String url =
        'https://le.sy/api/contract/serviceType/${servicesNumberType}/store';
    var response = await http.post(Uri.parse(url),
        body: json.encode(servicesModels.toJson()), headers: _setHeader());

    var result = json.decode(response.body);
    print('${result}');
    if (result["message"] == "contract is added success") {
      price = result['data']["contract"]["price"];
      return true;
    } else
      return false;
  }

  changeDate(int value) {
    if (value == chooseDateNumber) {
      chooseDateNumber = -1;
      chooseDate = null;
    } else {
      chooseDateNumber = value;
      chooseDate = dateTime.elementAt(value);
    }

    update();
  }

  changTime(int value) {
    if (value == choosetimeNumber) {
      choosetimeNumber = -1;
      chooseTime = null;
    } else {
      choosetimeNumber = value;
      chooseTime = time.elementAt(value);
    }

    update();
  }

  choosefrequency(int value) {
    frequency = value;
    update();
  }

  chooseWorkingHour(int value) {
    if (value == workingHour) {
      workingHour = -1;
    } else {
      workingHour = value;
    }
    update();
  }

  changeNumOfEmployes(int value) {
    if (value == numOfEmployes) {
      numOfEmployes = -1;
    } else {
      numOfEmployes = value;
    }
    update();
  }

  changeMaterials(int value) {
    if (value == material) {
      material = -1;
    } else {
      material = value;
    }
    update();
  }

  _setHeader() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization':
            'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2xlLnN5L2FwaS9sb2dpbiIsImlhdCI6MTY4OTIwMjc3OSwibmJmIjoxNjg5MjAyNzc5LCJqdGkiOiJhS1BzeUg4azFrNzZReFBVIiwic3ViIjoiMSIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.4afG1QjzjM9ToEH8buDnRYnMMIR5sQytc9s_ZDOkVgk',
        'language':check ?'ar':'en'   
      };
  waiting() {
    loading = !loading;
    update();
  }
}
