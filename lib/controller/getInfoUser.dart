import 'dart:convert';

import 'package:get/get.dart';
import 'package:house_app/models/hous/farm_model.dart';
import 'package:house_app/models/hous/house_model.dart';
import 'package:house_app/models/hous/land_model.dart';
import 'package:http/http.dart' as http;

import '../data/static/static.dart';

class InfoUser extends GetxController {
  List<HouseModel> listOfHouseUser = [];
  List<LandModel> listOfLandUser = [];
  List<FarmModel> listOfFarmUser = [];
  List<FarmModel> listOfShopUser = [];
  List<FarmModel> listOfContractUser = [];
  bool loading = false;
  Future<bool> getHouses() async {
    listOfHouseUser.clear();
    try {
      String url = 'https://le.sy/api/user/getUserHomes';
      var response = await http.get(Uri.parse(url), headers: _setHeader());
      if (response.statusCode == 200) {
        var result = jsonDecode(response.body);
        List<dynamic> lis = result['data']['user']['homes'];
        for (int i = 0; i < lis.length; i++) {
          HouseModel model = HouseModel.fromjsonUserInfo(lis[i]);
          listOfHouseUser.add(model);
        }
        print('the lenght of house is ${listOfHouseUser.length}');
        update();
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print('the are error');
      return false;
    }
  }

  Future<bool> getLand() async {
    listOfLandUser.clear();
    try {
      String url = 'https://le.sy/api/user/getUserLands';
      var response = await http.get(Uri.parse(url), headers: _setHeader());
      if (response.statusCode == 200) {
        var result = jsonDecode(response.body);
        List<dynamic> list = result['data']['user']['lands'];
        for (int i = 0; i < list.length; i++) {
          LandModel model = LandModel.fromJsonUser(list[i]);
          listOfLandUser.add(model);
        }
        print('the lenght of land is ${listOfLandUser.length}');
        update();
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  Future<bool> getFarm() async {
    listOfFarmUser.clear();
    try {
      String url = 'https://le.sy/api/user/getUserFarms';
      var response = await http.get(Uri.parse(url), headers: _setHeader());
      if (response.statusCode == 200) {
        var result = jsonDecode(response.body);
        List<dynamic> list = result['data']['user']['farms'];
        for (int i = 0; i < list.length; i++) {
          FarmModel model = FarmModel.fromJsonFarmUser(list[i]);
          listOfFarmUser.add(model);
        }
        print('the lenght of farm is ${listOfFarmUser.length}');
        update();
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  Future<bool> getShops() async {
    listOfShopUser.clear();
    try {
      String url = 'https://le.sy/api/user/getUserShops';
      var response = await http.get(Uri.parse(url), headers: _setHeader());
      if (response.statusCode == 200) {
        var result = jsonDecode(response.body);
        List<dynamic> lis = result['data']['user']['shops'];
        for (int i = 0; i < lis.length; i++) {
          FarmModel model = FarmModel.fromJsonShopUser(lis[i]);
          listOfShopUser.add(model);
        }
        print('the lenght of shops is ${listOfShopUser.length}');
        update();
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  waiting() {
    loading != loading;
    update();
  }

  _setHeader() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization':
            'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2xlLnN5L2FwaS9sb2dpbiIsImlhdCI6MTY4OTIwMjc3OSwibmJmIjoxNjg5MjAyNzc5LCJqdGkiOiJhS1BzeUg4azFrNzZReFBVIiwic3ViIjoiMSIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.4afG1QjzjM9ToEH8buDnRYnMMIR5sQytc9s_ZDOkVgk',
        'language':check ?'ar':'en'
      };
}
