import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:house_app/data/static/static.dart';
import 'package:house_app/models/hous/land_model.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';

class LandScreenController extends GetxController {
  // ignore: non_constant_identifier_names
  List<LandModel> ListOfLands = [];
  String region =places[0];
  int? region_id;
  int typeofland = -1;
  int? isOwner = -1;
  int direction = -1;
  int? stateRentOrSale = -1;
  int? payment = -1;
  File? imageOne;
  File? imageTwo;
  File? imageThree;
  TextEditingController texttitle = TextEditingController();
  TextEditingController textprice = TextEditingController();
  TextEditingController textspace = TextEditingController();

  bool loading = false;

  Future<bool> addLand(LandModel landModel, List<File> file) async {
    try {
      print('value of image is ${imageOne}');
      print('value of image is ${imageTwo}');
      print('value of image is ${imageThree}');

      // print('the burshes number is ${houseModel.furniture_type_id}');
      // print('the payment number is ${houseModel.payment_type_id}');
      // print('the value of Add type is ${addHomeType}');
      String url = 'https://le.sy/api/land/store';
      var request = http.MultipartRequest(
        'POST',
        Uri.parse(url),
      );
      request.headers.addAll(_setHeader());
      for (int i = 0; i < file.length; i++) {
        var stream = http.ByteStream(file[i].openRead());

        var length = await file[i].length();

        var ans = basename(file[i].path);
        print(ans);
        var multipartfile = http.MultipartFile('image${i + 1}', stream, length,
            filename: basename(file[i].path));

        request.files.add(multipartfile);
      }
      Map<String, dynamic> map = landModel.toJson();
      map.forEach((key, value) {
        request.fields[key] = value.toString();
      });
      print('bakri aweja heree');
      var myrequest = await request.send();
      print('bakri aweja heree');
      var response = await http.Response.fromStream(myrequest);
      print('bakri aweja heree');
      print('the request is ${response.body}');
      if (myrequest.statusCode == 200) {
        print(json.decode(response.body));
      } else {}
      return true;
    } catch (e) {
      print('the are error here ');
      print('statues is false');
      return false;
    }
  }

  Future<bool> getAllLand(LandModel landmodel) async {
    ListOfLands.clear();
    String url =
        'https://le.sy/api/land/getLands?filter[max_price]=${landmodel.max_price != null ? landmodel.max_price : ''}&filter[min_price]=${landmodel.min_price != null ? landmodel.min_price : ''}&filter[min_size]=${landmodel.min_size != null ? landmodel.min_size : ''}&filter[max_size]=${landmodel.max_size != null ? landmodel.max_size : ''}&filter[rent_or_buy]=${landmodel.rent_or_buy != null ? landmodel.rent_or_buy : ''}&filter[direction]=${landmodel.direction != null ? landmodel.direction : ''}&filter[region_id]=${landmodel.region_id != null ? landmodel.region_id : ''}&filter[land_type]=${landmodel.land_type != null ? landmodel.land_type : ''}';

    try {
      var response = await http.get(Uri.parse(url), headers: _setHeader());
      var ans = json.decode(response.body);

      if (ans['success'] == true) {
        List<dynamic> list = ans['data']['lands'];

        for (int i = 0; i < list.length; i++) {
          LandModel land = LandModel.fromJson(list[i]);
          ListOfLands.add(land);
        }

        update();
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  _setHeader() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization':
            'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2xlLnN5L2FwaS9sb2dpbiIsImlhdCI6MTY4OTIwMjc3OSwibmJmIjoxNjg5MjAyNzc5LCJqdGkiOiJhS1BzeUg4azFrNzZReFBVIiwic3ViIjoiMSIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.4afG1QjzjM9ToEH8buDnRYnMMIR5sQytc9s_ZDOkVgk',
        'language':check ?'ar':'en'
      };
  changeRegion(String value) {
    region = value;
    region_id = places.indexWhere((element) => element == value);
    update();
  }

  selectType(int value) {
    typeofland = value;
    update();
  }

  selectDirection(int val) {
    direction = val;
    update();
  }

  selectOwner(int value) {
    isOwner = value;
    update();
  }

  selectState(int value) {
    stateRentOrSale = value;
    update();
  }

  selectPayment(int value) {
    payment = value;
    update();
  }

  Waiting() {
    loading = !loading;
    update();
  }
}
